package com.central.file.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.hutool.core.util.IdUtil;
import com.central.common.model.Result;
import com.central.file.service.IFileService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.central.common.model.PageResult;
import com.central.file.model.FileInfo;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;

/**
 * 文件上传
 *
 */
@RestController
public class FileController {
    @Resource
    private IFileService fileService;


    /**
     * 文件上传
     * 根据fileType选择上传方式
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/files-anon")
    public FileInfo upload(@RequestParam("file") MultipartFile file) throws Exception {
        return fileService.upload(file);
    }



    @PostMapping("/auto")
    public void LocalhostUpload()  {
        showDir(new File("F:\\videos"));
    }


    @PostMapping("/test")
    public void test()  {
        List<FileInfo> list = fileService.list();
        list.stream().forEach(l->{
            fileService.delete(l.getId());
        });

    }


    /**
     * 文件删除
     *
     * @param id
     */
    @DeleteMapping("/files/{id}")
    public Result delete(@PathVariable String id) {
        try {
            fileService.delete(id);
            return Result.succeed("操作成功");
        } catch (Exception ex) {
            return Result.failed("操作失败");
        }
    }

    /**
     * 文件查询
     *
     * @param params
     * @return
     */
    @GetMapping("/files")
    public PageResult<FileInfo> findFiles(@RequestParam Map<String, Object> params) {
        return fileService.findList(params);
    }


    public List<FileInfo> showDir(File dir)  {
        List<FileInfo> fileInfos = new ArrayList();
        if(dir.exists()){
            //抽象路径名数组，这些路径名表示此抽象路径名表示的目录中的文件和目录。
            File[] files = dir.listFiles();
            if(null!=files){
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        showDir(files[i]);
                    } else {
                        //---------------------正则提取ip地址-----
                        String regexString="((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
                        Pattern p = Pattern.compile(regexString);
                        Matcher matcher = p.matcher(files[i].getPath());
                        String deviceIp = "";
                        if (matcher.find()) {
                            deviceIp = matcher.group();
                        }
                        //-----------------
                        FileInfo fileInfo = new FileInfo();
                        // 将文件的md5设置为文件表的id
                        fileInfo.setId(IdUtil.fastSimpleUUID());
                        fileInfo.setName(files[i].getName());
                        String contentType = new MimetypesFileTypeMap().getContentType(new File(files[i].getAbsolutePath()));
                        fileInfo.setContentType(contentType);

                        fileInfo.setIsImg(fileInfo.getContentType().startsWith("image/"));
                        fileInfo.setPath(files[i].getAbsolutePath());
                        fileInfo.setDeviceIp(deviceIp);
                        fileInfo.setSize(files[i].length());
                        fileInfo.setCreateTime(new Date());
                        try {
                            fileService.uploadLocal(fileInfo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        fileInfos.add(fileInfo);
                    }
                }
            }
        }else{
            System.out.println("文件不存在！");
        }
        return  fileInfos;
    }
}
