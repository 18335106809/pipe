package com.central.file.utils;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: FileTest
 * @Description: Todo
 *
 * @data: 2020/7/8  19:03
 */
public class FileTest {
    public static void main(String[] args) {
        System.out.println("---------------------递归开始-----------------------");
        showDir(new File("F:\\videos"));
        System.out.println("---------------------递归结束-----------------------");

    }

    public static void showDir(File dir)  {
        if(dir.exists()){
            //抽象路径名数组，这些路径名表示此抽象路径名表示的目录中的文件和目录。
            File[] files = dir.listFiles();
            if(null!=files){
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        showDir(files[i]);
                    } else {
                        //---------------------正则提取ip地址-----
                        String regexString="((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
                        Pattern p = Pattern.compile(regexString);
                        Matcher matcher = p.matcher(files[i].getPath());
                        String deviceIp = "";
                        if (matcher.find()) {
                            deviceIp = matcher.group();
                        }
                        //-----------------

                        System.out.println(files[i].getPath());
                    }
                }
            }
        }else{
            System.out.println("文件不存在！");
        }
    }

}
