package com.central.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.user.model.ChatMessage;
import com.central.user.service.IChatMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @ClassName: chatMessageHistory
 * @Description: Todo
 *
 * @data: 2020/5/9  14:58
 */

@RestController
@RequestMapping("/chatMessage")
@Api(tags = "聊天模块api")
public class ChatMessageController {

    @Autowired
    private IChatMessageService chatMessageService;


    @PostMapping("/historyChat")
    @ApiOperation(value = "查询当前用户收到的消息列表")
    public Result findSelfPage(@RequestBody Map<String, Object> map) {
        Integer current = MapUtils.getInteger(map, "current");
        Integer size = MapUtils.getInteger(map, "size");
        String sendUserId = MapUtils.getString(map, "sendUserId");
        String receiveUserId = MapUtils.getString(map, "receiveUserId");
        Page page = new Page<>(current, size);
        QueryWrapper<ChatMessage> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("send_user_id",sendUserId);
        queryWrapper.eq("receive_user_id",receiveUserId);
        return Result.succeed(chatMessageService.page(page, queryWrapper));
    }


    @PutMapping("/{ids}")
    @ApiOperation(value = "更新消息状态为已读")
    public Result update(
            @PathVariable
            @NotNull(message = "消息id不能为空")
            @NotEmpty(message = "消息id不能为空")
            @ApiParam(value = "消息id，多个用逗号分隔", required = true) List<Integer> ids) {
        List<ChatMessage> chatMessages = chatMessageService.listByIds(ids);
        if (chatMessages!=null) {
            chatMessages.forEach(chatMessage -> chatMessage.setReadStatus(1));
            chatMessageService.updateBatchById(chatMessages);
        }
        return Result.succeed("更新消息状态成功！");
    }


//    @PostMapping("/test")
//    @ApiOperation(value = "查询当前用户收到的消息列表")
//    public void test(@RequestBody Map<String, Object> map) {
//        String sendUserId = MapUtils.getString(map, "sendUserId");
//        QueryWrapper<ChatMessage> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("send_user_id",sendUserId).or().eq("receive_user_id",sendUserId).eq("type",1);
//        queryWrapper.orderByDesc("create_time");
//
//        List<ChatMessage> chatMessageList = chatMessageService.list(queryWrapper);
//        List<Long> userIds = new ArrayList<>();
//        for (int i=0;i<=chatMessageList.size();i++){
//
//            if (chatMessageList.get(i).getReceiveUserId().equals(sendUserId)){
//                userIds.
//            }
//
//                //ids.add(chatMessageList.get(i).getId());
//
//
//        }

        //chatMessageList.stream().filter(a->a.equals(a)).collect(Collectors.toList());


//
//        for (int i=0;i<chatMessageList.size();i++){
//            chatMessageList.get(i).setSendUser(iSysUserService.getById(chatMessageList.get(i).getSendUserId()));
//            chatMessageList.get(i).setReceiveUser(iSysUserService.getById(chatMessageList.get(i).getReceiveUserId()));
//        }

//    }


}
