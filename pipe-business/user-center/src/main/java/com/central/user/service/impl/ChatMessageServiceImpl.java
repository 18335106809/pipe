package com.central.user.service.impl;

import com.central.common.service.impl.SuperServiceImpl;
import com.central.user.mapper.ChatMessageMapper;
import com.central.user.model.ChatMessage;
import com.central.user.service.IChatMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ChatMessageServiceImpl
 * @Description: Todo
 *
 * @data: 2020/5/27  17:39
 */
@Slf4j
@Service
public class ChatMessageServiceImpl extends SuperServiceImpl<ChatMessageMapper, ChatMessage> implements IChatMessageService {
}
