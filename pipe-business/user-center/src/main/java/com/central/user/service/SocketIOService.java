package com.central.user.service;

public interface SocketIOService {
    //推送消息的事件
    public static final String CHAT_MESSAGE_EVENT = "chatMessage";

    //最近通话
    public static final String Recently_MESSAGE_EVENT = "recentlyMessage";
    // 启动服务
    void start() throws Exception;

    // 停止服务
    void stop();

}
