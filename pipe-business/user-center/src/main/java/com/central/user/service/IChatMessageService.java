package com.central.user.service;

import com.central.common.service.ISuperService;
import com.central.user.model.ChatMessage;

public interface IChatMessageService  extends ISuperService<ChatMessage> {

}
