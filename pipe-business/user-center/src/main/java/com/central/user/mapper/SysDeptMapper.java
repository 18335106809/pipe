package com.central.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.common.model.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @since 2020-02-11
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

    boolean editUserDept(String deptId, Long id);

}
