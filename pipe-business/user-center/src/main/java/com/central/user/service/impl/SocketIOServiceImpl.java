package com.central.user.service.impl;

/**
 * @ClassName: SocketIOServiceImpl
 * @Description: Todo
 *
 * @data: 2020/5/27  11:49
 */
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.model.SysUser;
import com.central.user.model.ChatMessage;
import com.central.user.service.IChatMessageService;
import com.central.user.service.ISysUserService;
import com.central.user.service.SocketIOService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;


@Slf4j
@Service(value = "socketIOService")
public class SocketIOServiceImpl implements SocketIOService {

    @Autowired
    private IChatMessageService iChatMessageService;
    @Autowired
    private ISysUserService iSysUserService;

    // 用来存已连接的客户端
    private static Map<String, SocketIOClient> clientMap = new ConcurrentHashMap<>();
    //sessionId 与 userInfo 映射
    private final static ConcurrentHashMap<UUID, SysUser> USER_INFO_MAP = new ConcurrentHashMap<>();


    @Autowired
    private SocketIOServer socketIOServer;


    @Resource(name = "JsonRedistemplate")
    private RedisTemplate redisTemplate;
    /**
     * Spring IoC容器创建之后，在加载SocketIOServiceImpl Bean之后启动
     * @throws Exception
     */
    @PostConstruct
    private void autoStartup() throws Exception {
        start();
    }

    /**
     * Spring IoC容器在销毁SocketIOServiceImpl Bean之前关闭,避免重启项目服务端口占用问题
     * @throws Exception
     */
    @PreDestroy
    private void autoStop() throws Exception  {
        stop();
    }

    @Override
    public void start() {
        // 监听客户端连接
        socketIOServer.addConnectListener(client -> {
            UUID sessionId = client.getSessionId();
            String userId = client.getHandshakeData().getSingleUrlParam("userId");
            if (userId == null) {
                return;
            }
            SysUser sysUser  = iSysUserService.getById(userId);
            //put 用户信息
            USER_INFO_MAP.put(sessionId, sysUser);
        });

        // 监听客户端断开连接
        socketIOServer.addDisconnectListener(client -> {
            String userId = client.getHandshakeData().getSingleUrlParam("userId");
            if (userId != null) {
                clientMap.remove(userId);
                client.disconnect();
            }
        });

        // 处理自定义的事件，与连接监听类似
        socketIOServer.addEventListener(CHAT_MESSAGE_EVENT, ChatMessage.class, (client, data, ackSender) -> {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setMsg(data.getMsg());
            chatMessage.setSendUserId(data.getSendUserId());
            chatMessage.setReceiveUserId(data.getReceiveUserId());
            chatMessage.setReadStatus(0);
            iChatMessageService.save(chatMessage);
            client.sendEvent(CHAT_MESSAGE_EVENT, iChatMessageService.getById(chatMessage.getId()));
        });

        // 处理自定义的事件，与连接监听类似
        socketIOServer.addEventListener("RECENTLY_MESSAGE_EVENT", ChatMessage.class, (client, data, ackSender) -> {
            QueryWrapper<ChatMessage> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("send_user_id",data.getSendUserId()).or().eq("receive_user_id",data.getSendUserId());
            List<ChatMessage> chatSendMessageList = iChatMessageService.list(queryWrapper);

            Map<String, Object> hashMap = new HashMap<>();

            for (int i=0;i<chatSendMessageList.size();i++){
                hashMap.put(chatSendMessageList.get(i).getSendUserId(),chatSendMessageList.get(i).getSendUserId());
                hashMap.put(chatSendMessageList.get(i).getReceiveUserId(),chatSendMessageList.get(i).getReceiveUserId());
            }

            List list = new ArrayList();
            for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                list.add(iSysUserService.getById(entry.getKey()));
            }

            client.sendEvent("RECENTLY_MESSAGE_EVENT", list);
        });
        socketIOServer.start();
    }

    @Override
    public void stop() {
        if (socketIOServer != null) {
            socketIOServer.stop();
            socketIOServer = null;
        }
    }


}