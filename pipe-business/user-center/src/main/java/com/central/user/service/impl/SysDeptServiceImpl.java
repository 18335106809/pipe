package com.central.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.common.model.SysUser;
import com.central.user.mapper.SysDeptMapper;
import com.central.common.model.SysDept;
import com.central.user.service.ISysDeptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @since 2020-02-11
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Resource
    private SysDeptMapper sysDeptMapper;

    @Override
    public List<SysDept> selectDeptList() {
        QueryWrapper<SysDept> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public SysDept insertDept(SysDept dept) {
        baseMapper.insert(dept);
        return baseMapper.selectById(dept.getId());
    }

    @Override
    public SysDept updateDept(SysDept dept) {
        baseMapper.updateById(dept);
        return baseMapper.selectById(dept);
    }

    @Override
    public boolean checkDeptNameUnique(String dept) {
        QueryWrapper<SysDept> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("deptName",dept);
        Integer i = baseMapper.selectCount(queryWrapper);
        if (i > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean editUserDept(String deptId, SysUser user) {
        return  sysDeptMapper.editUserDept(deptId, user.getId());
    }
}
