package com.central.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.common.model.SysDept;
import com.central.db.mapper.SuperMapper;
import com.central.user.model.ChatMessage;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ChatMessageMapper  extends SuperMapper<ChatMessage> {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);


    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(ChatMessage record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    ChatMessage selectByPrimaryKey(Long id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(ChatMessage record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(ChatMessage record);



}