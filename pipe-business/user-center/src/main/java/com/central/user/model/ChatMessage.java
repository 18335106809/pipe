package com.central.user.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.central.common.model.SuperEntity;
import com.central.common.model.SysUser;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName: PushMessage
 * @Description: Todo
 *
 * @data: 2020/5/27  11:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatMessage extends SuperEntity implements Serializable {

  /**
    * 消息类型 1别人给自己 2自己给别人
    */
    private String type;

    /**
    * 发送人
    */
    private String sendUserId;

    /**
    * 接收人
    */
    private String receiveUserId;

    /**
    * 发送消息
    */
    private String msg;

    /**
    * readStatus
    */
    private Integer readStatus;
    @TableField(exist = false)
    private SysUser sendUser;
    @TableField(exist = false)
    private SysUser receiveUser;
}