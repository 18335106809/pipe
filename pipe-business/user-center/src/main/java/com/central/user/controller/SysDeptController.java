package com.central.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginUser;
import com.central.common.model.*;
import com.central.log.annotation.AuditLog;
import com.central.user.service.ISysDeptService;
import com.central.user.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门信息
 */
@RestController
@RequestMapping("/dept")
@Api(tags = "部门模块api")
public class SysDeptController {


    @Autowired
    private ISysDeptService iSysDeptService;
    @Autowired
    private ISysUserService iSysUserService;

    @GetMapping("/page")
    @ApiOperation(value = "部门分页列表", notes="部门分页列表")
    @ResponseBody
    @AuditLog(operation = "部门分页列表")
    public PageResult<SysDept> page(@RequestParam("page") Integer page1, @RequestParam("limit") Integer limit) {
        Page<SysDept> page = new Page<>(page1,limit);
        List<SysDept> list = iSysDeptService.list();
        return PageResult.<SysDept>builder().data(list).code(0).count(page.getTotal()).build();
    }


    @GetMapping("/list")
    @ApiOperation(value = "部门列表", notes="部门列表")
    @AuditLog(operation = "部门列表")
    public Result deptlist() {
        List<SysDept> dept = iSysDeptService.list();
        List<SysDept> sysDepts = treeBuilder(dept);
        return Result.succeed(sysDepts);
    }
    /**
     * 新增或保存部门
     */
    @PostMapping("/saveOrUpdate")
    @ApiOperation(value = "新增或保存部门", notes="返回部门信息")
    public Result addDeptSave(@RequestBody SysDept dept) {
       return  Result.succeed(iSysDeptService.saveOrUpdate(dept));
    }


    /**
     * 删除
     */
    @DeleteMapping("/removeById")
    @ResponseBody
    @ApiOperation(value = "删除部门", notes="删除部门")
    public Result remove(@RequestParam("deptId") Long deptId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id",deptId);
        //判断是否有子部门
        List<Long> deptList = iSysDeptService.list(queryWrapper);
        if(deptList.size() > 0){
            return Result.failed("请先删除子部门");
        }

        return  Result.succeed(iSysDeptService.removeById(deptId));
    }

    /**
     * 校验部门名称
     */
    @GetMapping("/checkDeptNameUnique")
    @ApiOperation(value = "校验部门名称", notes="校验部门名称")
    public boolean checkDeptNameUnique(@RequestBody String dept) {
        return iSysDeptService.checkDeptNameUnique(dept);
    }


    @GetMapping("/deptUser")
    @ApiOperation(value = "部门下用户列表", notes="部门下用户列表")
    @ResponseBody
    @AuditLog(operation = "部门下用户列表")
    public Result DeptUser() {
        List<SysDept> deptList = iSysDeptService.list();
        List<SysDept> sysDepts = treeBuilder(deptList);


        for (int i=0;i< sysDepts.size();i++){
            //一级菜单复制
            QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
            queryWrapper.eq("dept_id",deptList.get(i).getId());
            List<SysUser> sysUserList = iSysUserService.list(queryWrapper);


            deptList.get(i).setUserInfo(sysUserList);
            //二級菜單
            if (sysDepts.get(i).getSubDept() != null) {
                for (int j = 0; j < sysDepts.get(i).getSubDept().size(); j++) {
                    QueryWrapper<SysUser> queryWrapper1 = new QueryWrapper();
                    queryWrapper1.eq("dept_id", sysDepts.get(j).getId());
                  List<SysUser> sysUserList2 = iSysUserService.list(queryWrapper1);
                    sysDepts.get(i).getSubDept().get(j).setUserInfo(sysUserList2);
               }
          }
        }
        return Result.succeed(sysDepts);
    }


    /**
     * 两层循环实现建树
     */
    public  List<SysDept> treeBuilder(List<SysDept> sysDepts) {
        List<SysDept> depts = new ArrayList<>();
        for (SysDept sysDept : sysDepts) {
            if (ObjectUtils.equals(-1L, sysDept.getParentId())) {
                depts.add(sysDept);
            }
            for (SysDept dept : sysDepts) {
                if (dept.getParentId().equals(sysDept.getId())) {
                    if (sysDept.getSubDept() == null) {
                        sysDept.setSubDept(new ArrayList<>());
                    }
                    sysDept.getSubDept().add(dept);
                }
            }
        }
        return depts;
    }


}