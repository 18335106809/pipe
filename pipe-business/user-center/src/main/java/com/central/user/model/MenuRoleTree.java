package com.central.user.model;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: MenuRoleTree
 * @Description: Todo
 *
 * @data: 2020/4/27  11:51
 */
@Data
public class MenuRoleTree {
    private String name;
    private Boolean checked;
    private Long pId;
    private Long id;
    private Boolean open;
    private List<MenuRoleTree> subMenuRole;
}
