package com.central.user.controller;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.annotation.LoginUser;
import com.central.common.constant.CommonConstant;
import com.central.common.model.*;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.user.model.MenuRoleTree;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import com.central.user.service.ISysMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletResponse;

/**
 */
@RestController
@Api(tags = "菜单模块api")
@Slf4j
@RequestMapping("/menus")
public class SysMenuController {
    @Autowired
    private ISysMenuService menuService;

    /**
     * 两层循环实现建树
     *
     * @param sysMenus
     * @return
     */
    public static List<SysMenu> treeBuilder(List<SysMenu> sysMenus) {
        List<SysMenu> menus = new ArrayList<>();
        for (SysMenu sysMenu : sysMenus) {
            if (ObjectUtils.equals(-1L, sysMenu.getParentId())) {
                menus.add(sysMenu);
            }
            for (SysMenu menu : sysMenus) {
                if (menu.getParentId().equals(sysMenu.getId())) {
                    if (sysMenu.getSubMenus() == null) {
                        sysMenu.setSubMenus(new ArrayList<>());
                    }
                    sysMenu.getSubMenus().add(menu);
                }
            }
        }
        return menus;
    }


    public static List<MenuRoleTree> roleTreeBuilder(List<MenuRoleTree> menuRoleTrees) {
        List<MenuRoleTree> menuroles = new ArrayList<>();
        for (MenuRoleTree menuRoleTree : menuRoleTrees) {
            if (ObjectUtils.equals(-1L, menuRoleTree.getPId())) {
                menuroles.add(menuRoleTree);
            }
            for (MenuRoleTree menuRole : menuRoleTrees) {
                if (menuRole.getPId().equals(menuRoleTree.getId())) {
                    if (menuRoleTree.getSubMenuRole() == null) {
                        menuRoleTree.setSubMenuRole(new ArrayList<>());
                    }
                    menuRoleTree.getSubMenuRole().add(menuRole);
                }
            }
        }
        return menuroles;
    }


    /**
     * 删除菜单
     *
     * @param id
     */
    @ApiOperation(value = "删除菜单")
    @AuditLog(operation ="删除菜单" )
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("id") Long id) {
        try {
            menuService.removeById(id);
            return Result.succeed("操作成功");
        } catch (Exception ex) {
            log.error("memu-delete-error", ex);
            return Result.failed("操作失败");
        }
    }

    @ApiOperation(value = "根据roleId获取对应的菜单")
    @AuditLog(operation ="根据roleId获取对应的菜单" )
    @GetMapping("/{roleId}/menus")
    public Result findMenusByRoleId(@PathVariable Long roleId) {
        Set<Long> roleIds = new HashSet<>();
        roleIds.add(roleId);
        //获取该角色对应的菜单
        List<SysMenu> roleMenus = menuService.findByRoles(roleIds);
        //全部的菜单列表
        List<SysMenu> allMenus = menuService.findAll();
        List<MenuRoleTree> menuRoleTrees = new ArrayList<>();
        Map<Long, SysMenu> roleMenusMap = roleMenus.stream().collect(Collectors.toMap(SysMenu::getId, SysMenu -> SysMenu));

        for (SysMenu sysMenu : allMenus) {
            MenuRoleTree menuRoleTree =  new MenuRoleTree();
            menuRoleTree.setId(sysMenu.getId());
            menuRoleTree.setName(sysMenu.getName());
            menuRoleTree.setPId(sysMenu.getParentId());
            menuRoleTree.setOpen(true);
            menuRoleTree.setChecked(false);
            if (roleMenusMap.get(sysMenu.getId()) != null) {
                menuRoleTree.setChecked(true);
            }
            menuRoleTrees.add(menuRoleTree);
        }
        List<MenuRoleTree> menuRoleTrees1 = roleTreeBuilder(menuRoleTrees);
        //---三级菜单更改【】-----
        for(MenuRoleTree tree : menuRoleTrees1){
            if (tree.getSubMenuRole()==null) {
                tree.setSubMenuRole(new ArrayList<>());
            }else {
                for(MenuRoleTree subTree : tree.getSubMenuRole()){
                    if (subTree.getSubMenuRole()==null) {
                        subTree.setSubMenuRole(new ArrayList<>());
                    }else {
                        for(MenuRoleTree treesTree :subTree.getSubMenuRole()){
                            if (treesTree.getSubMenuRole()==null) {
                                treesTree.setSubMenuRole(new ArrayList<>());
                            }
                        }

                    }
                }
            }
        }
        return Result.succeed(menuRoleTrees1);
    }


    @ApiOperation(value = "根据roleCodes获取对应的权限")
    @SuppressWarnings("unchecked")
    @GetMapping("/{roleCodes}")
    public  Result findMenuByRoles(@PathVariable String roleCodes) {
        List<SysMenu> result = null;
        if (StringUtils.isNotEmpty(roleCodes)) {
            Set<String> roleSet = (Set<String>)Convert.toCollection(HashSet.class, String.class, roleCodes);
            result = menuService.findByRoleCodes(roleSet, CommonConstant.PERMISSION);
        }
        return Result.succeed(result);
    }


    /**
     * 给角色分配菜单
     */
    @ApiOperation(value = "角色分配菜单")
    @PostMapping("/granted")
    public Result setMenuToRole(@RequestBody SysMenu sysMenu) {
        menuService.setMenuToRole(sysMenu.getRoleId(), sysMenu.getMenuIds());
        return Result.succeed("操作成功");
    }

    @ApiOperation(value = "查询所有菜单")
    @GetMapping("/findAlls")
    public PageResult<SysMenu> findAlls() {
        List<SysMenu> list = menuService.findAll();
        List<SysMenu> menus =treeBuilder(list);
        return PageResult.<SysMenu>builder().data(menus).code(0).count((long) list.size()).build();
    }

    @ApiOperation(value = "获取菜单以及顶级菜单")
    @GetMapping("/findOnes")
    public PageResult<SysMenu> findOnes() {
        List<SysMenu> list = menuService.findOnes();
        return PageResult.<SysMenu>builder().data(list).code(0).count((long) list.size()).build();
    }

    /**
     * 添加菜单 或者 更新
     *
     * @param menu
     * @return
     */
    @ApiOperation(value = "新增菜单")
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody SysMenu menu) {
        try {
            menuService.saveOrUpdate(menu);
            return Result.succeed("操作成功");
        } catch (Exception ex) {
            log.error("memu-saveOrUpdate-error", ex);
            return Result.failed("操作失败");
        }
    }

    /**
     * 当前登录用户的菜单
     *
     * @return
     */
    @GetMapping("/current")
    @ApiOperation(value = "查询当前用户菜单")
    public Result findMyMenu(@LoginUser SysUser user) {
        List<SysRole> roles = user.getRoles();
        if (CollectionUtil.isEmpty(roles)) {
            return Result.succeed(Collections.emptyList());
        }
        List<SysMenu> menus = menuService.findByRoleCodes(roles.parallelStream().map(SysRole::getCode).collect(Collectors.toSet()), CommonConstant.MENU);
        return  Result.succeed(treeBuilder(menus));
    }

//    /**
//     *用户的菜单
//     *
//     * @return
//     */
//    @GetMapping("/list")
//    @ApiOperation(value = "查询当前用户菜单")
//    public Result list() {
//        List<SysDept> dept = menuService.list();
//        List<SysDept> sysDepts = treeBuilder(dept);
//        return Result.succeed(sysDepts);
//
//    }

    @GetMapping("/export")
    public void exportUser(@RequestParam Map<String, Object> params, HttpServletResponse response)
            throws IOException {

        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        List<SysMenu> sysMenus = menuService.list(queryWrapper);
//        List<SysMenu> deviceExcels = new ArrayList<>();
//        for (int i = 0; i < devices.size(); i++) {
//            SysMenu deviceExcel = new SysMenu();
//            BeanUtil.copyProperties(devices.get(i), deviceExcel);
//            deviceExcels.add(deviceExcel);
//        }
        // 导出操作
        ExcelUtil.exportExcel(sysMenus, null, "系统菜单", SysMenu.class, "menu", response);
    }


}
