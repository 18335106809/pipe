package com.central.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.common.model.SysUser;
import com.central.common.model.SysDept;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @since 2020-02-11
 */
public interface ISysDeptService extends IService<SysDept> {

    List<SysDept> selectDeptList();

    SysDept insertDept(SysDept sysDept);

    SysDept updateDept(SysDept sysDept);

    boolean checkDeptNameUnique(String dept);

    boolean editUserDept(String deptId, SysUser user);
}
