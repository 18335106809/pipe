package com.central.ops.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-02-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "staff_pipe")
public class StaffPipe extends SuperEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请人
     */
    private Long submitterUserId;


    private String pipeUserName;

    /**
     * 申请人
     */
    private String submitterName;


    /**
     * 描述
     */
    private String description;

    /**
     * 受理人
     */
    private Long acceptUserId;
    /**
     * 受理人
     */
    private String acceptUserName;

    /**
     * 审批时间
     */
    private Date acceptTime;

    /**
     * 进入地方
     */
    private String location;


    private Integer status;

    /**
     * 实际离开时间
     */
    private Date actualDepartureTime;

    /**
     * 规定离开时间
     */
    private Date ruleDepartureTime;

    //标签
    private String lableId;


}
