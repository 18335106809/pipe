//package com.central.ops.controller;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.central.common.annotation.LoginUser;
//import com.central.common.feign.UserService;
//import com.central.common.model.PageResult;
//import com.central.common.model.Result;
//import com.central.common.model.SysUser;
//import com.central.log.annotation.AuditLog;
//import com.central.ops.model.Workorder;
//import com.central.ops.model.vo.DeviceMaintenanceRecord;
//import com.central.ops.model.vo.TaskWorker;
//import com.central.ops.service.IDeviceMaintenanceRecordService;
//import com.central.ops.service.IWorkorderService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.activiti.engine.*;
//import org.activiti.engine.history.HistoricActivityInstance;
//import org.activiti.engine.history.HistoricTaskInstance;
//import org.activiti.engine.repository.Deployment;
//import org.activiti.engine.repository.ProcessDefinition;
//import org.activiti.engine.runtime.ProcessInstance;
//import org.activiti.engine.task.Task;
//import org.apache.commons.collections4.MapUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import java.io.*;
//
//import java.util.*;
//
///**
// * @Description
// *
// * <p>任务相关接口
// */
//@Slf4j
//@RestController
//@Api(tags = "任务相关接口")
//@RequestMapping("/worker")
//public class DeployController {
//  @Resource private IWorkorderService iWorkorderService;
//  @Autowired private RuntimeService runtimeService;
//  @Autowired private TaskService taskService;
//  @Autowired private HistoryService historyService;
//  @Autowired private RepositoryService repositoryService;
//  @Resource private UserService userService;
//  @Autowired private IDeviceMaintenanceRecordService iDeviceMaintenanceRecordService;
//  /**
//   * 流程部署
//   *
//   * @param bpmnName bpmn文件名(不包含扩展名)
//   * @param deployName 流程部署名称
//   */
//  @AuditLog(operation = "流程部署")
//  @GetMapping("/deploy")
//  @ApiOperation(value = "流程部署", notes = "流程部署")
//  @Transactional
//  public Result deploy(
//      @RequestParam("bpmnName") String bpmnName, @RequestParam("deployName") String deployName) {
//    // 获取总操作对象
//    ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
//    // 获取部署对象
//    RepositoryService repositoryService = defaultProcessEngine.getRepositoryService();
//
//    // 添加流程信息
//    Deployment deploy =
//        repositoryService
//            .createDeployment()
//            .addClasspathResource("processes/" + bpmnName + ".bpmn")
//            .addClasspathResource("processes/" + bpmnName + ".png")
//            .name(deployName)
//            .deploy();
//    System.err.println("部署工单成功:" + deploy.getName());
//
//    return Result.succeed("部署成功,工单名：" + deploy.getName());
//  }
//
//  /** 新增工单管理 */
//  @AuditLog(operation = "新增一个工单")
//  @PostMapping("/add")
//  @ApiOperation(value = "新增工单", notes = "返回新增工单")
//  @Transactional
//  public Result save(@RequestBody Workorder workorder, @LoginUser(isFull = true) SysUser user) {
//    workorder.setSubmitterName(user.getNickname());
//    workorder.setSubmitterUserId(user.getId());
//    Workorder workorder1 = iWorkorderService.saveWorker(workorder);
//
//    if (workorder == null) {
//      return Result.failed("添加工单失败");
//    }
//    return Result.succeed(workorder1, "添加成功");
//  }
//
//  /** 转交工单 */
//  @AuditLog(operation = "转交工单")
//  @PostMapping("/deliver")
//  @ApiOperation(value = "转交工单", notes = "转交工单")
//  @Transactional
//  public Result deliver(@RequestBody Map<String, Object> params) {
//    String processInstanceId = MapUtils.getString(params, "processInstanceId");
//    Long id = MapUtils.getLong(params, "id");
//    Long deliverId = MapUtils.getLong(params, "deliverId");
//    String deliverName = MapUtils.getString(params, "deliverName");
//    Task task =
//        taskService
//            .createTaskQuery()
//            .processInstanceId(processInstanceId)
//            .processInstanceBusinessKey(id.toString())
//            .singleResult();
//    taskService.setAssignee(task.getId(), deliverId.toString());
//    Workorder workorder = new Workorder();
//    workorder.setId(id);
//    workorder.setAcceptUserId(deliverId);
//    workorder.setAcceptUserName(deliverName);
//    iWorkorderService.updateById(workorder);
//    return Result.succeed(workorder, "更新成功");
//  }
//
//  /** 删除工单 */
//  @AuditLog(operation = "删除工单")
//  @PostMapping("/delete")
//  @ApiOperation(value = "删除工单", notes = "删除工单")
//  @Transactional
//  public Result delete(@RequestBody Map<String, Object> params) {
//    String processInstanceId = MapUtils.getString(params, "processInstanceId");
//    ProcessInstance processInstance =
//        runtimeService
//            .createProcessInstanceQuery()
//            .processInstanceId(processInstanceId)
//            .singleResult();
//    Long id = MapUtils.getLong(params, "id");
//    runtimeService.deleteProcessInstance(processInstance.getProcessInstanceId(), "删除");
//    return Result.succeed(iWorkorderService.removeById(id), "删除成功");
//  }
//
//  @PostMapping(path = "findAllByPage")
//  @ApiOperation(value = "查询全部工单", notes = "查询全部工单")
//  public Result searchProcessInstance(@RequestBody Map<String, Object> params) {
//    Integer pages = MapUtils.getInteger(params, "page");
//    Integer limit = MapUtils.getInteger(params, "limit");
//    String name = MapUtils.getString(params, "name");
//    String createTime = MapUtils.getString(params, "createTime");
//    String acceptUserName = MapUtils.getString(params, "acceptUserName");
//    Page<Workorder> page = new Page<>(pages, limit);
//    QueryWrapper<Workorder> queryWrapper = new QueryWrapper<>();
//
//    if (StringUtils.isNotBlank(name)) {
//      queryWrapper.like("name", name);
//    }
//    if (StringUtils.isNotBlank(createTime)) {
//      queryWrapper.apply("date_format(create_time,'%Y-%m-%d') = {0}", createTime);
//    }
//
//    if (StringUtils.isNotBlank(acceptUserName)) {
//      queryWrapper.like("accept_user_name", acceptUserName);
//    }
//
//    Page<Workorder> workorderPage = iWorkorderService.page(page, queryWrapper);
//    return Result.succeed(workorderPage);
//  }
//
////  @PostMapping(path = "findTaskByAssignee")
////  @ApiOperation(value = "根据流程assignee查询当前人的个人任务", notes = "根据流程assignee查询当前人的个人任务")
////  public Result findTaskByAssignee(
////      @RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
////    Integer page = MapUtils.getInteger(params, "page");
////    Integer limit = MapUtils.getInteger(params, "limit");
////    Page<Workorder> pages = new Page<>(page, limit);
////    QueryWrapper queryWrapper = new QueryWrapper();
////    queryWrapper.orderByDesc("create_time");
////    queryWrapper.eq("accept_user_id", user.getId());
////    Page<Workorder> workorderPage = iWorkorderService.page(pages, queryWrapper);
////
////    if (workorderPage.getTotal() > 0) {
////      for (int i = 0; i < workorderPage.getTotal(); i++) {
////        Task task =
////            taskService
////                .createTaskQuery()
////                // 指定个人任务查询
////                .taskAssignee(user.getId().toString())
////                .processInstanceId(workorderPage.getRecords().get(i).getProcessInstanceId())
////                .singleResult();
////        if (task != null) {
////          workorderPage.getRecords().get(i).setTaskId(task.getId());
////        }
////      }
////    }
////    return Result.succeed(workorderPage);
////  }
//
////  /**
////   * 审核流程
////   *
////   * @param
////   * @return
////   */
////  @PostMapping("/audit")
////  @AuditLog(operation = "审核流程")
////  @ApiOperation(value = "审核流程:verify:true同意，false驳回", notes = "审核流程")
////  @Transactional
////  public Result completeTask(
////      @RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
////    String taskId = MapUtils.getString(params, "taskId");
////    Boolean verify = MapUtils.getBoolean(params, "verify");
////    Long id = MapUtils.getLong(params, "id");
////    String acceptReply = MapUtils.getString(params, "acceptReply");
////
////    Workorder workorder = iWorkorderService.getById(id);
////    Task task =
////        taskService
////            .createTaskQuery()
////            .processInstanceId(workorder.getProcessInstanceId())
////            .singleResult();
////    Map<String, Object> variables = new HashMap<String, Object>();
////    if (verify) {
////      workorder.setStatus(1);
////      workorder.setAcceptUserId(user.getId());
////      workorder.setAcceptUserName(user.getNickname());
////      workorder.setHandlingTime(new Date());
////      variables.put("status", 1);
////      if (StringUtils.isNotBlank(acceptReply)) {
////        workorder.setAcceptReply(acceptReply);
////      }
////      //taskService.complete(taskId, variables);
////      Boolean save = iWorkorderService.updateById(workorder);
////      DeviceMaintenanceRecord deviceMaintenanceRecord =
////          iDeviceMaintenanceRecordService.findByWorkorderId(id);
////      if (deviceMaintenanceRecord != null) {
////        deviceMaintenanceRecord.setMaintenanceTime(new Date());
////        iDeviceMaintenanceRecordService.save(deviceMaintenanceRecord);
////      }
////
////      return Result.succeed(save, "审核成功");
////    } else {
////      workorder.setStatus(4);
////      variables.put("status", 4);
////      Boolean save = iWorkorderService.updateById(workorder);
////      if (task != null && task.getId() != null) {
////        taskService.resolveTask(task.getId(), variables);
////      }
////      return Result.succeed(save, "驳回成功");
////    }
////  }
//
////  @PostMapping("/finish")
////  @AuditLog(operation = "完成流程")
////  @ApiOperation(value = "完成流程", notes = "审核流程")
////  @Transactional
////  public Result finishTask(
////      @RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
////    String id = MapUtils.getString(params, "id");
////
////    Workorder workorder = iWorkorderService.getById(id);
////    if (workorder.getStatus() == 0) {
////      return Result.failed("需要先审批该条工单");
////    }
////    String handlerReply = MapUtils.getString(params, "handlerReply");
////    if (StringUtils.isNotBlank(handlerReply)) {
////      workorder.setHandlerReply(handlerReply);
////    }
////    workorder.setStatus(2);
////    iWorkorderService.updateById(workorder);
////
////    Task task =
////        taskService
////            .createTaskQuery()
////            .processInstanceId(workorder.getProcessInstanceId())
////            .singleResult();
////    if (task != null) {
////      taskService.complete(task.getId());
////    }
////    return Result.succeed("完成审核成功");
////  }
//
//  @PostMapping("/operation")
//  @AuditLog(operation = "挂起和激活流程")
//  @ApiOperation(value = "挂起和激活流程")
//  @Transactional
//  public Result operation(@RequestBody Map<String, Object> params) {
//    String processInstanceId = MapUtils.getString(params, "processInstanceId");
//    Boolean type = MapUtils.getBoolean(params, "type");
//    // 查询流程状态  false在运行。true是挂起状态
//    boolean suspended =
//        runtimeService
//            .createProcessInstanceQuery()
//            .processInstanceId(processInstanceId)
//            .singleResult()
//            .isSuspended();
//    // 根据type获取  type=true激活。false挂起
//    if (type) {
//      // -------激活---------
//      if (!suspended) {
//        return Result.failed("流程已在激活状态");
//      } else {
//        // 说明是挂起状态，就可以激活操作
//        Long id = MapUtils.getLong(params, "id");
//        Workorder workorder = new Workorder();
//        workorder.setId(id);
//        workorder.setStatus(0);
//        iWorkorderService.updateById(workorder);
//        runtimeService.activateProcessInstanceById(processInstanceId);
//        return Result.succeed("流程定义：" + processInstanceId + "激活");
//      }
//
//    } else {
//      if (suspended) {
//        return Result.failed("流程已在挂起状态");
//      } else {
//        Long id = MapUtils.getLong(params, "id");
//        Workorder workorder = new Workorder();
//        workorder.setId(id);
//        workorder.setStatus(3);
//        iWorkorderService.updateById(workorder);
//        runtimeService.suspendProcessInstanceById(processInstanceId);
//        return Result.succeed("流程定义：" + processInstanceId + "挂起");
//      }
//    }
//  }
//
//  @PostMapping("queryHistoryWithAssignee")
//  @AuditLog(operation = "我提交的")
//  @ApiOperation(value = "我提交的")
//  public Result queryHistoryWithAssignee(
//      @RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
//
//    Integer pages = MapUtils.getInteger(params, "page");
//    Integer limit = MapUtils.getInteger(params, "limit");
//
//    Page<Workorder> page = new Page<>(pages, limit);
//    QueryWrapper queryWrapper = new QueryWrapper();
//    queryWrapper.orderByDesc("create_time");
//    queryWrapper.eq("submitter_user_id", user.getId());
//    Page<Workorder> workorderPage = iWorkorderService.page(page, queryWrapper);
//    return Result.succeed(workorderPage);
//  }
//
//  /** 删除工单 */
//  @AuditLog(operation = "根据流程实例ID删除流程实例")
//  @PostMapping("/deleteProcessInstanceByID")
//  @ApiOperation(value = "根据流程实例ID删除流程实例", notes = "")
//  @Transactional
//  public Result deleteWorkorder(@RequestBody Map<String, Object> params) {
//    String id = MapUtils.getString(params, "id");
//    String ProcessInstanceId = MapUtils.getString(params, "ProcessInstanceId");
//    runtimeService.deleteProcessInstance(ProcessInstanceId, "删除" + ProcessInstanceId);
//    return Result.succeed(iWorkorderService.removeById(id));
//  }
//
//  @PostMapping("queryHistoryAll")
//  @AuditLog(operation = "我完成的工单")
//  @ApiOperation(value = "我完成的工单")
//  public Result queryHistoryAll(@RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
//    Integer pages = MapUtils.getInteger(params, "page");
//    Integer limit = MapUtils.getInteger(params, "limit");
//
//    Page<Workorder> page = new Page<>(pages, limit);
//    QueryWrapper<Workorder> queryWrapper = new QueryWrapper<>();
//    queryWrapper.orderByDesc("create_time");
//    queryWrapper.eq("status", 1).or().eq("status", 4);
//    queryWrapper.eq("accept_user_id", user.getId());
//    Page<Workorder> workorderPage = iWorkorderService.page(page, queryWrapper);
//    return Result.succeed(workorderPage);
//  }
//
//  /** 查询某一个流程历史进程 */
//  @AuditLog(operation = "查询某一个流程历史进程")
//  @PostMapping("/findHistoryProcessInstance")
//  @ApiOperation(value = "查询某一个流程历史进程", notes = "")
//  @Transactional
//  public Result findHistoryProcessInstance(@RequestBody Map<String, Object> params) {
//    String processInstanceId = MapUtils.getString(params, "processInstanceId");
//    List<HistoricActivityInstance> list =
//        historyService
//            .createHistoricActivityInstanceQuery()
//            .processInstanceId(processInstanceId)
//            .orderByHistoricActivityInstanceId()
//            .asc()
//            .list();
//
//    if (list != null && list.size() > 0) {
//      List<Map<String, String>> resultList = new ArrayList<>();
//      for (HistoricActivityInstance task : list) {
//        Map<String, String> resultMap = new HashMap<>(7);
//        resultMap.put("taskId", task.getId());
//        resultMap.put("processInstanceId:", task.getProcessInstanceId());
//        resultMap.put("activityName", task.getActivityName());
//        if (task.getAssignee() != null) {
//          resultMap.put("assignee", task.getAssignee());
//          long userId = Long.valueOf(task.getAssignee());
//          SysUser user = userService.findUserById(userId);
//          if (user != null) {
//            resultMap.put("userName", user.getNickname());
//          }
//        }
//        if (task.getEndTime() != null) {
//          resultMap.put("startTime", task.getStartTime().toString());
//        }
//        if (task.getEndTime() != null) {
//          resultMap.put("endTime", task.getEndTime().toString());
//        }
//        resultList.add(resultMap);
//      }
//      return Result.succeed(resultList, "查询成功");
//    }
//    return Result.succeed("查询成功");
//  }
//
//  /** 获取流程图 */
//  @AuditLog(operation = "获取流程图")
//  @GetMapping(value = "/getImage", produces = MediaType.IMAGE_JPEG_VALUE)
//  @ApiOperation(value = "获取流程图", notes = "")
//  @Transactional
//  public byte[] getImage(@RequestParam Map<String, Object> params) throws IOException {
//    String processInstanceId = MapUtils.getString(params, "processInstanceId");
//    ProcessInstance processInstance =
//        runtimeService
//            .createProcessInstanceQuery()
//            .processInstanceId(processInstanceId)
//            .singleResult();
//    ProcessDefinition processDefinition =
//        repositoryService
//            .createProcessDefinitionQuery()
//            .processDefinitionId(processInstance.getProcessDefinitionId())
//            .singleResult();
//    String diagramResourceName = processDefinition.getDiagramResourceName();
//    InputStream imageStream =
//        repositoryService.getResourceAsStream(
//            processDefinition.getDeploymentId(), diagramResourceName);
//    byte[] bytes = new byte[imageStream.available()];
//    imageStream.read(bytes, 0, imageStream.available());
//    return bytes;
//  }
//}
