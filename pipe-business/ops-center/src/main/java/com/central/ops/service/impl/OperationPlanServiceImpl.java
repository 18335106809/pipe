package com.central.ops.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.central.ops.mapper.OperationPlanMapper;
import com.central.ops.model.OperationPlan;
import com.central.ops.service.IOperationPlanService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-02-21
 */
@Service
public class OperationPlanServiceImpl extends ServiceImpl<OperationPlanMapper, OperationPlan> implements IOperationPlanService {

}
