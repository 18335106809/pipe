package com.central.ops.service.fallback;

import com.central.ops.model.LableManage;
import com.central.ops.service.AssetsService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @ClassName: AssetsServiceFallbackFactory
 * @Description: Todo
 *
 * @data: 2020/5/15  15:41
 */
@Slf4j
@Component
public class AssetsServiceFallbackFactory implements FallbackFactory<AssetsService> {

    @Override
    public AssetsService create(Throwable throwable) {
        return new AssetsService() {
            @Override
            public LableManage findLableById(Long id) {
                log.error("调用findByRoleCodes异常：{}", id, throwable);
                return new LableManage();
            }
        };
    }
}