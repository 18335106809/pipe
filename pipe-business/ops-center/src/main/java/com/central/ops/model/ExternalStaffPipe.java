package com.central.ops.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExternalStaffPipe extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 姓名
     */
    private String name;

    /**
     * 证件类型
     */
    private String cardType;

    /**
     * 证件编号
     */
    private String cardNumber;

    /**
     * 电话
     */
    private String phone;

    private String location;

    /**
     * 规定结束时间
     */
    private Date scheduleEndTime;

    /**
     * 来访开始时间
     */
    private Date visitStartTime;
    /**
     * 来访结束时间
     */
    private Date visitEndTime;
    private Long lableId;

    private Integer status;

    private String accompanyingPersonName;
    private Long accompanyingPersonId;
    @TableField(exist = false)
    private String  lableNumber;


    private String groupApprovalName;
    private Date groupApprovalTime;
    private String leaderApprovalName;
    private Date leaderApprovalTime;


}
