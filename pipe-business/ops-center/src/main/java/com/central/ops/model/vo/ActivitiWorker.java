package com.central.ops.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: ActivitiWorker
 * @Description: Todo
 *
 * @data: 2020/5/9  17:48
 */
@Data
public class ActivitiWorker {


    private String  processInstanceId;
    private String processDefinitionKey;
    private String id;
    /**
     * 工单名称
     */
    private String title;

    /**
     * 工单类型
     */
    private String type;

    /**
     * 提交人
     */
    private String submitterName;
    /**
     * 提交人id
     */
    private Long submitterUserId;
    /**
     * 提交人id
     */
    private String mobile;

    /**
     * 受理人
     */
    private Long acceptUserId;
    /**
     * 受理人
     */
    private String acceptUserName;

    /**
     * 处理人
     */
    private String handlerName;
    /**
     * 处理人
     */
    private Long handlerUserId;
    /**
     * 处理时间
     */
    private Date handlingTime;
    /**
     * 等级
     */
    private String level;

    /**
     * 工单状态(0未审批，1已审批，2工单完成)
     */
    private Integer status;
    private Date createTime;
}
