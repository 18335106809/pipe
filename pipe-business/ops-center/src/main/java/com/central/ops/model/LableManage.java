package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LableManage extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 编号
     */
    @Excel(name = "编号", height = 20, width = 30, isImportField = "true_st")
    private String number;

    /**
     * 名称
     */
    @Excel(name = "名称", height = 20, width = 30, isImportField = "true_st")
    private String name;

    /**
     * 类型
     */
    @Excel(name = "类型", height = 20, width = 30, isImportField = "true_st")
    private String type;

    /**
     * 借出状态 
     */
    @Excel(name = "借出状态", height = 20, width = 30, isImportField = "true_st")
    private String lendState;

    /**
     * 标签状态
     */
    @Excel(name = "标签状态", height = 20, width = 30, isImportField = "true_st")
    private String lableState;


}
