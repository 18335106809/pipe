package com.central.ops.service;

import com.central.ops.model.EvaluationStandard;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-22
 */
public interface IEvaluationStandardService extends IService<EvaluationStandard> {

}
