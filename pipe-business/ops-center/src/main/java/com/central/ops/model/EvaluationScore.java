package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class EvaluationScore extends SuperEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 得分
     */
    @Excel(name = "得分", height = 20, width = 30, isImportField = "true_st")
    private String actualScore;

    /**
     * 用户id
     */
    @Excel(name = "用户id", height = 20, width = 30, isImportField = "true_st")
    private Long userId;

    /**
     * 用户名字
     */
    @Excel(name = "用户名字", height = 20, width = 30, isImportField = "true_st")
    private String userName;

    /**
     * 描述信息
     */
    @Excel(name = "描述信息", height = 20, width = 30, isImportField = "true_st")
    private String description;

    /**
     * 模板id
     */
    private Long evaluationStandardId;
}
