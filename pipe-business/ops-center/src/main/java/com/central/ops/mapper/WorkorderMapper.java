package com.central.ops.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.Workorder;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-02-17
 */
@Mapper
public interface WorkorderMapper extends BaseMapper<Workorder> {

}
