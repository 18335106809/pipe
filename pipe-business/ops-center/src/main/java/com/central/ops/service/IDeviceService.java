package com.central.ops.service;


import com.central.common.constant.ServiceNameConstants;
import com.central.ops.model.vo.Device;
import com.central.ops.service.fallback.DeviceServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = ServiceNameConstants.DEVICE_SERVICE,fallbackFactory = DeviceServiceFallbackFactory.class, decode404 = true)
public interface IDeviceService {


    @GetMapping(value = "/device/findById")
    Device findById(@RequestParam("id") Long id);
}
