package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-02-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "operation_plan")
public class OperationPlan extends SuperEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 计划名称
     */
    @Excel(name = "计划名称", height = 20, width = 30, isImportField = "true_st")
    private String name;

    /**
     * 计划内容
     */
    @Excel(name = "计划内容", height = 20, width = 30, isImportField = "true_st")
    private String content;

    /**
     * 地点
     */
    @Excel(name = "地点", height = 20, width = 30, isImportField = "true_st")
    private String site;

    /**
     * 计划当前状态
     */
    @Excel(name = "计划当前状态", height = 20, width = 30, isImportField = "true_st")
    private Integer status;

    /**
     * 类型
     */
    @Excel(name = "类型", height = 20, width = 30, isImportField = "true_st")
    private String type;


    @Excel(name = "循环类型", height = 20, width = 30, isImportField = "true_st")
    private int cyclesType;
    @Excel(name = "循环时间", height = 20, width = 30, isImportField = "true_st")
    private int cyclesTime;
    /**
     * 计划开始时间
     */
    @Excel(name = "计划开始时间", height = 20, width = 30, isImportField = "true_st")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date planStartTime;

    /**
     * 计划完成时间
     */
    @Excel(name = "计划完成时间", height = 20, width = 30, isImportField = "true_st")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date planEndTime;




    /**
     * 制定计划人姓名
     */
    @Excel(name = "制定计划人姓名", height = 20, width = 30, isImportField = "true_st")
    private String formulateName;
    /**
     * 制定计划人姓名
     */
    @Excel(name = "制定计划人姓名", height = 20, width = 30, isImportField = "true_st")
    private Long formulateUserId;

    /**
     * 工单模板id
     */
    private Integer workorderTemplateId;

    private String workorderId;
}
