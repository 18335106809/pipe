package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.WorkorderTemplate;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-14
 */
public interface IWorkorderTemplateService extends IService<WorkorderTemplate> {

}
