package com.central.ops.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.ops.mapper.SysLoggerMapper;
import com.central.ops.model.SysLogger;
import com.central.ops.service.ISysLoggerService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-21
 */
@Service
public class SysLoggerServiceImpl extends ServiceImpl<SysLoggerMapper, SysLogger> implements ISysLoggerService {

}
