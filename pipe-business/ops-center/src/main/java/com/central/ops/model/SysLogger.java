package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.central.common.model.SuperEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-21
 */
@Data
@Accessors(chain = true)
public class SysLogger implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 应用名
     */
    @Excel(name = "应用名", height = 20, width = 30, isImportField = "true_st")
    private String applicationName;

    /**
     * 类名
     */
    @Excel(name = "类名", height = 20, width = 30, isImportField = "true_st")
    private String className;

    /**
     * 方法名
     */
    @Excel(name = "方法名", height = 20, width = 30, isImportField = "true_st")
    private String methodName;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    @Excel(name = "用户名", height = 20, width = 30, isImportField = "true_st")
    private String userName;

    /**
     * 租户id
     */
    private String clientId;

    /**
     * 操作信息
     */
    @Excel(name = "操作信息", height = 20, width = 30, isImportField = "true_st")
    private String operation;

    /**
     * 创建时间
     */
    private String timestamp;


}
