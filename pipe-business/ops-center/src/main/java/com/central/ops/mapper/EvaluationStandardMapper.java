package com.central.ops.mapper;

import com.central.ops.model.EvaluationStandard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-22
 */
public interface EvaluationStandardMapper extends BaseMapper<EvaluationStandard> {

}
