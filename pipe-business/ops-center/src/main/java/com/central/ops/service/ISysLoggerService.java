package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.SysLogger;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-21
 */
public interface ISysLoggerService extends IService<SysLogger> {

}
