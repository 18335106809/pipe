package com.central.ops.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-11
 */
@Controller
@RequestMapping("/WorkorderRecord")
public class WorkorderRecordController {

}
