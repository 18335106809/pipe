package com.central.ops.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginUser;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.model.EvaluationStandard;
import com.central.ops.model.ExternalStaffPipe;
import com.central.ops.model.LableManage;
import com.central.ops.model.StaffPipe;
import com.central.ops.service.AssetsService;
import com.central.ops.service.IExternalStaffPipeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.task.Task;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-05-12
 */
@Slf4j
@RestController
@RequestMapping("/ExternalStaffPipe")
@Api("外部人员入廊管理")
public class ExternalStaffPipeController {

    @Autowired
    private IExternalStaffPipeService iExternalStaffPipeService;

    @Autowired
    private AssetsService assetsService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或更新外部人员入廊申请")
    @AuditLog(operation = "添加或更新外部人员入廊申请")
    public Result save(@RequestBody ExternalStaffPipe externalStaffPipe){
        return Result.succeed(iExternalStaffPipeService.saveOrUpdate(externalStaffPipe),"保存成功");
    }

    @PostMapping("/audit")
    @AuditLog(operation = "审核流程")
    @ApiOperation(value = "审核流程:verify:true同意，false驳回", notes = "审核流程")
    @Transactional
    public Result completeTask(@RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
        Long id = MapUtils.getLong(params, "id");
        ExternalStaffPipe externalStaffPipe = iExternalStaffPipeService.getById(id);
        Boolean verify = MapUtils.getBoolean(params, "verify");

        if (verify) {
            //判断
            if (externalStaffPipe.getStatus()==1){
                externalStaffPipe.setLeaderApprovalName(user.getNickname());
                externalStaffPipe.setLeaderApprovalTime(new Date());
                externalStaffPipe.setStatus(2);
            }
            if (externalStaffPipe.getStatus()==0){
                externalStaffPipe.setGroupApprovalName(user.getNickname());
                externalStaffPipe.setGroupApprovalTime(new Date());
                externalStaffPipe.setStatus(1);
            }

            boolean save = iExternalStaffPipeService.updateById(externalStaffPipe);

            return Result.succeed(save, "审核成功");
        } else {
            if (externalStaffPipe.getStatus()==1){
                externalStaffPipe.setLeaderApprovalName(user.getNickname());
                externalStaffPipe.setLeaderApprovalTime(new Date());
                externalStaffPipe.setStatus(4);
            }
            if (externalStaffPipe.getStatus()==0){
                externalStaffPipe.setGroupApprovalName(user.getNickname());
                externalStaffPipe.setGroupApprovalTime(new Date());
                externalStaffPipe.setStatus(4);
            }

            boolean save = iExternalStaffPipeService.updateById(externalStaffPipe);
            return Result.succeed(save,"驳回成功");
        }
    }



    @GetMapping(value = "/delete")
    @ApiOperation(value = "根据id删除外部人员入廊申请")
    @AuditLog(operation = "根据id删除外部人员入廊申请")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iExternalStaffPipeService.removeById(id));
    }

    @ApiOperation(value = "根据id外部人员入廊申请")
    @AuditLog(operation = "根据id外部人员入廊申请")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iExternalStaffPipeService.getById(id),"获取成功");
    }

    @ApiOperation(value = "根据id外部人员入廊离开")
    @AuditLog(operation = "根据id外部人员入廊离开")
    @GetMapping(value = "/theyLeft")
    public Result theyLeft(@RequestParam("id") Long id) {
        ExternalStaffPipe externalStaffPipe = new ExternalStaffPipe();
        externalStaffPipe.setId(id);
        externalStaffPipe.setStatus(3);
        return Result.succeed(iExternalStaffPipeService.updateById(externalStaffPipe),"更新成功");
    }

    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<ExternalStaffPipe> page = new Page<>(pages,limit);
        QueryWrapper<ExternalStaffPipe> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (name != null) {
            queryWrapper.like("name",name);
        }
        String phone = MapUtils.getString(params, "phone");
        if (phone !=null) {
            queryWrapper.like("phone",phone);
        }
        Integer status = MapUtils.getInteger(params, "status");
        if (status !=null) {
            queryWrapper.eq("status",status);
        }
        String location = MapUtils.getString(params, "location");
        if (location !=null) {
            queryWrapper.like("location",location);
        }

       Page<ExternalStaffPipe> externalStaffPipePage = iExternalStaffPipeService.page(page, queryWrapper);
        for (int i =0;i<externalStaffPipePage.getRecords().size();i++){
            Long id =  externalStaffPipePage.getRecords().get(i).getLableId();
            LableManage lableManage = assetsService.findLableById(id);
            externalStaffPipePage.getRecords().get(i).setLableNumber(lableManage.getNumber()) ;
        }

        return Result.succeed(externalStaffPipePage);
    }


    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/list")
    public Result  list(@RequestBody Map<String, Object> params) {
        QueryWrapper<ExternalStaffPipe> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (name != null) {
            queryWrapper.like("name",name);
        }
        String phone = MapUtils.getString(params, "phone");
        if (phone !=null) {
            queryWrapper.like("phone",phone);
        }
        Integer status = MapUtils.getInteger(params, "status");
        if (status !=null) {
            queryWrapper.eq("status",status);
        }
        String location = MapUtils.getString(params, "location");
        if (location !=null) {
            queryWrapper.like("location",location);
        }
        List<ExternalStaffPipe> externalStaffPipePage = iExternalStaffPipeService.list( queryWrapper);
        return Result.succeed(externalStaffPipePage);
    }


    @GetMapping("/export")
    public void exportUser( HttpServletResponse response) throws IOException {
        List<ExternalStaffPipe> result = iExternalStaffPipeService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "外部人员入廊管理", ExternalStaffPipe.class, "外部人员入廊管理", response);
    }


}
