package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.EvaluationProjectManage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-02-26
 */
public interface IEvaluationProjectManageService extends IService<EvaluationProjectManage> {

}
