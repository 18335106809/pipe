package com.central.ops.job;

import com.central.common.utils.SpringUtil;
import com.central.ops.model.Workorder;
import com.central.ops.service.IWorkorderService;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 * @ClassName: workerJob
 * @Description: Todo
 *
 * @data: 2020/5/19  18:45
 */

public class WorkerJob implements Job {



    @Override
    public void execute(JobExecutionContext context) {

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        String title = dataMap.getString("title");
        String type = dataMap.getString("type");
        String acceptUserName = dataMap.getString("acceptUserName");
        Long acceptUserId = dataMap.getLong("acceptUserId");
        String level = dataMap.getString("level");

        String mobile = dataMap.getString("mobile");
        Long submitterUserId = dataMap.getLong("submitterUserId");
        String submitterName = dataMap.getString("submitterName");
        String article = dataMap.getString("article");
        //保存工单信息------------
        Workorder workorder = new Workorder();
        workorder.setTitle(title);
        workorder.setType(type);
        workorder.setAcceptUserName(acceptUserName);
        workorder.setAcceptUserId(acceptUserId);
        workorder.setLevel(level);
        workorder.setSubmitterUserId(submitterUserId);
        workorder.setSubmitterName(submitterName);
        workorder.setMobile(mobile);
        workorder.setArticle(article);
        IWorkorderService iWorkorderService = SpringUtil.getBean(IWorkorderService.class);
        iWorkorderService.saveWorker(workorder);

    }
}
