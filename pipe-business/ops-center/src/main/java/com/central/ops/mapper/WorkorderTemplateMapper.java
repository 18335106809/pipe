package com.central.ops.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.WorkorderTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-14
 */
@Mapper
public interface WorkorderTemplateMapper extends BaseMapper<WorkorderTemplate> {

}
