package com.central.ops.mapper;

import com.central.ops.model.WorkorderRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-11
 */
public interface WorkorderRecordMapper extends BaseMapper<WorkorderRecord> {

}
