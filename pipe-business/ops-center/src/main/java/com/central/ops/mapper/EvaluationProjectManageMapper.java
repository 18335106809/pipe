package com.central.ops.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.EvaluationProjectManage;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-02-26
 */
@Mapper
public interface EvaluationProjectManageMapper extends BaseMapper<EvaluationProjectManage> {

}
