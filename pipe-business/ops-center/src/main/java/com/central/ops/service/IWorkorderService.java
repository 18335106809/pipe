package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.common.model.SysUser;
import com.central.ops.model.Workorder;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-02-17
 */
public interface IWorkorderService extends IService<Workorder> {


    Workorder saveWorker(Workorder workorder);
}
