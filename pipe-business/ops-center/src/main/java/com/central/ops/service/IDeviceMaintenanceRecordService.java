package com.central.ops.service;

import com.central.common.constant.ServiceNameConstants;
import com.central.common.model.Result;
import com.central.ops.model.vo.DeviceMaintenanceRecord;
import com.central.ops.service.fallback.DeviceServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-21
 */
@FeignClient(name = ServiceNameConstants.DEVICE_SERVICE,fallbackFactory = DeviceServiceFallbackFactory.class, decode404 = true)
public interface IDeviceMaintenanceRecordService {


    @PostMapping(value = "/DeviceMaintenanceRecord/saveOrUpdate")
    Result save(@RequestBody DeviceMaintenanceRecord deviceMaintenanceRecord);

    @GetMapping(value = "/DeviceMaintenanceRecord/findByWorkorderId")
    DeviceMaintenanceRecord findByWorkorderId(@RequestParam("id") Long id);

}
