package com.central.ops.model;

import com.central.common.model.SuperEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class WorkorderRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;
    /**
     * 工单id
     */
    private Long workorderId;

    /**
     * 类型(1发起工单，2接受工单，3转交工单，4确认工单，5驳回工单)
     */
    private Integer type;

    /**
     * 转交人（如果是转交的话，）
     */
    private String deliverToName;

    /**
     * 接受人
     */
    private String userName;


}
