package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.OperationPlan;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-02-21
 */

public interface IOperationPlanService extends IService<OperationPlan> {

}
