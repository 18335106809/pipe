package com.central.ops.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: EvaluationHistory
 * @Description: Todo
 *
 * @data: 2020/6/2  13:21
 */

    @Data
    @Accessors(chain = true)
    @ApiModel(description = "绩效")
    public class EvaluationHistory  implements Serializable {

        private static final long serialVersionUID = 1L;



        private Double score;
        private Double actualScore;

        @ApiModelProperty(value ="用户id")
        private Long userId;

        @ApiModelProperty(value ="用户名字")
        private String userName;
        private Long deptId;
    private Date createTime;
    }