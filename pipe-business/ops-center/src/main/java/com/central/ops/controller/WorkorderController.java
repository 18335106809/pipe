package com.central.ops.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginUser;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.model.Workorder;
import com.central.ops.model.WorkorderRecord;
import com.central.ops.service.IWorkorderRecordService;
import com.central.ops.service.IWorkorderService;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 前端控制器
 *
 * @since 2020-02-17
 */
@RestController
@RequestMapping("/workorder")
@Api(tags = "工单模块api")
public class WorkorderController {

  @Resource private IWorkorderService iWorkorderService;

  @Autowired private IWorkorderRecordService iWorkorderRecordService;
  /** 新增工单管理 */
  @AuditLog(operation = "工单管理")
  @PostMapping("/add")
  @ApiOperation(value = "新增工单", notes = "返回新增工单")
  public Result addWorkorderSave(
      @RequestBody @Validated Workorder workorder, @LoginUser(isFull = true) SysUser user) {
    workorder.setSubmitterName(user.getNickname());
    workorder.setSubmitterUserId(user.getId());
    workorder.setMobile(user.getMobile());
    workorder.setStatus(1);
    return Result.succeed(iWorkorderService.saveWorker(workorder), " 新增成功");
  }

  /** 转交工单 */
  @AuditLog(operation = "转交工单")
  @PostMapping("/deliver")
  @ApiOperation(value = "转交工单", notes = "转交工单")
  @Transactional
  public Result deliver(@RequestBody Map<String, Object> params) {
    Long id = MapUtils.getLong(params, "id");
    Long deliverId = MapUtils.getLong(params, "deliverId");
    String deliverName = MapUtils.getString(params, "deliverName");
    Workorder workorder = iWorkorderService.getById(id);
    WorkorderRecord workorderRecord = new WorkorderRecord();
    workorderRecord.setWorkorderId(workorder.getId());
    workorderRecord.setUserName(deliverName);
    workorderRecord.setDeliverToName(workorder.getAcceptUserName());
    workorderRecord.setType(3);
    iWorkorderRecordService.save(workorderRecord);
    workorder.setAcceptUserId(deliverId);
    workorder.setAcceptUserName(deliverName);
    iWorkorderService.updateById(workorder);
    return Result.succeed(workorder, "更新成功");
  }

  /** 工单列表 */
  @AuditLog(operation = "全部工单列表")
  @PostMapping("/page")
  @ApiOperation(value = "全部工单列表", notes = "返回工单列表")
  public Result WorkorderPage(@RequestBody Map<String, Object> params) {
    Integer pages = MapUtils.getInteger(params, "page");
    Integer limit = MapUtils.getInteger(params, "limit");
    String title = MapUtils.getString(params, "title");
    String createTime = MapUtils.getString(params, "createTime");
    String acceptUserName = MapUtils.getString(params, "acceptUserName");
    Page<Workorder> page = new Page<>(pages, limit);
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");

    if (StringUtils.isNotBlank(title)) {
      queryWrapper.like("title", title);
    }
    if (StringUtils.isNotBlank(createTime)) {
      queryWrapper.apply("date_format(create_time,'%Y-%m-%d') = {0}", createTime);
    }
    if (StringUtils.isNotBlank(acceptUserName)) {
      queryWrapper.like("accept_user_name", acceptUserName);
    }

    return Result.succeed(iWorkorderService.page(page, queryWrapper));
  }

  /** 工单列表 */
  @AuditLog(operation = "全部工单列表")
  @PostMapping("/list")
  @ApiOperation(value = "全部工单列表", notes = "返回工单列表")
  public Result list(@RequestBody Map<String, Object> params) {

    String title = MapUtils.getString(params, "title");
    String createTime = MapUtils.getString(params, "createTime");
    String acceptUserName = MapUtils.getString(params, "acceptUserName");
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    if (StringUtils.isNotBlank(title)) {
      queryWrapper.like("title", title);
    }
    if (StringUtils.isNotBlank(createTime)) {
      queryWrapper.apply("date_format(create_time,'%Y-%m-%d') = {0}", createTime);
    }
    if (StringUtils.isNotBlank(acceptUserName)) {
      queryWrapper.like("accept_user_name", acceptUserName);
    }
    return Result.succeed(iWorkorderService.list( queryWrapper));
  }

  @AuditLog(operation = "接受或驳回确认")
  @PostMapping("/approval")
  @ApiOperation(value = "接受或驳回,type=2接受 4确认 5驳回", notes = "")
  public Result approvalWorkorder(@RequestBody Map<String, Object> params) {
    Long id = MapUtils.getLong(params, "id");
    Integer type = MapUtils.getInteger(params, "status");
    String reply = MapUtils.getString(params, "acceptReply");
    Workorder workorder = iWorkorderService.getById(id);
    workorder.setStatus(type);
    if (reply != null) {
      workorder.setAcceptReply(reply);
    }

    WorkorderRecord workorderRecord = new WorkorderRecord();
    workorderRecord.setType(type);
    workorderRecord.setWorkorderId(id);
    workorderRecord.setUserName(workorder.getAcceptUserName());
    iWorkorderRecordService.save(workorderRecord);
    return Result.succeed(iWorkorderService.updateById(workorder), "成功");
  }

  /** 我发起的 */
  @AuditLog(operation = "查询我发起的工单")
  @PostMapping("/started")
  @ApiOperation(value = "查询我发起的工单", notes = "返回工单列表")
  public Result WorkorderStarted(
      @RequestBody PageVo pageVo, @LoginUser(isFull = true) SysUser user) {
    Page<Workorder> page = new Page<>(pageVo.getPage(), pageVo.getLimit());
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    queryWrapper.eq("submitter_user_id", user.getId());
    return Result.succeed(iWorkorderService.page(page, queryWrapper));
  }

  @AuditLog(operation = "查询需要我受理的工单")
  @PostMapping("/accept")
  @ApiOperation(value = "查询我受理的工单", notes = "返回工单列表")
  public Result WorkorderAccept(@RequestBody PageVo pageVo, @LoginUser SysUser user) {
    Page<Workorder> page = new Page<>(pageVo.getPage(), pageVo.getLimit());
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    queryWrapper.eq("accept_user_id", user.getId());
    return Result.succeed(iWorkorderService.page(page, queryWrapper));
  }

  /** 我完结的 */
  @AuditLog(operation = "我完结的")
  @PostMapping("/finish")
  @ApiOperation(value = "我完结的", notes = "返回工单列表")
  public Result WorkorderFinish(
      @RequestBody PageVo pageVo, @LoginUser(isFull = true) SysUser user) {
    Page<Workorder> page = new Page<>(pageVo.getPage(), pageVo.getLimit());
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    queryWrapper.eq("accept_user_id", user.getId());
    queryWrapper.eq("status", 4);
    return Result.succeed(iWorkorderService.page(page, queryWrapper));
  }

  /** 删除工单 */
  @AuditLog(operation = "删除工单")
  @DeleteMapping("/delete")
  @ApiOperation(value = "删除工单", notes = "")
  public Result deleteWorkorder(@RequestParam Long id) {
    return Result.succeed(iWorkorderService.removeById(id));
  }

  /** 流程进度 */
  @AuditLog(operation = "流程进度")
  @PostMapping("/activiti")
  @ApiOperation(value = "流程进度", notes = "")
  public Result activiti(@RequestBody Map<String, Object> params) {
    Long id = MapUtils.getLong(params, "id");
    List<WorkorderRecord> workorderRecords =
        iWorkorderRecordService.list(new QueryWrapper<WorkorderRecord>().eq("workorder_id", id));
    return Result.succeed(workorderRecords);
  }

  /** 保存或更新 */
  @AuditLog(operation = "保存或更新")
  @PostMapping("/saveOrUpdate")
  @ApiOperation(value = "保存或更新", notes = "")
  public Result deleteWorkorder(@RequestBody Workorder workorder) {
    return Result.succeed(iWorkorderService.saveOrUpdate(workorder));
  }

//  /**
//   * 导出excel
//   *
//   * @return
//   */
  @GetMapping("/export")
  public void exportUser(@RequestBody Map<String, Object> params,HttpServletResponse response) throws IOException {
    String startTime = MapUtils.getString(params, "startTime");
    String endTime = MapUtils.getString(params, "endTime");
    QueryWrapper queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    if (StringUtils.isNotBlank(startTime)) {
      queryWrapper.apply("date_format(create_time,'%Y-%m-%d') >= {0}", startTime);
    }
    if (StringUtils.isNotBlank(endTime)) {
      queryWrapper.apply("date_format(create_time,'%Y-%m-%d') <= {0}", endTime);
    }
    List<Workorder> result = iWorkorderService.list(queryWrapper);
    //导出操作
    ExcelUtil.exportExcel(result, null, "工单", Workorder.class, "Workorder", response);
  }

//  @PostMapping(value = "/users/import")
//  public Result importExcl(@RequestParam("file") MultipartFile excl) throws Exception {
//    int rowNum = 0;
//    if(!excl.isEmpty()) {
//      List<SysUserExcel> list = ExcelUtil.importExcel(excl, 0, 1, SysUserExcel.class);
//      rowNum = list.size();
//      if (rowNum > 0) {
//        List<SysUser> users = new ArrayList<>(rowNum);
//        list.forEach(u -> {
//          SysUser user = new SysUser();
//          BeanUtil.copyProperties(u, user);
//          user.setPassword(CommonConstant.DEF_USER_PASSWORD);
//          user.setType(UserType.BACKEND.name());
//          users.add(user);
//        });
//        appUserService.saveBatch(users);
//      }
//    }
//    return Result.succeed("导入数据成功，一共【"+rowNum+"】行");
//  }


}
