package com.central.ops.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.OperationPlan;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-02-21
 */
@Mapper
public interface OperationPlanMapper extends BaseMapper<OperationPlan> {

}
