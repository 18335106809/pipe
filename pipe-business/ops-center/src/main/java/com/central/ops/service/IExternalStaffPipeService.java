package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.ExternalStaffPipe;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-12
 */
public interface IExternalStaffPipeService extends IService<ExternalStaffPipe> {

}
