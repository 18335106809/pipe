package com.central.ops.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.SysLogger;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-21
 */
public interface SysLoggerMapper extends BaseMapper<SysLogger> {

}
