package com.central.ops.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.ops.model.EvaluationScore;
import com.central.ops.model.EvaluationStandard;
import com.central.ops.service.IEvaluationScoreService;
import com.central.ops.service.IEvaluationStandardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 前端控制器
 *
 * @since 2020-05-22
 */
@RestController
@RequestMapping("/EvaluationStandard")
@Api(tags = "绩效标准api")
public class EvaluationStandardController {

    @Autowired
    private IEvaluationStandardService iEvaluationStandardService;

  @ApiOperation(value = "查询全部信息")
  @PostMapping(value = "/list")
  public Result queryList(@RequestBody Map<String, Object> params) {
      String deptId = MapUtils.getString(params, "deptId");
      if (StringUtils.isBlank(deptId)) {
          return Result.failed("请先选择部门");
      }
        QueryWrapper<EvaluationStandard> queryWrapper = new QueryWrapper();
        queryWrapper.eq("dept_id", deptId);
        List<EvaluationStandard> evaluationStandards = iEvaluationStandardService.list(queryWrapper);

      return Result.succeed(treeBuilder(evaluationStandards));
      }

    @GetMapping("/export")
    public void exportUser( HttpServletResponse response) throws IOException {
        List<EvaluationStandard> result = iEvaluationStandardService.list();
        //导出操作
        ExcelUtil.exportExcel(treeBuilder(result), null, "绩效标准", EvaluationStandard.class, "绩效标准", response);
    }

    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或更新外部人员入廊申请")
    public Result save(@RequestBody EvaluationStandard evaluationStandard){
        return Result.succeed(iEvaluationStandardService.saveOrUpdate(evaluationStandard),"保存成功");
    }

    @PostMapping(value = "/saveOrUpdateBatch")
    @ApiOperation(value = "批量更新")
    public Result saveOrUpdateBatch(@RequestBody List<EvaluationStandard> evaluationStandard){
        return Result.succeed(iEvaluationStandardService.saveOrUpdateBatch(evaluationStandard),"保存成功");
    }

    @PostMapping(value = "/delete")
    @ApiOperation(value = "根据部门id删除")
    public Result deleteById(@RequestBody Map<String, Object> params) {
        String deptId = MapUtils.getString(params, "deptId");
        if (StringUtils.isBlank(deptId)) {
            return Result.failed("请选择部门");
        }
        String id = MapUtils.getString(params, "id");
        QueryWrapper<EvaluationStandard> queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", id);
        queryWrapper.eq("dept_id", deptId);
        return Result.succeed(iEvaluationStandardService.remove(queryWrapper));
    }



    public static List<EvaluationStandard> treeBuilder(List<EvaluationStandard> evaluationStandards) {
        List<EvaluationStandard> evaluations = new ArrayList<>();
        for (EvaluationStandard es : evaluationStandards) {
            if (es.getParentId().equals(-1L)) {
                evaluations.add(es);
            }
            for (EvaluationStandard evaluationStandard : evaluationStandards) {
                if (evaluationStandard.getParentId().equals(es.getId())) {
                    if (es.getSubEvaluationStandards() == null) {
                        es.setSubEvaluationStandards(new ArrayList<>());
                    }
                    es.getSubEvaluationStandards().add(evaluationStandard);
                }
            }
        }
        return evaluations;
    }




}

