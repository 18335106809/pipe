package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-02-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(description = "绩效")
public class EvaluationProjectManage extends SuperEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value ="绩效标准id")
    private Long evaluationStandardId ;

    @Excel(name = "实际得分", height = 20, width = 30, isImportField = "true_st")
   @ApiModelProperty(value ="实际得分")
    private Double actualScore;

   @ApiModelProperty(value ="用户id")
   @Excel(name = "用户id", height = 20, width = 30, isImportField = "true_st")
    private Long userId;

    @ApiModelProperty(value ="用户名字")
    @Excel(name = "用户名字", height = 20, width = 30, isImportField = "true_st")
    private String userName;

   @ApiModelProperty(value ="描述")
   @Excel(name = "描述", height = 20, width = 30, isImportField = "true_st")
    private String description;

}
