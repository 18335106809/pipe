package com.central.ops.model.vo;

import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DeviceMaintenanceRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;

    private Date maintenanceTime;

    /**
     * 维修时间
     */
    private String maintenanceContent;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备所在舱体
     */
    private String deviceCapsuleName;

    /**
     * 设备所在舱体区域位置
     */
    private String deviceLocation;

    /**
     * 维修人名字
     */
    private String maintenanceUserName;

    private Long workorderId;
}
