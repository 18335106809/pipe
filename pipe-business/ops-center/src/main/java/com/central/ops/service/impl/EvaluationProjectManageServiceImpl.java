package com.central.ops.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.ops.mapper.EvaluationProjectManageMapper;
import com.central.ops.model.EvaluationProjectManage;
import com.central.ops.service.IEvaluationProjectManageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-02-26
 */
@Service
public class EvaluationProjectManageServiceImpl extends ServiceImpl<EvaluationProjectManageMapper, EvaluationProjectManage> implements IEvaluationProjectManageService {

}
