package com.central.ops.model.vo;

import lombok.Data;

@Data
public class MyTaskDto {
    private String taskId;
    private String taskName;
    private String taskStatus;

}