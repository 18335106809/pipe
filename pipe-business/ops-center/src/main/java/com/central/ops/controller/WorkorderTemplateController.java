package com.central.ops.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.model.Workorder;
import com.central.ops.model.WorkorderTemplate;
import com.central.ops.service.IWorkorderTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: WorkorderTemplateController
 * @Description: Todo
 *
 * @data: 2020/5/14  11:41
 */
@RestController
@RequestMapping("/workorderTemplate")
@Api(tags = "工单模板api")
public class WorkorderTemplateController {


    @Resource
    private IWorkorderTemplateService iWorkorderTemplateService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加工单模板")
    @AuditLog(operation = "添加工单模板")
    public Result save(@RequestBody WorkorderTemplate workorderTemplate){
        return Result.succeed(iWorkorderTemplateService.saveOrUpdate(workorderTemplate));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除工单模板")
    @AuditLog(operation = "根据id删除工单模板")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iWorkorderTemplateService.removeById(id));
    }

    @ApiOperation(value = "根据id工单模板")
    @AuditLog(operation = "根据id工单模板")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iWorkorderTemplateService.getById(id));
    }


    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<WorkorderTemplate> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (name != null) {
            queryWrapper.like("name",name);
        }
        return Result.succeed(iWorkorderTemplateService.page(page,queryWrapper));
    }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<WorkorderTemplate> result = iWorkorderTemplateService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "工单模板", Workorder.class, "WorkorderTemplate", response);
    }

    @PostMapping(value = "/list")
    public Result  Querylist(@RequestBody Map<String, Object> params) {
        List<WorkorderTemplate> list = iWorkorderTemplateService.list();
        Boolean repair = MapUtils.getBoolean(params, "repair");
        if (repair!=null && repair) {


            List<WorkorderTemplate> collect = list.stream().filter(d ->  !(d.getName().equals("设备维修"))).collect(Collectors.toList());
            return Result.succeed(collect);
        }


        return Result.succeed(list);
    }
}
