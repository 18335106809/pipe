package com.central.ops.service.fallback;

import com.central.ops.model.vo.Device;
import com.central.ops.service.IDeviceService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName: DeviceServiceFallbackFactory
 * @Description: Todo
 * @data: 2020/6/15  12:05
 */

@Slf4j
@Component
public class DeviceServiceFallbackFactory implements FallbackFactory<IDeviceService> {
    @Override
    public IDeviceService create(Throwable throwable) {
        return id -> {
            log.info("device----------------"+id);
            log.error("调用findByRoleCodes异常：{}", id, throwable);
            return new Device();
        };
    }

}
