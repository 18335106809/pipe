package com.central.ops.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginUser;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.job.WorkerJob;
import com.central.ops.model.OperationPlan;
import com.central.ops.model.Workorder;
import com.central.ops.model.WorkorderTemplate;
import com.central.ops.service.IOperationPlanService;
import com.central.ops.service.IWorkorderService;
import com.central.ops.service.IWorkorderTemplateService;
import com.central.ops.utils.IdWorker;
import eu.bitwalker.useragentutils.OperatingSystem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-02-21
 */

@Slf4j
@RestController
@Api(tags = "运维计划模块api")
@RequestMapping("/operationplan")
public class OperationPlanController {

    @Resource
    private IOperationPlanService iOperationPlanService;
    @Resource
    private IWorkorderTemplateService iWorkorderTemplateService;
    @Resource
    private IWorkorderService iWorkorderService;
    @Autowired
    private Scheduler scheduler;


    @AuditLog(operation = "添加计划")
    @PostMapping("/save")
    @ApiOperation(value = "添加计划", notes="添加计划")
    @Transactional(rollbackFor = Exception.class)
    public Result saveOrUpdate(@RequestBody OperationPlan operationPlan, @LoginUser(isFull = true) SysUser user) throws SchedulerException {
        if(user.getId()==null){
            Result.failed("获取用户信息失败");
        }
        //更新运维计划
        operationPlan.setFormulateName(user.getNickname());
        operationPlan.setFormulateUserId(user.getId());
        operationPlan.setStatus(0);
        iOperationPlanService.save(operationPlan);

        WorkorderTemplate workorderTemplate = iWorkorderTemplateService.getById(operationPlan.getWorkorderTemplateId());
        //workorderTemplate.getFlowDiagram();
        //保存工单信息------------
        Workorder workorder = new Workorder();
        workorder.setTitle(operationPlan.getName());
        workorder.setType(operationPlan.getType());
        workorder.setAcceptUserName(workorderTemplate.getAcceptUserName());
        workorder.setAcceptUserId(workorderTemplate.getAcceptUserId());
        workorder.setLevel(workorderTemplate.getLevel());
        workorder.setSubmitterUserId(user.getId());
        workorder.setSubmitterName(user.getNickname());
        workorder.setType(operationPlan.getType());
        workorder.setMobile(user.getMobile());
        workorder.setArticle(operationPlan.getContent());
        Workorder workorder1 = iWorkorderService.saveWorker(workorder);

        //创建调度器Schedule
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();
        //创建JobDetail实例，并与OperationPlanController类绑定
        JobDetail jobDetail = JobBuilder.newJob(WorkerJob.class)
                .withIdentity(operationPlan.getId().toString(), "jobGroup1")
                .usingJobData("title", workorder1.getTitle())
                .usingJobData("type",workorder1.getType())
                .usingJobData("acceptUserName", workorder1.getAcceptUserName())
                .usingJobData("acceptUserId",workorder1.getAcceptUserId())
                .usingJobData("level", workorder1.getLevel())
                .usingJobData("submitterName",user.getNickname())
                .usingJobData("submitterUserId", user.getId())
                .usingJobData("mobile", user.getMobile())
                .usingJobData("article", workorder1.getArticle())
                .build();


        //创建触发器Trigger实例(立即执行，每隔60S执行一次)
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(operationPlan.getId().toString(), "triggerGroup1")
                .startAt(operationPlan.getPlanStartTime())
                .endAt(operationPlan.getPlanEndTime())
                //.startNow()
                .withSchedule(SimpleScheduleBuilder
                                .simpleSchedule()
                               //.withIntervalInSeconds(10)
                              .withIntervalInHours(operationPlan.getCyclesTime())
                                .repeatForever()

                )
                .build();
        //开始执行
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();


        return  Result.succeed("添加计划成功");
    }

    @AuditLog(operation = "更新计划")
    @PostMapping("/update")
    @ApiOperation(value = "更新计划", notes="更新计划")
    @Transactional(rollbackFor = Exception.class)
    public Result update(@RequestBody OperationPlan operationPlan) {
        return  Result.succeed(iOperationPlanService.updateById(operationPlan),"更新计划成功");
    }


    @AuditLog(operation = "根据ID查找计划")
    @GetMapping("/find")
    @ApiOperation(value = "根据ID查找计划", notes="返回计划信息")
    public Result findOperationPlanById(@RequestParam("id") String id) {
        return  Result.succeed(iOperationPlanService.getById(id));
    }

    @AuditLog(operation = "删除计划")
    @GetMapping("/delete")
    @ApiOperation(value = "删除计划", notes="")
    public Result deleteOperationPlanById(@RequestParam("id") Long id) {
        JobKey jobKey= JobKey.jobKey(String.valueOf(id), "jobGroup1");
        TriggerKey triggerKey =  TriggerKey.triggerKey(String.valueOf(id),"jobGroup1");
        try {

            // 停止触发器
            scheduler.pauseTrigger(triggerKey);
            // 移除触发器
            scheduler.unscheduleJob(triggerKey);
            // 删除任务
            scheduler.deleteJob(jobKey);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failed("失败");
        }
        return  Result.succeed(iOperationPlanService.removeById(id),"删除计划成功");
    }

    @AuditLog(operation = "全部计划")
    @PostMapping("/page")
    @ApiOperation(value = "全部计划", notes="返回计划信息")
    public Result pageOperationPlan(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<OperationPlan> page = new Page<>(pages,limit);
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.orderByDesc("create_time");
            String name = MapUtils.getString(params, "name");
            if (name != null) {
                queryWrapper.like("name",name);
            }
            String content = MapUtils.getString(params, "content");
            if (content !=null) {
                queryWrapper.like("content",content);
            }
            Integer site = MapUtils.getInteger(params, "site");
            if (site !=null) {
                queryWrapper.like("site",site);
            }
            return Result.succeed(iOperationPlanService.page(page,queryWrapper));
        }


    @GetMapping("/export")
    public void exportUser( HttpServletResponse response) throws IOException {
        List<OperationPlan> result = iOperationPlanService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "运维计划管理", OperationPlan.class, "运维计划管理", response);
    }


}
