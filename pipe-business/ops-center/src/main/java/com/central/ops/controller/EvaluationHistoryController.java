package com.central.ops.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.annotation.LoginUser;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.ops.model.EvaluationScore;
import com.central.ops.model.EvaluationStandard;
import com.central.ops.model.vo.EvaluationHistory;
import com.central.ops.service.IEvaluationScoreService;
import com.central.ops.service.IEvaluationStandardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName: EvaluationHistory @Description: Todo
 *
 * @data: 2020/6/2 12:19
 */
@RestController
@RequestMapping("/EvaluationHistory")
@Api(tags = "绩效历史模块api")
public class EvaluationHistoryController {

    @Autowired
    private IEvaluationScoreService iEvaluationScoreService;
    @Autowired
    private IEvaluationStandardService iEvaluationStandardService;

      @PostMapping("/list")
      @ApiOperation(value = "根据ID查找计划", notes = "返回计划信息")
      public Result list(@LoginUser(isFull = true) SysUser sysUser) {
        QueryWrapper<EvaluationStandard> evaluationStandardQueryWrapper = new QueryWrapper();
        evaluationStandardQueryWrapper.eq("dept_id",sysUser.getDeptId());
        List<EvaluationStandard>  evaluationStandardList = iEvaluationStandardService.list(evaluationStandardQueryWrapper);
        double score =0;
        //遍历总共得分
        for (int i=0;i< evaluationStandardList.size();i++){
            score = Double.parseDouble(evaluationStandardList.get(i).getScore()) + score;
        }
        //遍历每一条数据
        QueryWrapper<EvaluationScore> scoreQueryWrapper = new QueryWrapper();
        scoreQueryWrapper.eq("user_id",sysUser.getId());
        List<EvaluationScore>  evaluationScoreList = iEvaluationScoreService.list(scoreQueryWrapper);
          //获取时间格式
          SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//这个是你要转成后的时间的格式
          //获取当天数据
        List<String> createTime = new LinkedList<>();
          for (int i=0;i< evaluationScoreList.size();i++){
              createTime.add(sdf.format(evaluationScoreList.get(i).getUpdateTime()));
          }
          //去重
          List<String> createTimes = createTime.stream().distinct().collect(Collectors.toList());
          List<EvaluationHistory> evaluationHistoryArrayList = new ArrayList<>();
          for (int i=0;i< createTimes.size();i++){
              double actualScore = 0;
              EvaluationHistory evaluationHistory = new EvaluationHistory();
              for (int j=0;j< evaluationScoreList.size();j++){
                    if (sdf.format(evaluationScoreList.get(j).getUpdateTime()).equals(createTimes.get(i))){
                        actualScore =  Double.parseDouble(evaluationScoreList.get(j).getActualScore()) + actualScore;
                    }
              }
              evaluationHistory.setScore(score);
              evaluationHistory.setActualScore(actualScore);
              evaluationHistory.setUserId(sysUser.getId());
              evaluationHistory.setUserName(sysUser.getNickname());
              evaluationHistory.setDeptId(sysUser.getDeptId());
              evaluationHistory.setCreateTime(evaluationScoreList.get(i).getUpdateTime());
              evaluationHistoryArrayList.add(evaluationHistory);
          }
          return Result.succeed(evaluationHistoryArrayList);
      }


}

