package com.central.ops.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.ops.model.EvaluationProjectManage;
import com.central.ops.model.SysLogger;
import com.central.ops.service.ISysLoggerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-05-21
 */
@RestController
@Api(tags = "系统日志api")
@RequestMapping("/SysLogger")
public class SysLoggerController {

    @Autowired
    private ISysLoggerService iSysLoggerService;


    @PostMapping(value = "/page")
    public Result QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<SysLogger> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String userName = MapUtils.getString(params, "userName");
        if (userName != null) {
            queryWrapper.like("user_name",userName);
        }
        String operation = MapUtils.getString(params, "operation");
        if (operation != null) {
            queryWrapper.like("operation",operation);
        }
        String timestamp = MapUtils.getString(params, "timestamp");
        if (timestamp != null) {
            queryWrapper.like("timestamp",timestamp);
        }
        return Result.succeed(iSysLoggerService.page(page,queryWrapper));
    }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<SysLogger> result = iSysLoggerService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "系统日志", SysLogger.class, "系统日志", response);
    }


}
