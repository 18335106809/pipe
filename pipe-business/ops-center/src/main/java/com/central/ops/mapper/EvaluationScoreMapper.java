package com.central.ops.mapper;

import com.central.ops.model.EvaluationScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-26
 */
@Mapper
public interface EvaluationScoreMapper extends BaseMapper<EvaluationScore> {

}
