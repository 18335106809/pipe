package com.central.ops.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.ops.model.StaffPipe;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-02-25
 */
public interface IStaffPipeService extends IService<StaffPipe> {

}
