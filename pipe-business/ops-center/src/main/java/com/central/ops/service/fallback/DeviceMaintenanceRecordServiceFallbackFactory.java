package com.central.ops.service.fallback;

import com.central.common.model.Result;
import com.central.ops.model.vo.DeviceMaintenanceRecord;
import com.central.ops.service.IDeviceMaintenanceRecordService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName: IDeviceMaintenanceRecordServiceFallbackFactory
 * @Description: Todo
 *
 * @data: 2020/6/15  14:06
 */


@Slf4j
@Component
public class DeviceMaintenanceRecordServiceFallbackFactory implements FallbackFactory<IDeviceMaintenanceRecordService> {
    @Override
    public IDeviceMaintenanceRecordService create(Throwable throwable) {
        return new IDeviceMaintenanceRecordService() {
            @Override
            public Result save(DeviceMaintenanceRecord deviceMaintenanceRecord) {
                return null;
            }

            @Override
            public DeviceMaintenanceRecord findByWorkorderId(Long id) {
                return new DeviceMaintenanceRecord();
            }
        };
    }

}
