package com.central.ops.service.impl;

import com.central.ops.model.WorkorderRecord;
import com.central.ops.mapper.WorkorderRecordMapper;
import com.central.ops.service.IWorkorderRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-11
 */
@Service
public class WorkorderRecordServiceImpl extends ServiceImpl<WorkorderRecordMapper, WorkorderRecord> implements IWorkorderRecordService {

}
