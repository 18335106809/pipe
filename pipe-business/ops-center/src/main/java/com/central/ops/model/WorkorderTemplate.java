package com.central.ops.model;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WorkorderTemplate extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工单名称
     */
    @Excel(name = "工单名称", height = 20, width = 30, isImportField = "true_st")
    private String name;

    /**
     * 状态

     */
    private String status;

    /**
     * 流程图名称
     */
    private String flowDiagram;

    private Long acceptUserId;
    @Excel(name = "接受人", height = 20, width = 30, isImportField = "true_st")
    private String acceptUserName;
    @Excel(name = "登记", height = 20, width = 30, isImportField = "true_st")
    private String level;

}
