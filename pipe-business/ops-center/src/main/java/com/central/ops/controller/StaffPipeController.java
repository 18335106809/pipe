package com.central.ops.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginUser;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.model.EvaluationProjectManage;
import com.central.ops.model.StaffPipe;
import com.central.ops.service.IStaffPipeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-02-25
 */
@RestController
@Api(tags = "入廊管理模块api")
@RequestMapping("/staffpipe")
public class StaffPipeController  {

    @Resource
    private IStaffPipeService iStaffPipeService;
    /**
     * 入廊申请
     */
    @AuditLog(operation = "入廊申请")
    @PostMapping("/add")
    @ApiOperation(value = "入廊申请", notes="返回入廊申请")
    public Result Save(@RequestBody StaffPipe staffPipe, @LoginUser(isFull = true) SysUser user) {
        staffPipe.setSubmitterUserId(user.getId());
        staffPipe.setSubmitterName(user.getNickname());
        return  Result.succeed(iStaffPipeService.saveOrUpdate(staffPipe), "添加成功");
    }


    /**
     * 入廊修改
     */
    @AuditLog(operation = "入廊修改")
    @PostMapping("/edit")
    @ApiOperation(value = "入廊修改", notes="返回入廊申请")
    public Result edit(@RequestBody StaffPipe staffPipe) {
        return  Result.succeed(iStaffPipeService.updateById(staffPipe));

    }

    /**
     * 入廊删除
     */
    @AuditLog(operation = "入廊删除")
    @PostMapping("/delete")
    @ApiOperation(value = "入廊删除", notes="返回入廊申请")
    public Result delete(@RequestBody Map<String, Object> params) {
        String id =   MapUtils.getString(params,"id");
        return  Result.succeed(iStaffPipeService.removeById(id),"删除成功");
    }

    /**
     * 入廊详情
     */
    @AuditLog(operation = "入廊详情")
    @PostMapping("/find")
    @ApiOperation(value = "入廊详情", notes="返回入廊详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "`id", value = "0", dataType = "Long"),
    })
    public Result find(@RequestBody Map<String, Object> params) {
            String id =   MapUtils.getString(params,"id");
        return  Result.succeed(iStaffPipeService.getById(id));
    }


    @PostMapping("/audit")
    @AuditLog(operation = "审核流程")
    @ApiOperation(value = "审核流程:verify:true同意，false驳回", notes = "审核流程")
    @Transactional
    public Result completeTask(@RequestBody Map<String, Object> params, @LoginUser(isFull = true) SysUser user) {
        Boolean verify = MapUtils.getBoolean(params, "verify");
        Long id = MapUtils.getLong(params, "id");
        if (verify) {
            StaffPipe staffPipe = new StaffPipe();
            staffPipe.setId(id);
            staffPipe.setStatus(1);
            staffPipe.setAcceptUserId(user.getId());
            staffPipe.setAcceptUserName(user.getNickname());
            staffPipe.setAcceptTime(new Date());
            Boolean save =  iStaffPipeService.updateById(staffPipe);

            return Result.succeed(save, "审核成功");
        } else {
            StaffPipe staffPipe = new StaffPipe();
            staffPipe.setId(id);
            staffPipe.setStatus(4);
            Boolean save =  iStaffPipeService.updateById(staffPipe);
            return Result.succeed(save,"驳回成功");
        }
    }


    @PostMapping("findAllByPage")
    @ApiOperation(value = "查询全部工单", notes = "查询全部工单")
    public Result  searchProcessInstance(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        QueryWrapper<StaffPipe> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        Integer type = MapUtils.getInteger(params, "type");
        if (type != null) {
            queryWrapper.eq("type",type);
        }
        Integer status = MapUtils.getInteger(params, "status");
        if (status !=null) {
            queryWrapper.eq("status",status);
        }
        String acceptUserId = MapUtils.getString(params, "acceptUserId");
        if (acceptUserId !=null) {
            queryWrapper.like("accept_user_id",acceptUserId);
        }
        String submitterUserId = MapUtils.getString(params, "submitterUserId");
        if (submitterUserId !=null) {
            queryWrapper.like("submitter_user_id",submitterUserId);
        }
        String pipeUserName = MapUtils.getString(params, "pipeUserName");
        if (pipeUserName !=null) {
            queryWrapper.like("pipe_user_name",pipeUserName);
        }
        Page<StaffPipe> page = new Page<>(pages,limit);
        Page<StaffPipe> staffPipePage = iStaffPipeService.page(page,queryWrapper);
        return  Result.succeed(staffPipePage);
    }


    @PostMapping("list")
    @ApiOperation(value = "查询全部工单", notes = "查询全部工单")
    public Result  list(@RequestBody Map<String, Object> params) {
        QueryWrapper<StaffPipe> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        List<StaffPipe> staffPipePage = iStaffPipeService.list();
        return  Result.succeed(staffPipePage);
    }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        QueryWrapper<StaffPipe> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        List<StaffPipe> staffPipePage = iStaffPipeService.list();
        //导出操作
        ExcelUtil.exportExcel(staffPipePage, null, "入廊管理", StaffPipe.class, "StaffPipe", response);
    }


    @AuditLog(operation = "获取流程图")
    @GetMapping(value = "/getImage", produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(value = "获取流程图", notes = "")
    @Transactional
    public byte[] getImage() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("processes/staff.png");
        InputStream inputStream =classPathResource.getInputStream();
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes, 0, inputStream.available());
        return bytes;
    }


}
