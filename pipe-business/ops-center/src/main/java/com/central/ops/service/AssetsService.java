package com.central.ops.service;



import com.central.common.constant.ServiceNameConstants;
import com.central.ops.model.LableManage;
import com.central.ops.service.fallback.AssetsServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 */
@FeignClient(name = ServiceNameConstants.ASSETS_SERVICE,fallbackFactory = AssetsServiceFallbackFactory.class, decode404 = true)
public interface AssetsService {

    @GetMapping(value = "/LableManage/findLableById")
    LableManage findLableById(@RequestParam("id") Long id);

}
