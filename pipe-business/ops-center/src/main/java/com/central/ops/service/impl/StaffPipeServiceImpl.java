package com.central.ops.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.ops.mapper.StaffPipeMapper;
import com.central.ops.model.StaffPipe;
import com.central.ops.service.IStaffPipeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-02-25
 */
@Service
public class StaffPipeServiceImpl extends ServiceImpl<StaffPipeMapper, StaffPipe> implements IStaffPipeService {

}
