package com.central.ops.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.common.annotation.LoginUser;
import com.central.common.model.Result;
import com.central.common.model.SysUser;
import com.central.ops.mapper.WorkorderMapper;
import com.central.ops.model.Workorder;
import com.central.ops.model.WorkorderRecord;
import com.central.ops.model.vo.Device;
import com.central.ops.model.vo.DeviceMaintenanceRecord;
import com.central.ops.service.IDeviceMaintenanceRecordService;
import com.central.ops.service.IDeviceService;
import com.central.ops.service.IWorkorderRecordService;
import com.central.ops.service.IWorkorderService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 服务实现类
 *
 * @since 2020-02-17
 */
@Service
public class WorkorderServiceImpl extends ServiceImpl<WorkorderMapper, Workorder>
    implements IWorkorderService {

  @Autowired private IDeviceService iDeviceService;
  @Autowired private IDeviceMaintenanceRecordService iDeviceMaintenanceRecordService;
  @Autowired private IWorkorderRecordService iWorkorderRecordService;

  @Override
  public Workorder saveWorker(Workorder workorder) {

    int size = baseMapper.selectCount(new QueryWrapper<>());

    SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd"); // 设置日期格式
    workorder.setCode(df.format(new Date()) + "_" + size);
    workorder.setSubmitterName(workorder.getSubmitterName());
    workorder.setSubmitterUserId(workorder.getSubmitterUserId());
    workorder.setMobile(workorder.getMobile());
    workorder.setType(workorder.getType());
    workorder.setStatus(0);
    baseMapper.insert(workorder);

    //保存记录
    WorkorderRecord workorderRecord = new WorkorderRecord();
    workorderRecord.setType(1);
    workorderRecord.setWorkorderId(workorder.getId());
    workorderRecord.setUserName(workorder.getSubmitterName());
    iWorkorderRecordService.save(workorderRecord);

    if (workorder.getDeviceId() != null) {
      Device device = iDeviceService.findById(workorder.getDeviceId());
      DeviceMaintenanceRecord deviceMaintenanceRecord = new DeviceMaintenanceRecord();
      deviceMaintenanceRecord.setWorkorderId(workorder.getId());
      deviceMaintenanceRecord.setDeviceName(device.getName());
      deviceMaintenanceRecord.setDeviceLocation(device.getCapsuleLocation());
      deviceMaintenanceRecord.setDeviceCapsuleName(device.getCapsule().toString());
      deviceMaintenanceRecord.setMaintenanceContent(workorder.getTitle());
      deviceMaintenanceRecord.setMaintenanceUserName(workorder.getSubmitterName());
      iDeviceMaintenanceRecordService.save(deviceMaintenanceRecord);
    }

    return baseMapper.selectById(workorder.getId());
  }
}
