package com.central.ops.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.StaffPipe;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-02-25
 */
@Mapper
public interface StaffPipeMapper extends BaseMapper<StaffPipe> {

}
