package com.central.ops.service.impl;

import com.central.ops.model.EvaluationScore;
import com.central.ops.mapper.EvaluationScoreMapper;
import com.central.ops.service.IEvaluationScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-26
 */
@Service
public class EvaluationScoreServiceImpl extends ServiceImpl<EvaluationScoreMapper, EvaluationScore> implements IEvaluationScoreService {

}
