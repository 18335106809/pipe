package com.central.ops.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.ops.model.ExternalStaffPipe;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-12
 */
@Mapper
public interface ExternalStaffPipeMapper extends BaseMapper<ExternalStaffPipe> {

}
