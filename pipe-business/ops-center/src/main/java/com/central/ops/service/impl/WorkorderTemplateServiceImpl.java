package com.central.ops.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.ops.mapper.WorkorderTemplateMapper;
import com.central.ops.model.WorkorderTemplate;
import com.central.ops.service.IWorkorderTemplateService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-14
 */
@Service
public class WorkorderTemplateServiceImpl extends ServiceImpl<WorkorderTemplateMapper, WorkorderTemplate> implements IWorkorderTemplateService {

}
