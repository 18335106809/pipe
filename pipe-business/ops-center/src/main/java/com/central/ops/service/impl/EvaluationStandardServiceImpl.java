package com.central.ops.service.impl;

import com.central.ops.model.EvaluationStandard;
import com.central.ops.mapper.EvaluationStandardMapper;
import com.central.ops.service.IEvaluationStandardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-22
 */
@Service
public class EvaluationStandardServiceImpl extends ServiceImpl<EvaluationStandardMapper, EvaluationStandard> implements IEvaluationStandardService {

}
