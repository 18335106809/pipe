package com.central.ops.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.ops.model.EvaluationProjectManage;
import com.central.ops.model.EvaluationScore;
import com.central.ops.model.EvaluationStandard;
import com.central.ops.service.IEvaluationScoreService;
import com.central.ops.service.IEvaluationStandardService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/EvaluationScore")
public class EvaluationScoreController {

    @Autowired
    private IEvaluationScoreService iEvaluationScoreService;
    @Autowired
    private IEvaluationStandardService iEvaluationStandardService;


    @PostMapping("/saveOrUpdate")
    @ApiOperation(value = "添加", notes = "返回绩效考核打分信息")
    public Result add(@RequestBody EvaluationScore evaluationScore) {
        return Result.succeed(iEvaluationScoreService.saveOrUpdate(evaluationScore));
    }

    @PostMapping(value = "/saveOrUpdateBatch")
    @ApiOperation(value = "批量更新")
    public Result saveOrUpdateBatch(@RequestBody List<EvaluationScore> evaluationScores){
        return Result.succeed(iEvaluationScoreService.saveOrUpdateBatch(evaluationScores),"保存成功");
    }

    @PostMapping("/find")
    @ApiOperation(value = "根据ID查找计划", notes="返回计划信息")
    public Result find( @RequestBody Map<String, Object> params) {
        String id = MapUtils.getString(params,"id");
        return  Result.succeed(iEvaluationScoreService.getById(id));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "根据ID删除计划", notes="返回计划信息")
    public Result delete( @RequestBody Map<String, Object> params) {
        String id = MapUtils.getString(params,"id");
        return  Result.succeed(iEvaluationScoreService.removeById(id));
    }


    @ApiOperation(value = "查询全部信息")
    @PostMapping(value = "/list")
    public Result queryList(@RequestBody Map<String, Object> params) {
        String userId = MapUtils.getString(params, "userId");
        QueryWrapper<EvaluationScore> scoreQueryWrapper = new QueryWrapper();
        QueryWrapper<EvaluationStandard> evaluationStandardQueryWrapper = new QueryWrapper();
        scoreQueryWrapper.eq("user_id", userId);
        String deptId = MapUtils.getString(params, "deptId");
        evaluationStandardQueryWrapper.eq("dept_id",deptId);
        List<EvaluationStandard> evaluationStandards = iEvaluationStandardService.list(evaluationStandardQueryWrapper);
        List<EvaluationScore> evaluationScores = iEvaluationScoreService.list(scoreQueryWrapper);

        for (int i=0;i<evaluationStandards.size();i++){
            for (int j=0;j<evaluationScores.size();j++){
                if (evaluationStandards.get(i).getId().equals(evaluationScores.get(j).getEvaluationStandardId())){
                    evaluationStandards.get(i).setActualScore(evaluationScores.get(j).getActualScore());
                }
            }
        }
        return Result.succeed(EvaluationStandardController.treeBuilder(evaluationStandards) );
    }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<EvaluationScore> result = iEvaluationScoreService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "绩效考核", EvaluationScore.class, "EvaluationScore", response);
    }



}
