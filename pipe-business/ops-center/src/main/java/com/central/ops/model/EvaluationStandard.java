package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.central.common.model.SuperEntity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-22
 */
@Data
@Accessors(chain = true)
public class EvaluationStandard extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long parentId;

    /**
     * 考核标题
     */
    @Excel(name = "标题", height = 20, width = 30, isImportField = "true_st")
    private String name;

    /**
     * 配分
     */
    @Excel(name = "配分", height = 20, width = 30, isImportField = "true_st")
    private String score;


    private String description;

    private String deptId;

    @TableField(exist = false)
    @Excel(name = "具体项", height = 20, width = 30, isImportField = "true_st")
    private List<EvaluationStandard> subEvaluationStandards;

    @Excel(name = "实际得分", height = 20, width = 30, isImportField = "true_st")
    @TableField(exist = false)
    private String actualScore;


}
