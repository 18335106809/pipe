package com.central.ops.controller;


import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.ops.model.EvaluationProjectManage;
import com.central.ops.service.IEvaluationProjectManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-02-26
 */
@RestController
@RequestMapping("/EvaluationProjectManage")
@Api(tags = "绩效项目模块api")
public class EvaluationProjectManageController  {

    @Resource
    private IEvaluationProjectManageService iEvaluationProjectManageService;

    @AuditLog(operation = "绩效管理")
    @PostMapping("/add")
    @ApiOperation(value = "添加绩效管理", notes = "返回绩效管理信息")
    public Result add(@RequestBody EvaluationProjectManage evaluationProjectManage) {
        return Result.succeed(iEvaluationProjectManageService.save(evaluationProjectManage));
    }

    @AuditLog(operation = "绩效管理")
    @PostMapping("/edit")
    @ApiOperation(value = "编辑绩效管理", notes = "返回绩效管理信息")
    public Result edit(@RequestBody EvaluationProjectManage scorePerformance) {
        return Result.succeed(iEvaluationProjectManageService.updateById(scorePerformance));
    }



    @AuditLog(operation = "绩效管理")
    @PostMapping("/find")
    @ApiOperation(value = "根据ID查找计划", notes="返回计划信息")
    public Result find( @RequestBody Map<String, Object> params) {
            String id = MapUtils.getString(params,"id");
        return  Result.succeed(iEvaluationProjectManageService.getById(id));
    }

    @AuditLog(operation = "绩效管理")
    @PostMapping("/delete")
    @ApiOperation(value = "根据ID查找计划", notes="返回计划信息")
    public Result delete( @RequestBody Map<String, Object> params) {
        String id = MapUtils.getString(params,"id");
        return  Result.succeed(iEvaluationProjectManageService.removeById(id));
    }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<EvaluationProjectManage> result = iEvaluationProjectManageService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "绩效管理", EvaluationProjectManage.class, "EvaluationProjectManage", response);
    }



}
