package com.central.ops.service;

import com.central.ops.model.EvaluationScore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-26
 */
public interface IEvaluationScoreService extends IService<EvaluationScore> {

}
