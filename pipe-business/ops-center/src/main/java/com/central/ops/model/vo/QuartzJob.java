package com.central.ops.model.vo;

import com.central.common.model.SuperEntity;
import lombok.Data;

/**
 * @ClassName: AppQuartz
 * @Description: Todo
 *
 * @data: 2020/5/18  19:32
 */

@Data
public class QuartzJob extends SuperEntity {

    /**
     * cron 表达式
     */
    private String cronExpression;

    /**
     * 描述
     */
    private String description;
    /**
     * 任务执行时调用哪个类的方法 包名+类名，完全限定名
     */
    private String jobClassName;
    /**
     * 触发器名称
     */
    private String triggerName;

    /**
     * 任务状态
     */
    private Integer jobState;
    /**
     * 任务名
     */
    private String jobName;
    /**
     * 任务分组
     */
    private String jobGroup;

}
