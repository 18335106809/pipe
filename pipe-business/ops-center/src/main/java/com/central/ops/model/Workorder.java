package com.central.ops.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import com.central.ops.model.vo.DeviceMaintenanceRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("workorder")
public class Workorder extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工单名称
     */
    @ApiModelProperty(value = "工单名称")
    @Excel(name = "名称", height = 20, width = 30, isImportField = "true_st")
    private String title;

    /**
     * 工单类型
     */
    @ApiModelProperty(value = "工单类型")
    @Excel(name = "类型", height = 20, width = 30, isImportField = "true_st")
    private String type;
    @Excel(name = "编号", height = 20, width = 30, isImportField = "true_st")
    private String code;
    @Excel(name = "提交人", height = 20, width = 30, isImportField = "true_st")
    private String submitterName;
    private Long submitterUserId;
    @Excel(name = "手机号", height = 20, width = 30, isImportField = "true_st")
    private String mobile;

    /**
     * 受理人
     */
    private Long acceptUserId;
    @Excel(name = "接受人", height = 20, width = 30, isImportField = "true_st")
    private String acceptUserName;
    @Excel(name = "回复", height = 20, width = 30, isImportField = "true_st")
    private String acceptReply;
    @Excel(name = "接受时间", height = 20, width = 30, isImportField = "true_st")
    private String acceptTime;
    /**
     * 等级
     */
    @Excel(name = "等级", height = 20, width = 30, isImportField = "true_st")
    private String level;

    /**
     * 工单状态(0未审批，1已审批，2工单完成)
     */
    @Excel(name = "工单状态", height = 20, width = 30, isImportField = "true_st")
    private Integer status;


    @TableField(exist = false)
    private Long deviceId;
    @TableField(exist = false)
    private DeviceMaintenanceRecord deviceMaintain;

    private String article;
}
