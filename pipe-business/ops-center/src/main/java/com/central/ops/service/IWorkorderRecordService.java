package com.central.ops.service;

import com.central.ops.model.WorkorderRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-11
 */
public interface IWorkorderRecordService extends IService<WorkorderRecord> {

}
