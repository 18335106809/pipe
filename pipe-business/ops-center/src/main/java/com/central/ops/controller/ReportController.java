package com.central.ops.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.model.Result;
import com.central.ops.model.Workorder;
import com.central.ops.model.vo.DeviceMaintenanceRecord;
import com.central.ops.service.IDeviceMaintenanceRecordService;
import com.central.ops.service.IWorkorderService;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName: ReportController @Description: Todo
 *
 * @data: 2020/5/29 13:21
 */
@Slf4j
@RestController
@Api(tags = "报表模块api")
@RequestMapping("/repoert")
public class ReportController {

  @Autowired private IWorkorderService iWorkorderService;
  @Autowired private IDeviceMaintenanceRecordService iDeviceMaintenanceRecordService;

  //    @ResponseBody
  //    @PostMapping("/test")
  //    @ApiOperation(value = "工单情况", notes="工单情况")
  //    public void test(@RequestBody Map<String, Object> params, HttpServletResponse response)
  // throws IOException {
  //        String type = MapUtils.getString(params, "type");
  //        response.setContentType("application/pdf");
  //        response.setHeader("Content-Disposition", "attachment;fileName=" + "保洁工作汇总");
  //        if (type.equals("year")) {
  //            // --------------------------------数据支撑---------------------------------------
  //            //条件查询本年度保洁工单
  //            QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
  //            queryWrapper.eq("type",1);
  //            queryWrapper.apply("YEAR(create_time)=YEAR(NOW())");
  //            List<Workorder> workorderList = iWorkorderService.list(queryWrapper);
  //            // --------------------------------列表展现---------------------------------------
  //                      String dest = "./target/table/保洁工作汇总.pdf";
  //                      File file = new File(dest);
  //                      file.getParentFile().mkdirs();
  //            OutputStream out = response.getOutputStream();
  //            // 创建文档
  //            PdfDocument pdf = new PdfDocument(new PdfWriter(dest));
  //            Document document = new Document(pdf);
  //            // 设置中文
  //            PdfFont font = PdfFontFactory.createFont("STSong-Light", "UniGB-UCS2-H", true);
  //
  //            // 加入标题
  //            Paragraph title =
  //                    new Paragraph("本年度保洁工作汇总\n")
  //                            .setTextAlignment(TextAlignment.CENTER)
  //                            .setFont(font)
  //                            .setBold()
  //                            .setFontSize(12);
  //            // 定义段落
  //            Paragraph content = new Paragraph();
  //            Paragraph h2title = new Paragraph();
  //            com.itextpdf.layout.element.List list = new
  // com.itextpdf.layout.element.List().setSymbolIndent(9)
  //                    .setListSymbol(ListNumberingType.DECIMAL)//有序列表
  //                    .setFont(font);
  //            if (workorderList!=null){
  //
  // content.add("\n本年度，保洁组按照保洁标准进行进行保洁，共计本季度完成"+workorderList.size()+"次。").setFont(font);
  //            }else{
  //                content.add("\n暂无本年度保洁工单完成情况。").setFont(font);
  //            }
  //
  //            document.add(title);
  //            document.add(content);
  //            document.add(h2title);
  //            document.add(list);
  //            // 关闭文档
  //            document.close();
  //        }
  //    }

  @GetMapping("/workorderCondition")
  @ApiOperation(value = "工单情况", notes = "工单情况")
  public void condition(@RequestParam Map<String, Object> params, HttpServletResponse response)
      throws IOException {

    //    String dest = "./target/table/工单情况汇总.pdf";
    //    File file = new File(dest);
    // --------------------------------数据支撑---------------------------------------
    String type = MapUtils.getString(params, "type");
    // 时间格式化
    String startTime = MapUtils.getString(params, "startTime");
    String endTime = MapUtils.getString(params, "endTime");

    // 条件查询今日工单
    QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
    queryWrapper
        .apply("{0} <= date_format(create_time,'%Y-%m-%d')", startTime)
        .apply("{0} >= date_format(create_time,'%Y-%m-%d') ", endTime);
    List<Workorder> list = iWorkorderService.list(queryWrapper);
    int finishedCount = 0;
    int unfinishedCount = 0;
    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).getStatus() == 2) {
        finishedCount++;
      } else {
        unfinishedCount++;
      }
    }

    OutputStream out = response.getOutputStream();
    response.setContentType("application/pdf");
    response.setCharacterEncoding("UTF-8");
    response.setHeader(
        "Content-Disposition",
        "attachment;fileName=" + URLEncoder.encode("工单情况汇总", "UTF-8") + ".pdf");
    // 创建文档
    PdfDocument pdf = new PdfDocument(new PdfWriter(out));
    Document document = new Document(pdf);
    // 设置中文
    PdfFont font = PdfFontFactory.createFont("STSong-Light", "UniGB-UCS2-H", true);

    // 定义段落
    Paragraph content = new Paragraph();
    Table table =
        new Table(UnitValue.createPercentArray(new float[] {0, 1, 2, 3, 4, 5, 6}))
            .useAllAvailableWidth();
    //            设置表格字体
    table.setFont(font);
    //            设置表格背景色
    //        table.setBackgroundColor(ColorConstants.LIGHT_GRAY);
    //            设置表格文本居中对齐
    table.setTextAlignment(TextAlignment.CENTER);
    //            设置表格垂直对齐方式
    table.setVerticalAlignment(VerticalAlignment.MIDDLE);
    //            设置表格水平对齐方式
    table.setHorizontalAlignment(HorizontalAlignment.CENTER);
    //            Cell cell=new Cell();
    //            设置单元格背景色为深灰色
    //            cell.setBackgroundColor(ColorConstants.DARK_GRAY);
    table.addCell(new Paragraph("编号"));
    table.addCell(new Paragraph("概要")).setWidth(500);
    table.addCell(new Paragraph("类型"));
    table.addCell(new Paragraph("申请人"));
    table.addCell(new Paragraph("审批人"));
    table.addCell(new Paragraph("状态"));
    table.addCell(new Paragraph("时间"));

    if (list == null ||list.size()==0) {
      content.add("\n" + startTime + "至" + endTime + "暂无相关工单情况").setFont(font);
      document.add(content);

      // 关闭文档
      document.close();
    } else {
      if (type.equals("day")) {
        // 加入标题
        Paragraph title =
            new Paragraph("今日工单情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);

        if (unfinishedCount == 0) {
          content.add("\n今日管廊运维工作按照运维计划开展，各项运维计划均已完成。今日运维工作主要包括：").setFont(font);
        } else {
          content
              .add("\n今日管廊运维工作按照运维计划开展,目前有" + unfinishedCount + "项工作未完成。今日运维工作主要包括：")
              .setFont(font);
        }

        // 时间格式化
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for (int j = 0; j < list.size(); j++) {
          table.addCell(new Paragraph("\n" + list.get(j).getCode()));
          table.addCell(new Paragraph(list.get(j).getTitle()));
          table.addCell(new Paragraph(list.get(j).getType()));
          table.addCell(new Paragraph(list.get(j).getSubmitterName()));
          table.addCell(new Paragraph(list.get(j).getAcceptUserName()));
          table.addCell(new Paragraph(String.valueOf(list.get(j).getStatus())));
          table.addCell(new Paragraph(sdf.format(list.get(j).getCreateTime())));
        }
        // 设置水平居中
        table.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        table.setVerticalAlignment(VerticalAlignment.MIDDLE);
        table.setFont(font);
        document.add(title);
        document.add(content);
        document.add(table);
        // 关闭文档
        document.close();
      }

      if (type.equals("week")) {
        // 加入标题
        Paragraph title =
            new Paragraph("本周运维计划完成情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (unfinishedCount == 0) {
          content.add("\n本周管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前全部完成。").setFont(font);
        } else {
          content
              .add(
                  "\n本周管廊工单工作按照运维计划开展，工单计划总计"
                      + list.size()
                      + ",目前有"
                      + unfinishedCount
                      + "项工作未完成。未完成的运维工作主要包括：")
              .setFont(font);
        }

        // 时间格式化
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for (int j = 0; j < list.size(); j++) {
          table.addCell(new Paragraph("\n" + list.get(j).getCode()));
          table.addCell(new Paragraph(list.get(j).getTitle()));
          table.addCell(new Paragraph(list.get(j).getType()));
          table.addCell(new Paragraph(list.get(j).getSubmitterName()));
          table.addCell(new Paragraph(list.get(j).getAcceptUserName()));
          table.addCell(new Paragraph(String.valueOf(list.get(j).getStatus())));
          table.addCell(new Paragraph(sdf.format(list.get(j).getCreateTime())));
        }
        // 设置水平居中
        table.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        table.setVerticalAlignment(VerticalAlignment.MIDDLE);
        table.setFont(font);
        document.add(title);
        document.add(content);
        document.add(table);
        // 关闭文档
        document.close();
      }
      if (type.equals("mouth")) {
        // 加入标题
        Paragraph title =
            new Paragraph("本月运维计划完成情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (unfinishedCount == 0) {
          content.add("\n本月管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前全部完成。").setFont(font);
        } else {
          content
              .add("\n本月管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前有" + unfinishedCount + "项工作未完成。")
              .setFont(font);
        }
        document.add(title);
        document.add(content);
        // 关闭文档
        document.close();
      }

      if (type.equals("quarter")) {
        // ---------------------------------季度情况-------------------------------------------
        // 加入标题
        Paragraph title =
            new Paragraph("季度工单情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (unfinishedCount == 0) {
          content.add("\n此季度管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前全部完成。").setFont(font);
        } else {
          content
              .add(
                  "\n此季度管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前有" + unfinishedCount + "项工作未完成。")
              .setFont(font);
        }
        document.add(title);
        document.add(content);
        // 关闭文档
        document.close();
      }
      if (type.equals("half")) {
        // 加入标题
        Paragraph title =
            new Paragraph("半年工单情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (unfinishedCount == 0) {
          content.add("\n此半年管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前全部完成。").setFont(font);
        } else {
          content
              .add(
                  "\n此半年管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前有" + unfinishedCount + "项工作未完成。")
              .setFont(font);
        }
        document.add(title);
        document.add(content);
        // 关闭文档
        document.close();
      }

      if (type.equals("year")) {
        // ---------------------------------年情况-------------------------------------------
        // 加入标题
        Paragraph title =
            new Paragraph("年度工单情况\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (unfinishedCount == 0) {
          content.add("\n年度管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前全部完成。").setFont(font);
        } else {
          content
              .add("\n年度管廊工单工作按照运维计划开展，工单计划总计" + list.size() + ",目前有" + unfinishedCount + "项工作未完成。")
              .setFont(font);
        }
        document.add(title);
        document.add(content);
        // 关闭文档
        document.close();
      }
    }
  }

  @GetMapping("/workorderClean")
  @ApiOperation(value = "今日保洁情况", notes = "今日保洁情况")
  public void clean(@RequestParam Map<String, Object> params, HttpServletResponse response)
      throws IOException {
    String type = MapUtils.getString(params, "type");
    OutputStream out = response.getOutputStream();
    response.setContentType("application/pdf");
    response.setHeader("Content-Disposition", "attachment;fileName=" + "保洁工作汇总");
    // 创建文档
    PdfDocument pdf = new PdfDocument(new PdfWriter(out));
    Document document = new Document(pdf);
    // 设置中文
    PdfFont font = PdfFontFactory.createFont("STSong-Light", "UniGB-UCS2-H", true);
    // 定义段落
    Paragraph content = new Paragraph();
    Paragraph h2title = new Paragraph();
    com.itextpdf.layout.element.List list =
        new com.itextpdf.layout.element.List()
            .setSymbolIndent(9)
            .setListSymbol(ListNumberingType.DECIMAL) // 有序列表
            .setFont(font);

    // --------------------------------数据支撑---------------------------------------
    // 条件查询本月保洁工单
    QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 1);
    // 时间格式化
    String startTime = MapUtils.getString(params, "startTime");
    String endTime = MapUtils.getString(params, "endTime");
    queryWrapper
        .apply("{0} <= date_format(create_time,'%Y-%m-%d')", startTime)
        .apply("{0} >= date_format(create_time,'%Y-%m-%d') ", endTime);
    List<Workorder> workorderList = iWorkorderService.list(queryWrapper);

    if (workorderList == null ||workorderList.size()==0) {
      content.add("\n" + startTime + "至" + endTime + "暂无相关工单情况").setFont(font);
      document.add(content);
      // 关闭文档
      document.close();
    } else {
      if (type.equals("day")) {
        // --------------------------------数据支撑---------------------------------------
        // 时间格式化
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        //        String today = df.format(new Date());
        //        // 条件查询今日工单
        //        QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
        //        queryWrapper.eq("type", 1);
        //        queryWrapper.apply("date_format(update_time,'%Y-%m-%d') = {0}", today);
        //        Workorder workorder = iWorkorderService.getOne(queryWrapper);

        // 加入标题
        Paragraph title =
            new Paragraph("今日保洁工作汇总\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);

        if (workorderList != null) {
          content
              .add(
                  "\n"
                      + sdf.format(new Date())
                      + "，保洁组对"
                      + workorderList.get(0).getTitle()
                      + ",按照保洁标准进行进行保洁，按时按量完成保洁工作。")
              .setFont(font);
          h2title.add("\n具体内容包括").setFont(font);
          List<String> lis = Arrays.asList(workorderList.get(0).getAcceptReply().split(","));
          for (String cont : lis) {
            list.add(new com.itextpdf.layout.element.ListItem(cont));
          }
        } else {
          content.add("\n" + sdf.format(new Date()) + ",暂无今日保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        // 关闭文档
        document.close();
      }
      if (type.equals("week")) {
        // 加入标题
        Paragraph title =
            new Paragraph("本周保洁工作汇总\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);

        Table table =
            new Table(UnitValue.createPercentArray(new float[] {0, 1, 2, 3}))
                .useAllAvailableWidth();
        if (workorderList != null) {
          content.add("\n本周，保洁组按照保洁标准进行进行保洁，共计本周完成" + workorderList.size() + "次。").setFont(font);

          //            设置表格字体
          table.setFont(font);
          //            设置表格背景色
          //        table.setBackgroundColor(ColorConstants.LIGHT_GRAY);
          //            设置表格文本居中对齐
          table.setTextAlignment(TextAlignment.CENTER);
          //            设置表格垂直对齐方式
          table.setVerticalAlignment(VerticalAlignment.MIDDLE);
          //            设置表格水平对齐方式
          table.setHorizontalAlignment(HorizontalAlignment.CENTER);
          //            Cell cell=new Cell();
          //            设置单元格背景色为深灰色
          //            cell.setBackgroundColor(ColorConstants.DARK_GRAY);
          table.addCell(new Paragraph("编号"));
          table.addCell(new Paragraph("概要")).setWidth(500);
          table.addCell(new Paragraph("时间"));
          table.addCell(new Paragraph("打扫内容"));
          // 时间格式化
          DateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm");
          for (int j = 0; j < workorderList.size(); j++) {
            table.addCell(new Paragraph("\n" + workorderList.get(j).getCode()));
            table.addCell(new Paragraph(workorderList.get(j).getTitle())).setWidth(500);
            table.addCell(new Paragraph(sd.format(workorderList.get(j).getCreateTime())));
            if (workorderList.get(j).getAcceptReply() != null) {
              table.addCell(new Paragraph(workorderList.get(j).getAcceptReply()));
            } else {
              table.addCell(new Paragraph("暂无保洁回复"));
            }
          }
          table.setFont(font);
        } else {
          content.add("\n暂无本周保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        document.add(table);
        // 关闭文档
        document.close();
      }
      if (type.equals("mouth")) {
        // 加入标题
        Paragraph title =
            new Paragraph("本月保洁工作汇总\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);

        Table table =
            new Table(UnitValue.createPercentArray(new float[] {0, 1, 2, 3}))
                .useAllAvailableWidth();
        if (workorderList != null) {
          content.add("\n本月，保洁组按照保洁标准进行进行保洁，共计本月完成" + workorderList.size() + "次。").setFont(font);

          //            设置表格字体
          table.setFont(font);
          //            设置表格背景色
          //        table.setBackgroundColor(ColorConstants.LIGHT_GRAY);
          //            设置表格文本居中对齐
          table.setTextAlignment(TextAlignment.CENTER);
          //            设置表格垂直对齐方式
          table.setVerticalAlignment(VerticalAlignment.MIDDLE);
          //            设置表格水平对齐方式
          table.setHorizontalAlignment(HorizontalAlignment.CENTER);
          //            Cell cell=new Cell();
          //            设置单元格背景色为深灰色
          //            cell.setBackgroundColor(ColorConstants.DARK_GRAY);
          table.addCell(new Paragraph("编号"));
          table.addCell(new Paragraph("概要")).setWidth(500);
          table.addCell(new Paragraph("时间"));
          table.addCell(new Paragraph("打扫内容"));
          // 时间格式化
          DateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm");
          for (int j = 0; j < workorderList.size(); j++) {
            table.addCell(new Paragraph("\n" + workorderList.get(j).getCode()));
            table.addCell(new Paragraph(workorderList.get(j).getTitle())).setWidth(500);
            table.addCell(new Paragraph(sd.format(workorderList.get(j).getCreateTime())));
            if (workorderList.get(j).getAcceptReply() != null) {
              table.addCell(new Paragraph(workorderList.get(j).getAcceptReply()));
            } else {
              table.addCell(new Paragraph("暂无保洁回复"));
            }
          }
          table.setFont(font);
        } else {
          content.add("\n暂无本月保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        document.add(table);
        // 关闭文档
        document.close();
      }
      if (type.equals("quarte")) {
        // 加入标题
        Paragraph title =
            new Paragraph("本季度保洁工作汇总\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);

        if (workorderList != null) {
          content.add("\n本季度，保洁组按照保洁标准进行进行保洁，共计本季度完成" + workorderList.size() + "次。").setFont(font);
        } else {
          content.add("\n暂无本季度保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        // 关闭文档
        document.close();
      }

      if (type.equals("half")) {
        // 加入标题
        Paragraph title =
                new Paragraph("本季度保洁工作汇总\n")
                        .setTextAlignment(TextAlignment.CENTER)
                        .setFont(font)
                        .setBold()
                        .setFontSize(12);

        if (workorderList != null) {
          content.add("\n半年度，保洁组按照保洁标准进行进行保洁，共计半年度完成" + workorderList.size() + "次。").setFont(font);
        } else {
          content.add("\n暂无本半年度保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        // 关闭文档
        document.close();
      }

      if (type.equals("year")) {
        // --------------------------------列表展现---------------------------------------
        String dest = "./target/table/保洁工作汇总.pdf";
        File file = new File(dest);
        file.getParentFile().mkdirs();

        // 加入标题
        Paragraph title =
            new Paragraph("本年度保洁工作汇总\n")
                .setTextAlignment(TextAlignment.CENTER)
                .setFont(font)
                .setBold()
                .setFontSize(12);
        if (workorderList != null) {
          content.add("\n本年度，保洁组按照保洁标准进行进行保洁，共计本季度完成" + workorderList.size() + "次。").setFont(font);
        } else {
          content.add("\n暂无本年度保洁工单完成情况。").setFont(font);
        }

        document.add(title);
        document.add(content);
        document.add(h2title);
        document.add(list);
        // 关闭文档
        document.close();
      }
    }
  }

  @GetMapping("/workorderPatrol")
  @ApiOperation(value = "今日巡检情况", notes = "今日巡检情况")
  public Result patrol() {
    // 时间格式化
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    String today = df.format(new Date());
    // 条件查询今日工单
    QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 2);
    queryWrapper.apply("date_format(update_time,'%Y-%m-%d') = {0}", today);
    List<Workorder> list = iWorkorderService.list(queryWrapper);
    int finishedCount = 0;
    int unfinishedCount = 0;
    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).getStatus() == 2) {
        finishedCount++;
      } else {
        unfinishedCount++;
      }
    }
    ;
    Map map = new HashMap();
    map.put("workorder", list);
    map.put("finishedCount", finishedCount);
    map.put("unfinishedCount", unfinishedCount);
    map.put("wcl", percentInstance(finishedCount, list.size()));

    return Result.succeed(map);
  }

  @GetMapping("/workordermMintain")
  @ApiOperation(value = "今日维修情况", notes = "今日维修情况")
  public Result maintain(@RequestParam Map<String, Object> params, HttpServletResponse response)
      throws IOException {

    String type = MapUtils.getString(params, "type");
    String dest = "./target/table/维修情况汇总.pdf";
    File file = new File(dest);
    // --------------------------------数据支撑---------------------------------------
    // 时间格式化
    String startTime = MapUtils.getString(params, "startTime");
    String endTime = MapUtils.getString(params, "endTime");

    // 时间格式化
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    String today = df.format(new Date());
    // 条件查询今日工单
    QueryWrapper<Workorder> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 3);
    queryWrapper.apply("date_format(update_time,'%Y-%m-%d') = {0}", today);
    List<Workorder> list = iWorkorderService.list(queryWrapper);

    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).getDeviceId() != null) {
        DeviceMaintenanceRecord deviceMaintenanceRecord =
            iDeviceMaintenanceRecordService.findByWorkorderId(list.get(i).getId());
        if (deviceMaintenanceRecord != null) {
          list.get(i).setDeviceMaintain(deviceMaintenanceRecord);
        }
      }
    }

    OutputStream out = response.getOutputStream();
    // 创建文档
    PdfDocument pdf = new PdfDocument(new PdfWriter(dest));
    Document document = new Document(pdf);
    // 设置中文
    PdfFont font = PdfFontFactory.createFont("STSong-Light", "UniGB-UCS2-H", true);

    Paragraph content = new Paragraph();
    com.itextpdf.layout.element.List cilList =
        new com.itextpdf.layout.element.List()
            .setSymbolIndent(9)
            .setListSymbol(ListNumberingType.DECIMAL) // 有序列表
            .setFont(font);

    if (type.equals("day")) {
      // 加入标题
      Paragraph title =
          new Paragraph("今日设备维修保养工作汇总\n")
              .setTextAlignment(TextAlignment.CENTER)
              .setFont(font)
              .setBold()
              .setFontSize(12);
      if (list.size() == 0) {
        content.add("\n今日共处理设备维修工单" + list.size() + "个，全部完成维修，未有设备维修工单逾期未完成情况,").setFont(font);
      } else {
        content.add("\n今日共处理设备维修工单" + list.size() + "个，涉及" + list.size() + "台设备，").setFont(font);
      }

      for (int i = 0; i <= list.size(); i++) {
        cilList.add(
            (new com.itextpdf.layout.element.ListItem(
                "工单号"
                    + list.get(i).getCode()
                    + "，"
                    + list.get(i).getDeviceMaintain().getDeviceLocation()
                    + "的"
                    + list.get(i).getDeviceMaintain().getDeviceName()
                    + "，"
                    + list.get(i).getTitle()
                    + "，由"
                    + list.get(i).getAcceptUserName()
                    + "到现场进行维修，故障现象为"
                    + list.get(i).getAcceptReply()
                    + " ，经检测，定位故障原因为+"
                    + list.get(i).getAcceptReply()
                    + "+，")));
      }
      document.add(title);
    }

    if (type.equals("week")) {
      // 加入标题
      Paragraph title =
          new Paragraph("本周设备维修保养工作汇总\n")
              .setTextAlignment(TextAlignment.CENTER)
              .setFont(font)
              .setBold()
              .setFontSize(12);
      if (list.size() == 0) {
        content.add("\n本周共处理设备维修工单" + list.size() + "个，全部完成维修，未有设备维修工单逾期未完成情况,").setFont(font);
      } else {
        content.add("\n本周共处理设备维修工单" + list.size() + "个，涉及" + list.size() + "台设备，").setFont(font);
      }

      for (int i = 0; i <= list.size(); i++) {
        cilList.add(
            (new com.itextpdf.layout.element.ListItem(
                "工单号"
                    + list.get(i).getCode()
                    + "，"
                    + list.get(i).getDeviceMaintain().getDeviceLocation()
                    + "的"
                    + list.get(i).getDeviceMaintain().getDeviceName()
                    + "，"
                    + list.get(i).getTitle()
                    + "，由"
                    + list.get(i).getAcceptUserName()
                    + "到现场进行维修，故障现象为"
                    + list.get(i).getAcceptReply()
                    + " ，经检测，定位故障原因为+"
                    + list.get(i).getAcceptReply()
                    + "+，")));
      }
      document.add(title);
    }
    if (type.equals("mouth")) {
      // 加入标题
      Paragraph title =
          new Paragraph("本月设备维修保养工作汇总\n")
              .setTextAlignment(TextAlignment.CENTER)
              .setFont(font)
              .setBold()
              .setFontSize(12);
      if (list.size() == 0) {
        content.add("\n本月共处理设备维修工单" + list.size() + "个，全部完成维修，未有设备维修工单逾期未完成情况,").setFont(font);
      } else {
        content.add("\n本周共处理设备维修工单" + list.size() + "个，涉及" + list.size() + "台设备，").setFont(font);
      }

      for (int i = 0; i <= list.size(); i++) {
        cilList.add(
            (new com.itextpdf.layout.element.ListItem(
                "工单号"
                    + list.get(i).getCode()
                    + "，"
                    + list.get(i).getDeviceMaintain().getDeviceLocation()
                    + "的"
                    + list.get(i).getDeviceMaintain().getDeviceName()
                    + "，"
                    + list.get(i).getTitle()
                    + "，由"
                    + list.get(i).getAcceptUserName()
                    + "到现场进行维修，故障现象为"
                    + list.get(i).getAcceptReply()
                    + " ，经检测，定位故障原因为+"
                    + list.get(i).getAcceptReply()
                    + "+，")));
      }
      document.add(title);
    }

    if (type.equals("quarter")) {
      // ---------------------------------季度情况-------------------------------------------
      // 加入标题
      Paragraph title =
          new Paragraph("季度设备维修保养工作汇总\n")
              .setTextAlignment(TextAlignment.CENTER)
              .setFont(font)
              .setBold()
              .setFontSize(12);
      if (list.size() == 0) {
        content.add("\n季度共处理设备维修工单" + list.size() + "个，全部完成维修，未有设备维修工单逾期未完成情况,").setFont(font);
      } else {
        content.add("\n季度共处理设备维修工单" + list.size() + "个，涉及" + list.size() + "台设备，").setFont(font);
      }
      document.add(title);
    }

    if (type.equals("year")) {
      // ---------------------------------年情况-------------------------------------------
      // 加入标题
      Paragraph title =
          new Paragraph("年度工单情况\n")
              .setTextAlignment(TextAlignment.CENTER)
              .setFont(font)
              .setBold()
              .setFontSize(12);
      if (list.size() == 0) {
        content.add("\n季度共处理设备维修工单" + list.size() + "个，全部完成维修，未有设备维修工单逾期未完成情况,").setFont(font);
      } else {
        content.add("\n季度共处理设备维修工单" + list.size() + "个，涉及" + list.size() + "台设备，").setFont(font);
      }
      document.add(title);
    }

    document.add(content);
    document.add(cilList);
    // 关闭文档
    document.close();

    // ---------------------------File-----------------------------------
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/pdf");
    response.setHeader(
        "Content-Disposition",
        "attachment;fileName=" + URLEncoder.encode(file.getName(), "UTF-8") + ".pdf");

    FileInputStream in = new FileInputStream(file);
    byte[] b = new byte[512];
    while ((in.read(b)) != -1) {
      out.write(b);
    }
    out.flush();
    in.close();
    out.close();
    if (file.exists()) {
      file.delete();
    }

    return Result.succeed("");
  }

  /**
   * 百分比计算
   *
   * @param TotalNumber 完成总数
   * @param totalNumber 资产总数 占比=完成总数/资产总数
   * @return
   */
  public String percentInstance(Integer TotalNumber, Integer totalNumber) {
    Double bfTotalNumber = Double.valueOf(TotalNumber);
    Double zcTotalNumber = Double.valueOf(totalNumber);
    double percent = bfTotalNumber / zcTotalNumber;
    // 获取格式化对象
    NumberFormat nt = NumberFormat.getPercentInstance();
    // 设置百分数精确度2即保留两位小数
    nt.setMinimumFractionDigits(2);
    return nt.format(percent);
  }

  private static void downLoadPdf(String dest, HttpServletResponse response) throws IOException {
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/pdf");
    response.setHeader(
        "Content-Disposition",
        "attachment;fileName=" + URLEncoder.encode("工单情况", "UTF-8") + ".pdf");

    FileInputStream in = new FileInputStream(new File(dest));
    OutputStream out = response.getOutputStream();
    byte[] b = new byte[512];
    while ((in.read(b)) != -1) {
      out.write(b);
    }
    out.flush();
    in.close();
    out.close();
  }
}
