package com.central.ops.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.central.ops.mapper.ExternalStaffPipeMapper;
import com.central.ops.model.ExternalStaffPipe;
import com.central.ops.service.IExternalStaffPipeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-12
 */
@Service
public class ExternalStaffPipeServiceImpl extends ServiceImpl<ExternalStaffPipeMapper, ExternalStaffPipe> implements IExternalStaffPipeService {

}
