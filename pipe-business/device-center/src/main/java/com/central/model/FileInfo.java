package com.central.model;

import com.central.common.model.SuperEntity;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-07-09
 */
@Data
@Accessors(chain = true)
public class FileInfo {

    private static final long serialVersionUID = 1L;

    /**
     * 文件md5
     */
    private String id;

    private String name;

    private Boolean isImg;

    private String contentType;

    private Integer size;

    /**
     * 物理路径
     */
    private String path;

    private String url;

    private String source;

    private Date createTime;

    private Date updateTime;

    /**
     * 租户字段
     */
    private String tenantId;

    private String deviceIp;


}
