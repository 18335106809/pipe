package com.central.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/** */
@Data
@Accessors(chain = true)
@TableName("device")
@EqualsAndHashCode(callSuper = false)
public class DeviceExcel extends SuperEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Excel(name = "设备名称", height = 20, width = 30, isImportField = "true_st")
  private String name;

  @Excel(name = "设备型号", height = 20, width = 30, isImportField = "true_st")
  private String unitType;

  @Excel(name = "生产日期", height = 20, width = 30, isImportField = "true_st")
  private Date producedTime;

  @Excel(name = "保修期", height = 20, width = 30, isImportField = "true_st")
  private String warranty;

  @Excel(name = "设备类型", height = 20, width = 30, isImportField = "true_st")
  private String type;

  @Excel(name = "所在舱体", height = 20, width = 30, isImportField = "true_st")
  private Long capsule;

  @Excel(name = "所在舱体组", height = 20, width = 30, isImportField = "true_st")
  private String capsuleGroup;

  @Excel(name = "设备位置", height = 20, width = 30, isImportField = "true_st")
  private String capsuleLocation;

  @Excel(name = "设备协议", height = 20, width = 30, isImportField = "true_st")
  private String protocol;

  @Excel(name = "设备状态", height = 20, width = 30, isImportField = "true_st")
  private Integer status;

  @Excel(name = "设备参数", height = 20, width = 30, isImportField = "true_st")
  private String parameters;

  @Excel(name = "健康值", height = 20, width = 30, isImportField = "true_st")
  private String healthIndex;

  @Excel(name = "设备阈值", height = 20, width = 30, isImportField = "true_st")
  private String threshold;

  @Excel(name = "所属PLCname", height = 20, width = 30, isImportField = "true_st")
  private String plcName;

  @Excel(name = "检修期", height = 20, width = 30, isImportField = "true_st")
  private String overhaul;

  @Excel(name = "理论寿命", height = 20, width = 30, isImportField = "true_st")
  private String theory;

  @Excel(name = "生产厂商", height = 20, width = 30, isImportField = "true_st")
  private String vendorName;

  @Excel(name = "理论寿命", height = 20, width = 30, isImportField = "true_st")
  private String code;

  @Excel(name = "位置坐标", height = 20, width = 30, isImportField = "true_st")
  private String position;

  @Excel(name = "图标", height = 20, width = 30, isImportField = "true_st")
  private String iconfont;

  @Excel(name = "颜色", height = 20, width = 30, isImportField = "true_st")
  private String color;

  @Excel(name = "plc信息", height = 20, width = 30, isImportField = "true_st")
  private String plcInfo;

  @Excel(name = "plcid", height = 20, width = 30, isImportField = "true_st")
  private Long plcId;
}
