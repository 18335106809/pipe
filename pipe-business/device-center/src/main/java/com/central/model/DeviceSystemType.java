package com.central.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("device_system_type")
public class DeviceSystemType extends SuperEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 需要的设备类型
     */
    private String deviceTypes;


}


