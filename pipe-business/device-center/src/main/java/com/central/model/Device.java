package com.central.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 **/
@Data
@Accessors(chain = true)
@TableName("device")
@EqualsAndHashCode(callSuper = false)
public class Device extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备名称
     */
    private String name;

    /**
     * 设备型号
     */
    private String unitType;

    /**
     * 生产日期
     */
    private Date producedTime;

    /**
     * 保修期
     */
    private String warranty;

    /**
     * 设备类型
     */
    private String type;

    /**
     * 所在舱体
     */
    private Long capsule;
    /**
     * 所在舱体组
     */
    private String capsuleGroup;
    /**
     * 设备位置
     */
    private String capsuleLocation;

    /**
     * 设备协议
     */
    private String protocol;

    /**
     * 设备状态
     */
    private Integer status;

    /**
     * 设备参数
     */
    private String  parameters;


    /**
     * 健康值
     */
    private String healthIndex;


    /**
     * 设备阈值
     */
    private String threshold;

    /**
     * 所属PLCname
     */
    private String plcName;

    /**
     * 所属PLCname
     */
    private String overhaul;

    private String theory;

    private String vendorName;

    private String code;

    private String position;

    private String iconfont;

    private String color;

    private String plcInfo;

    private Long  plcId;
}
