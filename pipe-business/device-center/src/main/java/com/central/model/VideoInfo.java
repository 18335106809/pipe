package com.central.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.central.common.model.SuperEntity;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class VideoInfo extends SuperEntity {

    private static final long serialVersionUID = 1L;

    private String name;

    private String ipAddress;

    private Long capsule;

    private String rtspAddress;

    /**
     * 0不需要记录 1需要记录
     */
    private Integer isRecord;

    /**
     *  0 未开始记录 1正在记录
     */
    private Boolean status;

    private String username;

    private String password;

    private String type;

    private String protocol;

    private String parameters;

    private String capsueLocation;

    private String position;

    private String iconfont;

    private String color;

    private String code;

    @TableField(exist = false)
    private String capsuleName;

    @TableField(exist = false)
    private Long spaceId;

    @TableField(exist = false)
    private String spaceName;

    @TableField(exist = false)
    private Integer capsuleNumber;
}
