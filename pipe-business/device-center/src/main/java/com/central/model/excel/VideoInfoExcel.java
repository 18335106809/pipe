package com.central.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class VideoInfoExcel extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "设备名称", height = 20, width = 30, isImportField = "true_st")
    private String name;
    @Excel(name = "ip地址", height = 20, width = 30, isImportField = "true_st")
    private String ipAddress;
    @Excel(name = "防火段", height = 20, width = 30, isImportField = "true_st")
    private Long capsule;
    @Excel(name = "rtsp地址", height = 20, width = 30, isImportField = "true_st")
    private String rtspAddress;

    /**
     * 0不需要记录 1需要记录
     */
    @Excel(name = "需要记录", height = 20, width = 30, isImportField = "true_st")
    private Integer isRecord;

    /**
     *  0 未开始记录 1正在记录
     */
    @Excel(name = "状态", height = 20, width = 30, isImportField = "true_st")
    private Boolean status;
    @Excel(name = "用户名", height = 20, width = 30, isImportField = "true_st")
    private String username;
    @Excel(name = "密码", height = 20, width = 30, isImportField = "true_st")
    private String password;
    @Excel(name = "类型", height = 20, width = 30, isImportField = "true_st")
    private String type;
    @Excel(name = "协议", height = 20, width = 30, isImportField = "true_st")
    private String protocol;
    @Excel(name = "参数", height = 20, width = 30, isImportField = "true_st")
    private String parameters;
    @Excel(name = "防火段位置", height = 20, width = 30, isImportField = "true_st")
    private String capsueLocation;

    @Excel(name = "位置", height = 20, width = 30, isImportField = "true_st")
    private String position;
    @Excel(name = "iconfont", height = 20, width = 30, isImportField = "true_st")
    private String iconfont;
    @Excel(name = "颜色", height = 20, width = 30, isImportField = "true_st")
    private String color;
    @Excel(name = "编号", height = 20, width = 30, isImportField = "true_st")
    private String code;

}
