package com.central.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-04-10
 */
@Data
@Accessors(chain = true)
@TableName("device_parameter")
public class DeviceParameter  extends SuperEntity implements Serializable{

    private static final long serialVersionUID = 1L;


    private String type;

    private String name;

    private String protocol;

    private String parameter;



}
