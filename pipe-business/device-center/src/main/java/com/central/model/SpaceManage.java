package com.central.model;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-03-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "space_manage")
public class SpaceManage extends SuperEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 名称
     */
    private String name;

    /**
     * 空间组
     */
    private String spaceGroup;

    /**
     * 位置
     */
    private String location;


    private Integer xAxis ;

    private Integer yAxis ;

    private Integer zAxis ;

    private Integer number;

    /**
     * 长度
     **/
    private String spaceLength;

    @TableField(exist = false)
    private List<Device> children;

    @TableField(exist = false)
    private List<VideoInfo> videoInfos;

    @TableField(exist = false)
    private List<SpaceManage> subSpaceManage;
}
