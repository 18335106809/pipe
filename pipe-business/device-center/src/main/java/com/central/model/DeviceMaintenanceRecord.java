package com.central.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.central.common.model.SuperEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-05-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DeviceMaintenanceRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "维修时间", height = 20, width = 30, isImportField = "true_st")
    private Date maintenanceTime;

    /**
     * 维修时间
     */
    @Excel(name = "维修内容", height = 20, width = 30, isImportField = "true_st")
    private String maintenanceContent;

    /**
     * 设备名称
     */
    @Excel(name = "设备名称", height = 20, width = 30, isImportField = "true_st")
    private String deviceName;

    /**
     * 设备所在舱体
     */
    @Excel(name = "设备所在舱体", height = 20, width = 30, isImportField = "true_st")
    private String deviceCapsuleName;

    /**
     * 设备所在舱体区域位置
     */
    @Excel(name = "设备所在舱体区域位置", height = 20, width = 30, isImportField = "true_st")
    private String deviceLocation;

    /**
     * 维修人名字
     */
    @Excel(name = "维修人名字", height = 20, width = 30, isImportField = "true_st")
    private String maintenanceUserName;

    private Long workorderId;

}
