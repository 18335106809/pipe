package com.central.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import com.central.service.ISpaceManageService;
import com.central.model.SpaceManage;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-02
 */
@RestController
@RequestMapping("/SpaceManage")
public class SpaceManageController  {

    @Resource
    private ISpaceManageService iSpaceManageService;

    @Resource(name = "JsonRedistemplate")
    private RedisTemplate redisTemplate;
    /**
     * 空间管理
     */
    @AuditLog(operation = "添加空间")
    @ApiOperation(value = "添加空间")
    @PostMapping("saveOrUpdate")
    public Result add(@RequestBody SpaceManage spaceManage) {
        boolean b = iSpaceManageService.saveOrUpdate(spaceManage);
        redisTemplate.opsForValue().set("spaceManage:"+ spaceManage.getId(),iSpaceManageService.getById(spaceManage.getId()));
        return  Result.succeed(b,"保存成功");
    }



    @PostMapping(value = "/saveBeachRedis")
    @ApiOperation(value = "批量更新")
    @AuditLog(operation = "批量更新")
    public Result saveBeachRedis(){
        List<SpaceManage> list = iSpaceManageService.list();
        for (int i = 0; i < list.size(); i++) {
            SpaceManage deviceInfo = iSpaceManageService.getById(list.get(i).getId());
            redisTemplate.opsForValue().set("spaceManage:" + deviceInfo.getId(), deviceInfo);
        }
        return Result.succeed("批量更新成功");
    }


    @AuditLog(operation ="根据id删除")
    @GetMapping("/delete")
    @ApiOperation(value = "入廊删除", notes="返回入廊申请")
    public Result delete(@RequestParam("id") Long id) {
        redisTemplate.delete("spaceManage:"+id);
        return  Result.succeed(iSpaceManageService.removeById(id));
    }

    @AuditLog(operation ="根据id查找")
    @GetMapping("/find")
    @ApiOperation(value = "根据ID查找计划", notes="返回计划信息")
    public Result find(@RequestParam("id") Long id) {
        return  Result.succeed(iSpaceManageService.getById(id));
    }


    @ApiOperation(value = "查询全部空间信息")
    @AuditLog(operation = "查询全部空间信息")
    @PostMapping(value = "/page")
    public Result  queryPage(@RequestBody Map<String, Object> params) {
        Integer pages = org.apache.commons.collections4.MapUtils.getInteger(params, "page");
        Integer limit = org.apache.commons.collections4.MapUtils.getInteger(params, "limit");
        Page<SpaceManage> page1 = new Page<>(pages,limit);
        //查询舱体所有空间
        Page<SpaceManage> page = iSpaceManageService.page(page1);
        //构建树
        List<SpaceManage> spaceManages = treeBuilder(page.getRecords());
        return Result.succeed(spaceManages);
    }

    @ApiOperation(value = "查询全部空间信息")
    @AuditLog(operation = "查询全部空间信息")
    @PostMapping(value = "/list")
    public Result  queryList() {

        //查询舱体所有空间
        List<SpaceManage> list = iSpaceManageService.list();
        //构建树
        List<SpaceManage> spaceManages = treeBuilder(list);
        return Result.succeed(spaceManages);
    }

    @ApiOperation(value = "查询空间一级目录")
    @AuditLog(operation = "查询空间一级目录")
    @PostMapping(value = "/parentList")
    public Result  querylist() {
        QueryWrapper<SpaceManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("parent_id","-1");
        return Result.succeed(iSpaceManageService.list(queryWrapper));
    }


    /**
     * 两层循环实现建树
     */
    public List<SpaceManage> treeBuilder(List<SpaceManage> spaceManages) {
        List<SpaceManage> spaces = new ArrayList<>();
        for (SpaceManage spaceManage : spaceManages) {
            if (ObjectUtils.equals(-1L, spaceManage.getParentId())) {
                spaces.add(spaceManage);
            }
            for (SpaceManage spacemanage : spaceManages) {
                if (spacemanage.getParentId().equals(spaceManage.getId())) {
                    if (spaceManage.getSubSpaceManage() == null) {
                        spaceManage.setSubSpaceManage(new ArrayList<>());
                    }
                    spaceManage.getSubSpaceManage().add(spacemanage);
                }
            }
        }
        return spaces;
    }



}
