package com.central.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.model.Device;
import com.central.model.FileInfo;
import com.central.model.SpaceManage;
import com.central.model.VideoInfo;
import com.central.model.excel.DeviceExcel;
import com.central.model.excel.VideoInfoExcel;
import com.central.service.ISpaceManageService;
import com.central.service.IVideoInfoService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-19
 */
@RestController
@RequestMapping("/VideoInfo")
public class VideoInfoController {

    @Autowired
    private IVideoInfoService iVideoInfoService;
    @Autowired
    private ISpaceManageService iSpaceManageService;
    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加视频信息")
    @AuditLog(operation = "添加视频信息")
    public Result saveOrUpdate(@RequestBody VideoInfo videoInfo){
        return Result.succeed(iVideoInfoService.saveOrUpdate(videoInfo));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除视频信息")
    @AuditLog(operation = "根据id删除视频信息")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iVideoInfoService.removeById(id));
    }

    @ApiOperation(value = "根据id视频信息")
    @AuditLog(operation = "根据id视频信息")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iVideoInfoService.getById(id));
    }

    @ApiOperation(value = "舱体视频设备列表")
    @PostMapping(value = "/list")
    public Result  list(@RequestBody Map<String, Object> params) {
        String name = MapUtils.getString(params, "name");
        String code = MapUtils.getString(params, "code");
        String type = MapUtils.getString(params, "type");
        String capsuleLocation = MapUtils.getString(params, "capsuleLocation");

        //查询舱体所有空间
        List<SpaceManage> spaceManageList = iSpaceManageService.list();
        //构建树
        List<SpaceManage> spaceManages = treeBuilder(spaceManageList);

        for (int i=0; i<spaceManages.size();i++){
            if (spaceManages.get(i).getSubSpaceManage() != null) {
                for (int j = 0; j < spaceManages.get(i).getSubSpaceManage().size(); j++) {
                    QueryWrapper<VideoInfo> queryWrapper = new QueryWrapper();
                    queryWrapper.orderByDesc("create_time");
                    if (StringUtils.isNotBlank(name)){
                        queryWrapper.eq("name",name);
                    }
                    if (StringUtils.isNotBlank(code)){
                        queryWrapper.eq("code",code);
                    }
                    if (StringUtils.isNotBlank(type)){
                        queryWrapper.eq("type",type);
                    }
                    if (StringUtils.isNotBlank(capsuleLocation)){
                        queryWrapper.like("capsule_location",capsuleLocation);
                    }
                    queryWrapper.eq("capsule",spaceManages.get(i).getSubSpaceManage().get(j).getId());
                    List<VideoInfo> videoInfos = iVideoInfoService.list(queryWrapper);
                    spaceManages.get(i).getSubSpaceManage().get(j).setVideoInfos(videoInfos);
                }
            }else {
                spaceManages.get(i).setSubSpaceManage(new ArrayList<>());
            }
        }
        return Result.succeed(spaceManages,"查询成功");
    }


    @ApiOperation(value = "根据舱体id查询")
    @GetMapping(value = "/querySpaceId")
    public Result querySpaceDevice(@RequestParam("spaceId") String spaceId) {

        QueryWrapper<SpaceManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", spaceId);
        // 查询舱体所有空间
        List<SpaceManage> spaceManageList = iSpaceManageService.list(queryWrapper);
        List capsuleId = new ArrayList();
        for (int i = 0; i < spaceManageList.size(); i++) {
            capsuleId.add(spaceManageList.get(i).getId());
        }
        if (capsuleId == null) {
            return Result.failed("舱体下没有设备");
        }
        QueryWrapper<SpaceManage> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("id", spaceId);
        SpaceManage spaceManage2 = iSpaceManageService.getOne(queryWrapper2);
        Map<Long, SpaceManage> spaceManageMap = new ConcurrentHashMap<>();
        for (SpaceManage spaceManage : spaceManageList) {
            spaceManageMap.put(spaceManage.getId(), spaceManage);
        }
        QueryWrapper<VideoInfo> deviceQueryWrapper = new QueryWrapper<>();
        deviceQueryWrapper.in("capsule", capsuleId);
        List<VideoInfo> devices = iVideoInfoService.list(deviceQueryWrapper);
        for (VideoInfo videoInfo : devices) {
            if(videoInfo.getCapsule() != null && spaceManageMap.containsKey(videoInfo.getCapsule())) {
                SpaceManage spaceManage = spaceManageMap.get(videoInfo.getCapsule());
                videoInfo.setCapsuleName(spaceManage.getName());
                videoInfo.setCapsuleNumber(spaceManage.getNumber());
            }
            videoInfo.setSpaceId(Long.valueOf(spaceId));
            videoInfo.setSpaceName(spaceManage2.getName());
        }
        List<VideoInfo> list = new ArrayList();
        devices.stream()
                .forEach(
                        d -> {
                            list.add(d);
                        });
        return Result.succeed(list);
    }


    @ApiOperation(value = "舱体设备type")
    @PostMapping(value = "/listByType")
    public Result listByType(@RequestBody Map<String, Object> params) {
        String capsule = MapUtils.getString(params, "capsule");
        String type = MapUtils.getString(params, "type");
        QueryWrapper<VideoInfo> queryWrapper = new QueryWrapper();
        if (StringUtils.isNotBlank(type)) {
            queryWrapper.eq("type", type);
        }
        if (StringUtils.isNotBlank(capsule)) {
            queryWrapper.eq("capsule", capsule);
        }
        return Result.succeed(iVideoInfoService.list(queryWrapper), "查询成功");
    }


    /**
     * 导出excel
     *
     * @return
     */
    @GetMapping("/export")
    public void exportUser(@RequestParam Map<String, Object> params, HttpServletResponse response)
            throws IOException {
        QueryWrapper<VideoInfo> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        List<VideoInfo> devices = iVideoInfoService.list(queryWrapper);
        List<VideoInfoExcel> deviceExcels = new ArrayList<>();
        for (int i = 0; i < devices.size(); i++) {
            VideoInfoExcel deviceExcel = new VideoInfoExcel();
            BeanUtil.copyProperties(devices.get(i), deviceExcel);
            deviceExcels.add(deviceExcel);
        }
        // 导出操作
        ExcelUtil.exportExcel(deviceExcels, null, "视频设备", DeviceExcel.class, "videos", response);
    }

    @PostMapping(value = "/import")
    public Result importExcl(@RequestParam("file") MultipartFile excl) throws Exception {
        int rowNum = 0;
        if(!excl.isEmpty()) {
            List<VideoInfoExcel> list = ExcelUtil.importExcel(excl, 0, 1, VideoInfoExcel.class);
            rowNum = list.size();
            if (rowNum > 0) {
                List<VideoInfo> devices = new ArrayList<>(rowNum);
                list.forEach(u -> {
                    VideoInfo deviceExcel = new VideoInfo();
                    BeanUtil.copyProperties(u, deviceExcel);
                    devices.add(deviceExcel);
                });
                iVideoInfoService.saveBatch(devices);
            }
        }
        return Result.succeed("导入数据成功，一共【"+rowNum+"】行");
    }


    /**
     * 两层循环实现建树
     */
    public  List<SpaceManage> treeBuilder(List<SpaceManage> spaceManages) {
        List<SpaceManage> spaces = new ArrayList<>();
        for (SpaceManage spaceManage : spaceManages) {
            if (ObjectUtils.equals(-1L, spaceManage.getParentId())) {
                spaces.add(spaceManage);
            }
            for (SpaceManage spacemanage : spaceManages) {
                if (spacemanage.getParentId().equals(spaceManage.getId())) {
                    if (spaceManage.getSubSpaceManage() == null) {
                        spaceManage.setSubSpaceManage(new ArrayList<>());
                    }
                    spaceManage.getSubSpaceManage().add(spacemanage);
                }
            }
        }
        return spaces;
    }

}
