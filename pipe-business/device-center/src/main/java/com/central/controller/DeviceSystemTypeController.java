package com.central.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import com.central.model.DeviceSystemType;
import com.central.service.IDeviceSystemTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-06-22
 */
@RestController
@RequestMapping("/DeviceSystemType")
public class DeviceSystemTypeController {

    @Autowired
    private IDeviceSystemTypeService iDeviceSystemTypeService;

    @ApiOperation(value = "保存参数")
    @PostMapping(value = "/saveOrUpdate")
    public Result saveParametcer(@RequestBody DeviceSystemType deviceSystemType){
        iDeviceSystemTypeService.saveOrUpdate(deviceSystemType);
        return Result.succeed(deviceSystemType);
    }

    @ApiOperation(value = "根据id删除参数")
    @DeleteMapping(value = "/delete")
    public Result deleteParametcerById(@RequestParam("id") String id) {
        return Result.succeed(iDeviceSystemTypeService.removeById(id));
    }

    @ApiOperation(value = "根据设备id获取信息")
    @GetMapping(value = "/find")
    public Result queryAllParametcerTypes(@RequestParam("id") String id) {
        return Result.succeed(iDeviceSystemTypeService.getById(id));

    }

    @ApiOperation(value = "查询全部参数信息")
    @AuditLog(operation = "查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  queryPage(@RequestBody PageVo pageVo) {
        Page<DeviceSystemType> page=new Page<>(pageVo.getPage(),pageVo.getLimit());
        return Result.succeed(iDeviceSystemTypeService.page(page));
    }


    @ApiOperation(value = "查询全部参数信息")
    @PostMapping(value = "/list")
    public Result  list() {
        return Result.succeed(iDeviceSystemTypeService.list());
    }


}
