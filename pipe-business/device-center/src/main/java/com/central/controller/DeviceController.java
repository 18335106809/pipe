package com.central.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.constant.CommonConstant;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.model.DeviceParameter;
import com.central.model.excel.DeviceExcel;
import com.central.service.IDeviceParameterService;
import com.central.service.IDeviceService;
import com.central.service.ISpaceManageService;
import com.central.model.Device;
import com.central.model.SpaceManage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 前端控制器
 *
 * @since 2020-02-03
 */
@Slf4j
@RestController
@RequestMapping("/device")
@Api(value = "设备管理")
public class DeviceController {

  @Autowired private IDeviceService iDeviceService;

  @Resource(name = "JsonRedistemplate")
  private RedisTemplate redisTemplate;

  @Autowired private IDeviceParameterService iDeviceParameterService;
  @Autowired private ISpaceManageService iSpaceManageService;

  /**
   * 保存所有信息
   *
   * @param device 参数
   * @return 返回值
   */
  @PostMapping(value = "/saveOrUpdate")
  @ApiOperation(value = "添加设备")
  @AuditLog(operation = "添加设备")
  public Result saveDevice(@RequestBody Device device) {
    // 设置日期格式
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
    device.setCode(df.format(new Date()) + "_" + iDeviceService.count());
    iDeviceService.saveOrUpdate(device);
    Device deviceInfo = iDeviceService.getById(device.getId());
    redisTemplate.opsForValue().set("device:" + deviceInfo.getId(), deviceInfo);
    return Result.succeed(deviceInfo);
  }

  @ApiOperation(value = "根據防火段ID查設備")
  @GetMapping(value = "/queryDeviceIds")
  public Result queryDeviceIds(@RequestParam("capsule") String capsule) {
    QueryWrapper<Device> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("capsule", capsule);
    List<Device> devices = iDeviceService.list(queryWrapper);
    List list = new ArrayList();
    devices.stream()
        .forEach(
            d -> {
              list.add(d.getId());
            });
    return Result.succeed(list);
  }

  @ApiOperation(value = "根據防火段ID查設備")
  @GetMapping(value = "/queryDevice")
  public Result queryDevice(@RequestParam("capsule") String capsule) {
    QueryWrapper<Device> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("capsule", capsule);
    List<Device> devices = iDeviceService.list(queryWrapper);
    List<Device> list = new ArrayList();
    devices.stream()
        .forEach(
            d -> {
              list.add(d);
            });
    return Result.succeed(list);
  }

  @ApiOperation(value = "根據防火段ID查設備")
  @GetMapping(value = "/querySpaceDeviceIds")
  public Result querySpaceDeviceIds(@RequestParam("spaceId") String spaceId) {
    QueryWrapper<SpaceManage> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("parent_id", spaceId);
    // 查询舱体所有空间
    List<SpaceManage> spaceManageList = iSpaceManageService.list(queryWrapper);
    List capsuleId = new ArrayList();
    for (int i = 0; i < spaceManageList.size(); i++) {
      capsuleId.add(spaceManageList.get(i).getId());
    }
    if (capsuleId == null) {
      return Result.failed("舱体下没有设备");
    }
    QueryWrapper<Device> deviceQueryWrapper = new QueryWrapper<>();
    deviceQueryWrapper.in("capsule", capsuleId);
    List<Device> devices = iDeviceService.list(deviceQueryWrapper);
    List list = new ArrayList();
    devices.stream()
        .forEach(
            d -> {
              list.add(d.getId());
            });
    return Result.succeed(list);
  }

  @ApiOperation(value = "根據防火段ID")
  @GetMapping(value = "/querySpaceDevice")
  public Result querySpaceDevice(@RequestParam("spaceId") String spaceId) {

    QueryWrapper<SpaceManage> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("parent_id", spaceId);
    // 查询舱体所有空间
    List<SpaceManage> spaceManageList = iSpaceManageService.list(queryWrapper);
    List capsuleId = new ArrayList();
    for (int i = 0; i < spaceManageList.size(); i++) {
      capsuleId.add(spaceManageList.get(i).getId());
    }
    if (capsuleId == null) {
      return Result.failed("舱体下没有设备");
    }
    QueryWrapper<Device> deviceQueryWrapper = new QueryWrapper<>();
    deviceQueryWrapper.in("capsule", capsuleId);
    List<Device> devices = iDeviceService.list(deviceQueryWrapper);
    List<Device> list = new ArrayList();
    devices.stream()
        .forEach(
            d -> {
              list.add(d);
            });
    return Result.succeed(list);
  }

  @PostMapping(value = "/saveBeachRedis")
  @ApiOperation(value = "批量更新")
  @AuditLog(operation = "批量更新")
  public Result saveBeachRedis() {
    List<Device> list = iDeviceService.list();
    for (int i = 0; i < list.size(); i++) {
      Device deviceInfo = iDeviceService.getById(list.get(i).getId());
      redisTemplate.opsForValue().set("device:" + deviceInfo.getId(), deviceInfo);
    }
    return Result.succeed("批量更新成功");
  }

  @GetMapping(value = "/delete")
  @ApiOperation(value = "根据id删除设备")
  @AuditLog(operation = "根据id删除设备")
  public Result deleteDeviceById(@RequestParam("id") String id) {
    return Result.succeed(iDeviceService.deleteDeviceById(id));
  }

  @ApiOperation(value = "根据设备id编辑设备信息")
  @AuditLog(operation = "根据设备id编辑设备信息")
  @PatchMapping(value = "/edit")
  public Result editDevice(@RequestBody Device device) {
    return Result.succeed(iDeviceService.updateDeviceById(device));
  }

  @ApiOperation(value = "根据设备id获取设备信息")
  @AuditLog(operation = "根据设备id获取设备信息")
  @GetMapping(value = "/find")
  public Result queryAllDeviceById(@RequestParam("id") String id) {
    return Result.succeed(iDeviceService.getById(id));
  }

  @GetMapping(value = "/findById")
  public Device findById(@RequestParam("id") Long id) {
    return iDeviceService.getById(id);
  }

  @ApiOperation(value = "根据设备type获取设备信息")
  @AuditLog(operation = "根据设备type获取设备信息")
  @GetMapping(value = "/queryDeviceByType")
  public Result queryDeviceByType() {
    QueryWrapper<Device> queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByDesc("create_time");
    queryWrapper.select("type");
    List<Device> deviceList = iDeviceService.list(queryWrapper);
    Iterator it = deviceList.iterator();
    while (it.hasNext()) {
      if (it.next() == null) {
        it.remove();
      }
    }
    return Result.succeed(deviceList);
  }

  @ApiOperation(value = "查询全部参数信息")
  @AuditLog(operation = "查询全部参数信息")
  @PostMapping(value = "/page")
  public Result QueryPage(@RequestBody Map<String, Object> params) {
    Integer pages = org.apache.commons.collections4.MapUtils.getInteger(params, "page");
    Integer limit = org.apache.commons.collections4.MapUtils.getInteger(params, "limit");
    Page<SpaceManage> page1 = new Page<>(pages, limit);
    // 查询舱体所有空间
    Page<SpaceManage> page = iSpaceManageService.page(page1);
    // 构建树
    List<SpaceManage> spaceManages = treeBuilder(page.getRecords());
    for (int i = 0; i < spaceManages.size(); i++) {
      if (spaceManages.get(i).getSubSpaceManage() != null) {
        for (int j = 0; j < spaceManages.get(i).getSubSpaceManage().size(); j++) {
          QueryWrapper<Device> queryWrapper = new QueryWrapper();
          queryWrapper.eq("capsule", spaceManages.get(i).getSubSpaceManage().get(j).getId());
          List<Device> device = iDeviceService.list(queryWrapper);
          spaceManages.get(i).getSubSpaceManage().get(j).setChildren(device);
        }
      }
    }
    return Result.succeed(page);
  }

  @ApiOperation(value = "舱体设备列表")
  @PostMapping(value = "/list")
  public Result list(@RequestBody Map<String, Object> params) {
    String name = MapUtils.getString(params, "name");
    String code = MapUtils.getString(params, "code");
    String type = MapUtils.getString(params, "type");
    String capsuleLocation = MapUtils.getString(params, "capsuleLocation");

    // 查询舱体所有空间
    List<SpaceManage> spaceManageList = iSpaceManageService.list();
    // 构建树
    List<SpaceManage> spaceManages = treeBuilder(spaceManageList);

    for (int i = 0; i < spaceManages.size(); i++) {
      if (spaceManages.get(i).getSubSpaceManage() != null) {
        for (int j = 0; j < spaceManages.get(i).getSubSpaceManage().size(); j++) {
          QueryWrapper<Device> queryWrapper = new QueryWrapper();
          queryWrapper.orderByDesc("create_time");
          if (StringUtils.isNotBlank(name)) {
            queryWrapper.eq("name", name);
          }
          if (StringUtils.isNotBlank(code)) {
            queryWrapper.eq("code", code);
          }
          if (StringUtils.isNotBlank(type)) {
            queryWrapper.eq("type", type);
          }
          if (StringUtils.isNotBlank(capsuleLocation)) {
            queryWrapper.like("capsule_location", capsuleLocation);
          }
          queryWrapper.eq("capsule", spaceManages.get(i).getSubSpaceManage().get(j).getId());
          List<Device> device = iDeviceService.list(queryWrapper);
          spaceManages.get(i).getSubSpaceManage().get(j).setChildren(device);
        }
      } else {
        spaceManages.get(i).setSubSpaceManage(new ArrayList<>());
      }
    }
    return Result.succeed(spaceManages, "查询成功");
  }

  @ApiOperation(value = "舱体设备列表")
  @PostMapping(value = "/listByCapsule")
  public Result listByCapsule(@RequestBody Map<String, Object> params) {
    String capsule = MapUtils.getString(params, "capsule");
    QueryWrapper<Device> queryWrapper = new QueryWrapper();
    queryWrapper.orderByDesc("create_time");
    if (StringUtils.isNotBlank(capsule)) {
      queryWrapper.eq("capsule", capsule);
    }
    return Result.succeed(iDeviceService.list(queryWrapper), "查询成功");
  }

  @ApiOperation(value = "舱体设备type")
  @PostMapping(value = "/listByType")
  public Result listByType(@RequestBody Map<String, Object> params) {
    String capsule = MapUtils.getString(params, "capsule");
    String type = MapUtils.getString(params, "type");
    QueryWrapper<Device> queryWrapper = new QueryWrapper();
    if (StringUtils.isNotBlank(type)) {
      queryWrapper.eq("type", type);
    }
    if (StringUtils.isNotBlank(capsule)) {
      queryWrapper.eq("capsule", capsule);
    }
    return Result.succeed(iDeviceService.list(queryWrapper), "查询成功");
  }

  @ApiOperation(value = "舱体所有设备")
  @PostMapping(value = "/listByStatus")
  public Result listByStatus() {
    QueryWrapper<Device> queryWrapper = new QueryWrapper();
    List<Device> deviceList = iDeviceService.list(queryWrapper);
    HashSet hashSet = new HashSet<String>();
    deviceList.stream().forEach(d -> hashSet.add(d.getType()));
    Iterator<String> it = hashSet.iterator();
    List<Object> countList = new ArrayList<>();
    while (it.hasNext()) {
      Map map = new HashMap();
      String deviceType = it.next();
      List<Device> list =
          deviceList.stream()
              .filter(d -> d.getType().equals(deviceType))
              .collect(Collectors.toList());
      Integer badCount = 0;
      for (int j = 0; j < list.size(); j++) {
        if (list.get(j).getStatus() == 0) {
          badCount++;
        }
      }
      DeviceParameter deviceParameter =
          iDeviceParameterService.getOne(
              new QueryWrapper<DeviceParameter>().eq("type", deviceType));
      if (deviceParameter != null) {
        map.put("type", deviceParameter.getName());
      } else {
        map.put("type", deviceType);
      }

      map.put("AllCount", list.size());
      map.put("startCount", list.size() - badCount);
      map.put("badCount", badCount);
      countList.add(map);
    }

    return Result.succeed(countList, "查询成功");
  }

  @ApiOperation(value = "查询所有设备的运行状态")
  @PostMapping(value = "/allDeviceRunStatusCount")
  public Result allDeviceRunStatusCount() {
    List<Device> deviceList = iDeviceService.list();
    Integer badCount = 0;
    for (int j = 0; j < deviceList.size(); j++) {
      if (deviceList.get(j).getStatus() == 0) {
        badCount++;
      }
    }
    Map map = new HashMap();
    map.put("AllCount", deviceList.size());
    map.put("runCount", deviceList.size() - badCount);
    map.put("badCount", badCount);
      return Result.succeed(map,"查询成功");

 }

  @ApiOperation(value = "根据舱体ID和设备类型查询")
  @GetMapping(value = "/querySpaceDeviceByType")
  public Result querySpaceDeviceByType(@RequestParam("spaceId") String spaceId,@RequestParam("type") String type) {

    QueryWrapper<SpaceManage> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("parent_id", spaceId);
    // 查询舱体所有空间
    List<SpaceManage> spaceManageList = iSpaceManageService.list(queryWrapper);
    List capsuleId = new ArrayList();
    for (int i = 0; i < spaceManageList.size(); i++) {
      capsuleId.add(spaceManageList.get(i).getId());
    }
    if (capsuleId.size()<1) {
      return Result.succeed("舱体下没有设备");
    }
    QueryWrapper<Device> deviceQueryWrapper = new QueryWrapper<>();
    deviceQueryWrapper.in("capsule", capsuleId);
    deviceQueryWrapper.in("type", type);
    List<Device> devices = iDeviceService.list(deviceQueryWrapper);
    List<Device> list = new ArrayList();
    devices.stream()
            .forEach(
                    d -> {
                      list.add(d);
                    });
    return Result.succeed(list);
  }

  /**
   * 导出excel
   *
   * @return
   */
  @GetMapping("/export")
  public void exportUser(@RequestParam Map<String, Object> params, HttpServletResponse response)
      throws IOException {
    String name = MapUtils.getString(params, "name");
    String code = MapUtils.getString(params, "code");
    String type = MapUtils.getString(params, "type");
    String capsuleLocation = MapUtils.getString(params, "capsuleLocation");
    QueryWrapper<Device> queryWrapper = new QueryWrapper();
    queryWrapper.orderByDesc("create_time");
    if (StringUtils.isNotBlank(name)) {
      queryWrapper.eq("name", name);
    }
    if (StringUtils.isNotBlank(code)) {
      queryWrapper.eq("code", code);
    }
    if (StringUtils.isNotBlank(type)) {
      queryWrapper.eq("type", type);
    }
    if (StringUtils.isNotBlank(capsuleLocation)) {
      queryWrapper.like("capsule_location", capsuleLocation);
    }
    List<Device> devices = iDeviceService.list(queryWrapper);
    List<DeviceExcel> deviceExcels = new ArrayList<>();
    for (int i = 0; i < devices.size(); i++) {
      DeviceExcel deviceExcel = new DeviceExcel();
      BeanUtil.copyProperties(devices.get(i), deviceExcel);
      deviceExcels.add(deviceExcel);
    }
    // 导出操作
    ExcelUtil.exportExcel(deviceExcels, null, "设备", DeviceExcel.class, "device", response);
  }

      @PostMapping(value = "/device/import")
      public Result importExcl(@RequestParam("file") MultipartFile excl) throws Exception {
          int rowNum = 0;
          if(!excl.isEmpty()) {
              List<DeviceExcel> list = ExcelUtil.importExcel(excl, 0, 1, DeviceExcel.class);
              rowNum = list.size();
              if (rowNum > 0) {
                  List<Device> devices = new ArrayList<>(rowNum);
                  list.forEach(u -> {
                      Device deviceExcel = new Device();
                      BeanUtil.copyProperties(u, deviceExcel);
                      devices.add(deviceExcel);
                  });
                  iDeviceService.saveBatch(devices);
              }
          }
          return Result.succeed("导入数据成功，一共【"+rowNum+"】行");
      }

  /** 两层循环实现建树 */
  public List<SpaceManage> treeBuilder(List<SpaceManage> spaceManages) {
    List<SpaceManage> spaces = new ArrayList<>();
    for (SpaceManage spaceManage : spaceManages) {
      if (ObjectUtils.equals(-1L, spaceManage.getParentId())) {
        spaces.add(spaceManage);
      }
      for (SpaceManage spacemanage : spaceManages) {
        if (spacemanage.getParentId().equals(spaceManage.getId())) {
          if (spaceManage.getSubSpaceManage() == null) {
            spaceManage.setSubSpaceManage(new ArrayList<>());
          }
          spaceManage.getSubSpaceManage().add(spacemanage);
        }
      }
    }
    return spaces;
  }
}
