package com.central.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import com.central.model.ProfileManage;
import com.central.service.IProfileManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: ProfileManageController
 * @Description: Todo
 *
 * @data: 2020/4/28  15:13
 */
@RestController
@RequestMapping("/profileManage")
@Api("指令管理")
public class ProfileManageController {

    @Autowired
    private IProfileManageService iProfileManageService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加指令")
    @AuditLog(operation = "添加指令")
    public Result save(@RequestBody ProfileManage profileManage){
        return Result.succeed(iProfileManageService.saveOrUpdate(profileManage));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除指令")
    @AuditLog(operation = "根据id删除指令")
    public Result deleteById(@RequestParam("id") String id) {
        return Result.succeed(iProfileManageService.removeById(id));
    }

    @ApiOperation(value = "根据设备id获取设备信息")
    @AuditLog(operation = "根据设备id获取设备信息")
    @GetMapping(value = "/find")
    public Result queryAllDeviceById(@RequestParam("id") String id) {
        return Result.succeed(iProfileManageService.getById(id));
    }


    @ApiOperation(value = "查询全部参数信息")
    @AuditLog(operation = "查询全部参数信息")
    @GetMapping(value = "/page")
    public Result  queryPage(@RequestBody PageVo pageVo) {
        Page<ProfileManage> page = new Page<>(pageVo.getPage(),pageVo.getLimit());
        QueryWrapper<ProfileManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        return Result.succeed(iProfileManageService.page(page,queryWrapper));
    }
}
