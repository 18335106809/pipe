package com.central.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import com.central.model.FileInfo;
import com.central.model.ProfileManage;
import com.central.service.IFileInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/FileInfo")
public class FileInfoController {
    @Autowired
    private IFileInfoService iFileInfoService;

    @GetMapping(value = "/page")
    public Result queryPage(@RequestBody PageVo pageVo) {
        Page<FileInfo> page = new Page<>(pageVo.getPage(),pageVo.getLimit());
        QueryWrapper<FileInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        return Result.succeed(iFileInfoService.page(page,queryWrapper));
    }

    @GetMapping(value = "/list")
    public Result queryList() {
        QueryWrapper<FileInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        return Result.succeed(iFileInfoService.list(queryWrapper));
    }
}
