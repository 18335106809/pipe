package com.central.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import com.central.model.DeviceMaintenanceRecord;
import com.central.model.VideoInfo;
import com.central.model.excel.DeviceExcel;
import com.central.model.excel.VideoInfoExcel;
import com.central.service.IDeviceMaintenanceRecordService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-05-21
 */
@RestController
@RequestMapping("/DeviceMaintenanceRecord")
public class DeviceMaintenanceRecordController {
    @Autowired
    private IDeviceMaintenanceRecordService iDeviceMaintenanceRecordService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加维修记录")
    @AuditLog(operation = "添加维修记录")
    public Result save(@RequestBody DeviceMaintenanceRecord deviceMaintenanceRecord){
        return Result.succeed(iDeviceMaintenanceRecordService.saveOrUpdate(deviceMaintenanceRecord));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除维修记录")
    @AuditLog(operation = "根据id删除维修记录")
    public Result deleteById(@RequestParam("id") String id) {
        return Result.succeed(iDeviceMaintenanceRecordService.removeById(id));
    }

    @ApiOperation(value = "根据id维修记录")
    @AuditLog(operation = "根据id维修记录")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") String id) {
        return Result.succeed(iDeviceMaintenanceRecordService.getById(id));
    }


    @GetMapping(value = "/findByWorkorderId")
    public DeviceMaintenanceRecord findByWorkorderId(@RequestParam("id") Long id) {
        return iDeviceMaintenanceRecordService.getOne(new QueryWrapper<DeviceMaintenanceRecord>().eq("workorder_id",id));
    }


    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<DeviceMaintenanceRecord> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String deviceName = MapUtils.getString(params, "deviceName");
        if (deviceName != null) {
            queryWrapper.like("device_name",deviceName);
        }
        String deviceLocation = MapUtils.getString(params, "deviceLocation");
        if (deviceLocation !=null) {
            queryWrapper.like("device_location",deviceLocation);
        }
        Integer maintenanceUserName = MapUtils.getInteger(params, "maintenanceUserName");
        if (maintenanceUserName !=null) {
            queryWrapper.like("maintenance_user_name",maintenanceUserName);
        }
        return Result.succeed(iDeviceMaintenanceRecordService.page(page,queryWrapper));
    }


    @GetMapping("/export")
    public void exportUser(@RequestParam Map<String, Object> params, HttpServletResponse response)
            throws IOException {
        QueryWrapper<DeviceMaintenanceRecord> queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        List<DeviceMaintenanceRecord> devices = iDeviceMaintenanceRecordService.list(queryWrapper);
        // 导出操作
        ExcelUtil.exportExcel(devices, null, "视频设备", DeviceExcel.class, "videos", response);
    }

}
