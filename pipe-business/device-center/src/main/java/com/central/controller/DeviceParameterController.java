package com.central.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.model.PageVo;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import com.central.model.Device;
import com.central.service.IDeviceParameterService;
import com.central.model.DeviceParameter;
import com.central.service.IDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-04-10
 */
@RestController
@RequestMapping("/deviceParameter")
@Api("协议")
public class DeviceParameterController {

    @Autowired
    private IDeviceParameterService iParametcerService;
    @Resource(name = "JsonRedistemplate")
    private RedisTemplate redisTemplate;
    @Autowired
    private IDeviceService iDeviceService;

    @ApiOperation(value = "保存参数")
    @AuditLog(operation = "保存参数")
    @PostMapping(value = "/saveOrUpdate")
    public Result saveParametcer(@RequestBody DeviceParameter deviceParameter){
        iParametcerService.saveOrUpdate(deviceParameter);
        redisTemplate.opsForValue().set("parameter:" + deviceParameter.getId(), deviceParameter);
        return Result.succeed(deviceParameter);
    }

    @ApiOperation(value = "根据id删除参数")
    @AuditLog(operation = "根据id删除参数")
    @DeleteMapping(value = "/delete")
    public Result deleteParametcerById(@RequestParam("id") String id) {
        redisTemplate.delete("parameter." +id);
        return Result.succeed(iParametcerService.removeById(id));
    }


    @ApiOperation(value = "根据设备id获取设备信息")
    @AuditLog(operation = "根据设备id获取设备信息")
    @GetMapping(value = "/find")
    public Result queryAllParametcerTypes(@RequestParam("id") String id) {
        return Result.succeed(iParametcerService.getById(id));

    }

    @ApiOperation(value = "根据设备type获取设备信息")
    @AuditLog(operation = "根据设备type获取设备信息")
    @GetMapping(value = "/QueryByType")
    public Result queryParametcerByType(@RequestParam("type") String type) {
        QueryWrapper<DeviceParameter> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("type",type);
        return Result.succeed(iParametcerService.getMap(queryWrapper));
    }

    @ApiOperation(value = "查询全部参数信息")
    @AuditLog(operation = "查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  queryPage(@RequestBody PageVo pageVo) {
        Page<DeviceParameter> page=new Page<>(pageVo.getPage(),pageVo.getLimit());
        return Result.succeed(iParametcerService.page(page));
    }


    @ApiOperation(value = "查询全部参数信息")
    @PostMapping(value = "/list")
    public Result  list() {
        return Result.succeed(iParametcerService.list());
    }



    @PostMapping(value = "/deviceSync")
    public Result  deviceSync() {
        List<Device> list = iDeviceService.list(new QueryWrapper<Device>().select("name", "type", "protocol", "parameters"));
        List<String> types = new ArrayList();

        List<DeviceParameter> deviceParameters= new ArrayList<>();
        for (int i=0;i<list.size();i++){
            if (!types.contains(list.get(i).getType())){
                DeviceParameter deviceParameter = new DeviceParameter();
                deviceParameter.setName(list.get(i).getName());
                deviceParameter.setParameter(list.get(i).getParameters());
                deviceParameter.setProtocol(list.get(i).getProtocol());
                deviceParameter.setType(list.get(i).getType());
                deviceParameters.add(deviceParameter);
            }
            types.add(list.get(i).getType());
        }
        iParametcerService.saveBatch(deviceParameters);
        return Result.succeed(iParametcerService.list());
    }


}
