package com.central.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.model.Device;
import org.apache.ibatis.annotations.Mapper;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-02-02
 */
@Mapper
public interface DeviceMapper extends BaseMapper<Device> {

}
