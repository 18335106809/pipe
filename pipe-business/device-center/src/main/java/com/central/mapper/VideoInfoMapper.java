package com.central.mapper;

import com.central.model.VideoInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-19
 */
public interface VideoInfoMapper extends BaseMapper<VideoInfo> {

}
