package com.central.mapper;

import com.central.model.DeviceSystemType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-06-22
 */
public interface DeviceSystemTypeMapper extends BaseMapper<DeviceSystemType> {

}
