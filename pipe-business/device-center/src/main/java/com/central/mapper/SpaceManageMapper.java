package com.central.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.model.SpaceManage;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-03-02
 */
@Mapper
public interface SpaceManageMapper extends BaseMapper<SpaceManage> {

}
