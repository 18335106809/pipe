package com.central.mapper;

import com.central.model.DeviceMaintenanceRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-05-21
 */
public interface DeviceMaintenanceRecordMapper extends BaseMapper<DeviceMaintenanceRecord> {

}
