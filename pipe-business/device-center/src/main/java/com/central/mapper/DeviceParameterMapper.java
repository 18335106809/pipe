package com.central.mapper;

import com.central.db.mapper.SuperMapper;
import com.central.model.DeviceParameter;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 *
 *
 */
@Mapper
public interface DeviceParameterMapper extends SuperMapper<DeviceParameter> {

}
