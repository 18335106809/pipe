package com.central.mapper;

import com.central.model.ProfileManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-04-28
 */
public interface ProfileManageMapper extends BaseMapper<ProfileManage> {

}
