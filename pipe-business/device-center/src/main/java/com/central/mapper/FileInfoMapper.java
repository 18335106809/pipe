package com.central.mapper;

import com.central.model.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-07-09
 */
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
