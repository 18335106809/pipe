package com.central.service;

import com.central.model.VideoInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangyang
 * @since 2020-08-19
 */
public interface IVideoInfoService extends IService<VideoInfo> {

}
