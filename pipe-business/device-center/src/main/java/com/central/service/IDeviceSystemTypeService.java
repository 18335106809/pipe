package com.central.service;

import com.central.model.DeviceSystemType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-06-22
 */
public interface IDeviceSystemTypeService extends IService<DeviceSystemType> {

}
