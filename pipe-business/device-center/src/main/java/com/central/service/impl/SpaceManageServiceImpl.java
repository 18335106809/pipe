package com.central.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.mapper.SpaceManageMapper;
import com.central.service.ISpaceManageService;
import com.central.model.SpaceManage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-03-02
 */
@Slf4j
@Service
@Component
public class SpaceManageServiceImpl extends ServiceImpl<SpaceManageMapper, SpaceManage> implements ISpaceManageService {

}
