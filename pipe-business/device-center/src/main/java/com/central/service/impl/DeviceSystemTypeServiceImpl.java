package com.central.service.impl;

import com.central.model.DeviceSystemType;
import com.central.mapper.DeviceSystemTypeMapper;
import com.central.service.IDeviceSystemTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-06-22
 */
@Service
public class DeviceSystemTypeServiceImpl extends ServiceImpl<DeviceSystemTypeMapper, DeviceSystemType> implements IDeviceSystemTypeService {

}
