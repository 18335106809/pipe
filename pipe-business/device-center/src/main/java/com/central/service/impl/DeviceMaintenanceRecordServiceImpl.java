package com.central.service.impl;

import com.central.mapper.DeviceMaintenanceRecordMapper;
import com.central.model.DeviceMaintenanceRecord;
import com.central.service.IDeviceMaintenanceRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-05-21
 */
@Service
public class DeviceMaintenanceRecordServiceImpl extends ServiceImpl<DeviceMaintenanceRecordMapper, DeviceMaintenanceRecord> implements IDeviceMaintenanceRecordService {

}
