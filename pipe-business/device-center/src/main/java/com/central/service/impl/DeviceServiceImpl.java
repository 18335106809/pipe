package com.central.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.mapper.DeviceMapper;
import com.central.service.IDeviceService;
import com.central.model.Device;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-02-02
 */
@Slf4j
@Service
@Component
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {


    @Resource(name = "JsonRedistemplate")
    private RedisTemplate redisTemplate;


    @Override
    public int deleteDeviceById(String id) {
        int i = baseMapper.deleteById(id);
        redisTemplate.delete("device:"+id);
        return i;
    }

    @Override
    public Device updateDeviceById(Device device) {
        baseMapper.updateById(device);
        redisTemplate.opsForValue().set("device:"+device.getId(),device);
        return  baseMapper.selectById(device);
    }



}
