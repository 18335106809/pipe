package com.central.service.impl;

import com.central.mapper.DeviceParameterMapper;
import com.central.service.IDeviceParameterService;
import com.central.model.DeviceParameter;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-10
 */
@Slf4j
@Service
@Component
public class DeviceParameterServiceImpl extends ServiceImpl<DeviceParameterMapper, DeviceParameter> implements IDeviceParameterService {

}
