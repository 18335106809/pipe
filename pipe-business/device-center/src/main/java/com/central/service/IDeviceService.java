package com.central.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.model.Device;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-02-02
 */
public interface IDeviceService extends IService<Device> {

    int deleteDeviceById(String id);

    Device updateDeviceById(Device device);


}
