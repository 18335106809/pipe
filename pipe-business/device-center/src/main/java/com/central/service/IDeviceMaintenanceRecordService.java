package com.central.service;

import com.central.model.DeviceMaintenanceRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-05-21
 */
public interface IDeviceMaintenanceRecordService extends IService<DeviceMaintenanceRecord> {

}
