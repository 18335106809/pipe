package com.central.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.model.SpaceManage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-03-02
 */
public interface ISpaceManageService extends IService<SpaceManage> {

}
