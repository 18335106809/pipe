package com.central.service;

import com.central.model.FileInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-09
 */
public interface IFileInfoService extends IService<FileInfo> {

}
