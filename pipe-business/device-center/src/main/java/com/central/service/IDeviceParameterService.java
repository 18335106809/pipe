package com.central.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.model.DeviceParameter;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-10
 */
public interface IDeviceParameterService extends IService<DeviceParameter> {

}
