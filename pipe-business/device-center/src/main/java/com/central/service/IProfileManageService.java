package com.central.service;

import com.central.model.ProfileManage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-28
 */
public interface IProfileManageService extends IService<ProfileManage> {

}
