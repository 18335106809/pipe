package com.central.service.impl;

import com.central.mapper.ProfileManageMapper;
import com.central.model.ProfileManage;
import com.central.service.IProfileManageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-28
 */
@Service
public class ProfileServiceImpl extends ServiceImpl<ProfileManageMapper, ProfileManage> implements IProfileManageService {

}
