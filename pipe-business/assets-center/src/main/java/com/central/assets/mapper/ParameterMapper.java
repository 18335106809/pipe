package com.central.assets.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.assets.entity.Parameter;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName: ParameterMapper
 * @Description: Todo
 *
 * @data: 2020/5/18  11:50
 */

@Mapper
public interface ParameterMapper  extends BaseMapper<Parameter> {
}
