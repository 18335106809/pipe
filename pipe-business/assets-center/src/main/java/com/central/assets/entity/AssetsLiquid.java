package com.central.assets.entity;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("assets_liquid")
public class AssetsLiquid extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Excel(name = "名称", height = 20, width = 30, isImportField = "true_st")
    private String name;

    /**
     * 编号
     */
    @Excel(name = "编号", height = 20, width = 30, isImportField = "true_st")
    private String number;

    /**
     * 型号
     */
    @Excel(name = "型号", height = 20, width = 30, isImportField = "true_st")
    private String type;

    /**
     * 导入人
     */
    @Excel(name = "导入人", height = 20, width = 30, isImportField = "true_st")
    private String importUserName;


    /**
     * 设备数量
     */
    @Excel(name = "设备数量", height = 20, width = 30, isImportField = "true_st")
    private Integer quantity;

    /**
     * 更换期
     */
    @Excel(name = "更换期", height = 20, width = 30, isImportField = "true_st")
    private String changePeriod;

    /**
     * 注释
     */
    @Excel(name = "注释", height = 20, width = 30, isImportField = "true_st")
    private String annotation;

    /**
     * 管廊id
     */
    private Long pipeId;
}
