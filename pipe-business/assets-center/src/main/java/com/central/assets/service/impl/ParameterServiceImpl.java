package com.central.assets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.assets.entity.Parameter;
import com.central.assets.mapper.ParameterMapper;
import com.central.assets.service.IParameterService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ParameterServiceImpl
 * @Description: Todo
 *
 * @data: 2020/5/18  11:49
 */
@Service
public class ParameterServiceImpl   extends ServiceImpl<ParameterMapper, Parameter> implements IParameterService {
}
