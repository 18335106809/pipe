package com.central.assets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.assets.entity.VendorManage;
import com.central.assets.mapper.VendorManageMapper;
import com.central.assets.service.IVendorManageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-28
 */
@Service
public class VendorManageServiceImpl extends ServiceImpl<VendorManageMapper, VendorManage> implements IVendorManageService {

}
