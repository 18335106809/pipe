package com.central.assets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.assets.entity.AssetsLiquid;
import com.central.assets.mapper.AssetsLiquidMapper;
import com.central.assets.service.IAssetsLiquidService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-28
 */
@Service
public class AssetsLiquidServiceImpl extends ServiceImpl<AssetsLiquidMapper, AssetsLiquid> implements IAssetsLiquidService {

}
