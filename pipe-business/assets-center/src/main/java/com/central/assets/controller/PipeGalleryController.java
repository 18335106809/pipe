package com.central.assets.controller;

import com.central.assets.entity.PipeGallery;
import com.central.assets.service.IPipeGalleryService;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: PipeGalleryController
 * @Description: Todo
 *
 * @data: 2020/4/29  16:08
 */
@RestController
@Api("管廊列表管理")
@RequestMapping("/pipeGallery")
public class PipeGalleryController {

    @Autowired
    private IPipeGalleryService iPipeGalleryService;


    @ApiOperation(value = "查询管廊列表")
    @AuditLog(operation = "查询管廊列表")
    @GetMapping(value = "/list")
    public Result QueryAllDeviceById() {
        return Result.succeed(iPipeGalleryService.list());
    }

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或保存管廊")
    @AuditLog(operation = "添加或保存管廊")
    public Result save(@RequestBody PipeGallery pipeGallery){
        return Result.succeed(iPipeGalleryService.saveOrUpdate(pipeGallery));
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除管廊")
    @AuditLog(operation = "根据id删除管廊")
    public Result deleteById(@RequestParam("id") String id) {
        return Result.succeed(iPipeGalleryService.removeById(id));
    }
}
