package com.central.assets.entity;

import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName: Parameter
 * @Description: Todo
 *
 * @data: 2020/5/18  11:46
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Parameter extends SuperEntity implements Serializable {
    private String name;
    private String unit;
    private String notes;
}
