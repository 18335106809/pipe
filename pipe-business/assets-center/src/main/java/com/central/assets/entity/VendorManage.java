package com.central.assets.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class VendorManage extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Excel(name = "厂商名称", height = 20, width = 30, isImportField = "true_st")
    private String name;
    @Excel(name = "地址", height = 20, width = 30, isImportField = "true_st")
    private String address;




}
