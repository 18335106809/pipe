package com.central.assets.service;

import com.central.assets.entity.LableManage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-28
 */
public interface ILableManageService extends IService<LableManage> {
    LableManage getLableById(Long id);
}
