package com.central.assets.mapper;

import com.central.assets.entity.LableManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-04-28
 */
@Mapper
public interface LableManageMapper extends BaseMapper<LableManage> {

}
