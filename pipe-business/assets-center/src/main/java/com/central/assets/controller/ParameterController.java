package com.central.assets.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.assets.entity.Parameter;
import com.central.assets.entity.VendorManage;
import com.central.assets.service.IParameterService;
import com.central.assets.service.IVendorManageService;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @ClassName: ParameterController
 * @Description: Todo
 *
 * @data: 2020/5/18  11:42
 */
@RestController
@Api("参数管理")
@RequestMapping("/Paramter")
public class ParameterController {

    @Autowired
    private IParameterService iParameterService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或保存系统参数")
    @AuditLog(operation = "添加或保存系统参数")
    public Result save(@RequestBody Parameter parameter){
        return Result.succeed(iParameterService.saveOrUpdate(parameter));
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除系统参数")
    @AuditLog(operation = "根据id删除系统参数")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iParameterService.removeById(id));
    }

    @ApiOperation(value = "根据id查找系统参数")
    @AuditLog(operation = "根据id查找系统参数")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iParameterService.getById(id));
    }

    @ApiOperation(value = "根据name查找系统参数")
    @GetMapping(value = "/findByName")
    public Result findByName(@RequestParam("name") String name) {
        return Result.succeed(iParameterService.getOne(new QueryWrapper<Parameter>().eq("name",name)));
    }


    @PostMapping(value = "/list")
    public Result  list(@RequestBody Map<String, Object> params) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (StringUtils.isNotBlank(name)) {
            queryWrapper.like("name",name);
        }
        String unit = MapUtils.getString(params, "unit");
        if (StringUtils.isNotBlank(unit)) {
            queryWrapper.like("unit",unit);
        }
        return Result.succeed(iParameterService.list(queryWrapper));
    }

    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<Parameter> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (StringUtils.isNotBlank(name)) {
            queryWrapper.like("name",name);
        }
        String unit = MapUtils.getString(params, "unit");
        if (StringUtils.isNotBlank(unit)) {
            queryWrapper.like("unit",unit);
        }
        return Result.succeed(iParameterService.page(page,queryWrapper));
    }
}
