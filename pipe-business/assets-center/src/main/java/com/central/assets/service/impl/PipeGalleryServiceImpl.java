package com.central.assets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.assets.entity.PipeGallery;
import com.central.assets.mapper.PipeGalleryMapper;
import com.central.assets.service.IPipeGalleryService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: PipeGalleryServiceImpl
 * @Description: Todo
 *
 * @data: 2020/4/29  16:09
 */

@Service
public class PipeGalleryServiceImpl  extends ServiceImpl<PipeGalleryMapper, PipeGallery> implements IPipeGalleryService {
}
