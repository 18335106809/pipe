package com.central.assets.mapper;

import com.central.assets.entity.AssetsLiquid;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-04-28
 */
@Mapper
public interface AssetsLiquidMapper extends BaseMapper<AssetsLiquid> {

}
