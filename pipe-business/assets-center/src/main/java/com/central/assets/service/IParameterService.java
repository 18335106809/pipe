package com.central.assets.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.assets.entity.Parameter;

/**
 * @ClassName: IParameterService
 * @Description: Todo
 *
 * @data: 2020/5/18  11:48
 */

public interface IParameterService  extends IService<Parameter> {
}
