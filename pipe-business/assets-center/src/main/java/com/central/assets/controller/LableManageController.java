package com.central.assets.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.assets.entity.LableManage;
import com.central.assets.service.ILableManageService;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-04-28
 */
@RestController
@Api("标签管理")
@RequestMapping("/LableManage")
public class LableManageController {
    @Autowired
    private ILableManageService iLableManageService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或保存标签")
    @AuditLog(operation = "添加或保存标签")
    public Result save(@RequestBody LableManage lableManage){
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式
        lableManage.setNumber(df.format(new Date()) + "_"+ iLableManageService.count());
        return Result.succeed(iLableManageService.saveOrUpdate(lableManage));

    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除标签")
    @AuditLog(operation = "根据id删除标签")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iLableManageService.removeById(id));
    }

    @ApiOperation(value = "根据id标签")
    @AuditLog(operation = "根据id标签")
    @GetMapping(value = "/find")
    public Result queryLableById(@RequestParam("id") Long id) {
        return Result.succeed(iLableManageService.getById(id));
    }


    @GetMapping(value = "/findLableById")
    public LableManage findLableById(@RequestParam("id") Long id) {
        return iLableManageService.getLableById(id);
    }

    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<LableManage> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        String name = MapUtils.getString(params, "name");
        if (name != null) {
            queryWrapper.like("name",name);
        }
        String number = MapUtils.getString(params, "number");
        if (number !=null) {
            queryWrapper.like("number",number);
        }
        String type = MapUtils.getString(params, "type");
        if (type !=null) {
            queryWrapper.like("type",type);
        }
        String lendState = MapUtils.getString(params, "lendState");
        if (lendState !=null) {
            queryWrapper.like("lend_state",lendState);
        }
        String lableState = MapUtils.getString(params, "lableState");
        if (lableState !=null) {
            queryWrapper.like("lable_state",lableState);
        }
        return Result.succeed(iLableManageService.page(page,queryWrapper));
    }

    @ApiOperation(value = "全部参数信息")
    @AuditLog(operation = "全部参数信息")
    @PostMapping(value = "/list")
    public Result  list() {
        return Result.succeed(iLableManageService.list());
    }

    /**
     * 导出excel
     *
     * @return
     */
    @GetMapping("/export")
    public void exportUser( HttpServletResponse response) throws IOException {
        QueryWrapper queryWrapper = new QueryWrapper();
        List<LableManage> result = iLableManageService.list(queryWrapper);
        //导出操作
        ExcelUtil.exportExcel(result, null, "标签", LableManage.class, "lable", response);
    }

    @PostMapping(value = "/import")
    public Result importExcl(@RequestParam("file") MultipartFile excl) throws Exception {
        int rowNum = 0;
        if(!excl.isEmpty()) {
            List<LableManage> list = ExcelUtil.importExcel(excl, 0, 1, LableManage.class);
            rowNum = list.size();
            if (rowNum > 0) {
                List<LableManage> lableManages = new ArrayList<>(rowNum);
                list.forEach(u -> {
                    LableManage lableManage = new LableManage();
                    BeanUtil.copyProperties(u, lableManage);
                    lableManages.add(lableManage);
                });
                iLableManageService.saveBatch(lableManages);
            }
        }
        return Result.succeed("导入数据成功，一共【"+rowNum+"】行");
    }

}
