package com.central.assets.entity;

import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName: PipeGallery
 * @Description: Todo
 *
 * @data: 2020/4/29  16:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PipeGallery extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 名称
     */
    private String name;
}
