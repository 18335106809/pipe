package com.central.assets.entity;

import java.io.Serializable;

import com.central.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ServerManage extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String number;

    /**
     * 型号
     */
    private String type;

    /**
     * 服务器IP
     */
    private String ipAddress;

    /**
     * 备注
     */
    private String annotation;


}
