package com.central.assets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.central.assets.entity.LableManage;
import com.central.assets.mapper.LableManageMapper;
import com.central.assets.service.ILableManageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-28
 */
@Service
public class LableManageServiceImpl extends ServiceImpl<LableManageMapper, LableManage> implements ILableManageService {

    @Override
    public LableManage getLableById(Long id) {
        return baseMapper.selectById(id);
    }
}
