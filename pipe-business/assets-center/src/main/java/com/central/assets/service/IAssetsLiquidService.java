package com.central.assets.service;

import com.central.assets.entity.AssetsLiquid;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-28
 */
public interface IAssetsLiquidService extends IService<AssetsLiquid> {

}
