package com.central.assets.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.assets.entity.LableManage;
import com.central.assets.entity.ServerManage;
import com.central.assets.service.ILableManageService;
import com.central.assets.service.IServerManageService;
import com.central.common.model.Result;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-04-28
 */
@RestController
@Api("服务器管理")
@RequestMapping("/serverManage")
public class ServerManageController {
    @Autowired
    private IServerManageService iServerManageService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或保存服务器")
    @AuditLog(operation = "添加或保存服务器")
    public Result save(@RequestBody ServerManage serverManage){
        return Result.succeed(iServerManageService.saveOrUpdate(serverManage));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除服务器")
    @AuditLog(operation = "根据id删除服务器")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iServerManageService.removeById(id));
    }

    @ApiOperation(value = "根据id查找服务器")
    @AuditLog(operation = "根据id查找服务器")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iServerManageService.getById(id));
    }


    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<ServerManage> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        String name = MapUtils.getString(params, "name");
        if (name!=null ||name !="") {
            queryWrapper.like("name",name);
        }
        String number = MapUtils.getString(params, "number");
        if (number!=null ||number !="") {
            queryWrapper.like("number",number);
        }
        String type = MapUtils.getString(params, "type");
        if (type!=null ||type !="") {
            queryWrapper.like("type",type);
        }
        return Result.succeed(iServerManageService.page(page,queryWrapper));
    }
}
