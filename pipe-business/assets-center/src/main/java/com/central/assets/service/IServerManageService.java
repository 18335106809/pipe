package com.central.assets.service;

import com.central.assets.entity.ServerManage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-28
 */
public interface IServerManageService extends IService<ServerManage> {

}
