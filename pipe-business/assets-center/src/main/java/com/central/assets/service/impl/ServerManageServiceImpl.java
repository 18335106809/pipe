package com.central.assets.service.impl;

import com.central.assets.entity.ServerManage;
import com.central.assets.mapper.ServerManageMapper;
import com.central.assets.service.IServerManageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-04-28
 */
@Service
public class ServerManageServiceImpl extends ServiceImpl<ServerManageMapper, ServerManage> implements IServerManageService {

}
