package com.central.assets.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.assets.entity.AssetsLiquid;
import com.central.assets.service.IAssetsLiquidService;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-04-28
 */
@RestController
@RequestMapping("/liquid")
@Api("流动资产")
public class AssetsLiquidController {

    @Autowired
    private IAssetsLiquidService iAssetsLiquidService;

        /**
         * 保存所有信息
         * @return 返回值
         */
        @PostMapping(value = "/saveOrUpdate")
        @ApiOperation(value = "添加流动资产")
        @AuditLog(operation = "添加流动资产")
        public Result save(@RequestBody AssetsLiquid assetsLiquid){
            return Result.succeed(iAssetsLiquidService.saveOrUpdate(assetsLiquid));
        }


        @DeleteMapping(value = "/delete")
        @ApiOperation(value = "根据id删除流动资产")
        @AuditLog(operation = "根据id删除流动资产")
        public Result deleteById(@RequestParam("id") Long id) {
            return Result.succeed(iAssetsLiquidService.removeById(id));
        }

        @ApiOperation(value = "根据id流动资产")
        @AuditLog(operation = "根据id流动资产")
        @GetMapping(value = "/find")
        public Result QueryAllDeviceById(@RequestParam("id") Long id) {
            return Result.succeed(iAssetsLiquidService.getById(id));
        }


        @ApiOperation(value = "分页查询全部参数信息")
        @AuditLog(operation = "分页查询全部参数信息")
        @PostMapping(value = "/page")
        public Result  QueryPage(@RequestBody Map<String, Object> params) {
            Integer pages = MapUtils.getInteger(params, "page");
            Integer limit = MapUtils.getInteger(params, "limit");
            Page<AssetsLiquid> page = new Page<>(pages,limit);
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.orderByDesc("create_time");
            String name = MapUtils.getString(params, "name");
            if (name != null) {
                queryWrapper.like("name",name);
            }
            String number = MapUtils.getString(params, "number");
            if (number !=null) {
                queryWrapper.like("number",number);
            }
            Long pipeId = MapUtils.getLong(params, "pipeId");
            if (pipeId !=null) {
                queryWrapper.like("pipe_id",pipeId);
            }
            String type = MapUtils.getString(params, "type");
            if (type !=null) {
                queryWrapper.like("type",type);
            }
            return Result.succeed(iAssetsLiquidService.page(page,queryWrapper));
        }

    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<AssetsLiquid> result = iAssetsLiquidService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "流动资产", AssetsLiquid.class, "AssetsLiquid", response);
    }


}
