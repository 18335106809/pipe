package com.central.assets.mapper;

import com.central.assets.entity.ServerManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-04-28
 */
public interface ServerManageMapper extends BaseMapper<ServerManage> {

}
