package com.central.assets.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.assets.entity.AssetsLiquid;
import com.central.assets.entity.ServerManage;
import com.central.assets.entity.VendorManage;
import com.central.assets.service.IServerManageService;
import com.central.assets.service.IVendorManageService;
import com.central.common.model.Result;
import com.central.common.utils.ExcelUtil;
import com.central.log.annotation.AuditLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-04-28
 */

@RequestMapping("/VendorMmanage")
@RestController
@Api("厂商管理")
public class VendorManageController {
    @Autowired
    private IVendorManageService iVendorManageService;

    /**
     * 保存所有信息
     * @return 返回值
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "添加或保存厂商")
    @AuditLog(operation = "添加或保存厂商")
    public Result save(@RequestBody VendorManage vendorManage){
        return Result.succeed(iVendorManageService.saveOrUpdate(vendorManage));
    }


    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "根据id删除厂商")
    @AuditLog(operation = "根据id删除厂商")
    public Result deleteById(@RequestParam("id") Long id) {
        return Result.succeed(iVendorManageService.removeById(id));
    }

    @ApiOperation(value = "根据id查找厂商")
    @AuditLog(operation = "根据id查找厂商")
    @GetMapping(value = "/find")
    public Result QueryAllDeviceById(@RequestParam("id") Long id) {
        return Result.succeed(iVendorManageService.getById(id));
    }


    @ApiOperation(value = "分页查询全部参数信息")
    @AuditLog(operation = "分页查询全部参数信息")
    @PostMapping(value = "/page")
    public Result  QueryPage(@RequestBody Map<String, Object> params) {
        Integer pages = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<VendorManage> page = new Page<>(pages,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        String name = MapUtils.getString(params, "name");
        if (name != null) {
            queryWrapper.like("name",name);
        }
        return Result.succeed(iVendorManageService.page(page,queryWrapper));
    }


    @GetMapping("/export")
    public void exportUser(HttpServletResponse response) throws IOException {
        List<VendorManage> result = iVendorManageService.list();
        //导出操作
        ExcelUtil.exportExcel(result, null, "厂商管理", VendorManage.class, "VendorManage", response);
    }

}
