package com.central.assets.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.assets.entity.PipeGallery;

public interface IPipeGalleryService extends IService<PipeGallery> {
}
