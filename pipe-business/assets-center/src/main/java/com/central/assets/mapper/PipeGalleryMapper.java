package com.central.assets.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.central.assets.entity.PipeGallery;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PipeGalleryMapper extends BaseMapper<PipeGallery> {
}
