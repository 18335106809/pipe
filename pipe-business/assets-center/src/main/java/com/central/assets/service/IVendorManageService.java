package com.central.assets.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.central.assets.entity.VendorManage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-04-28
 */
public interface IVendorManageService extends IService<VendorManage> {

}
