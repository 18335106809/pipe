package com.central;

import com.central.common.ribbon.annotation.EnableFeignInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableFeignInterceptor
@SpringBootApplication
@EnableFeignClients
public class AssetsCenterApp {
    public static void main(String[] args) {
        SpringApplication.run(AssetsCenterApp.class, args);
        log.info("===========================启动成功===========================");
    }
}
